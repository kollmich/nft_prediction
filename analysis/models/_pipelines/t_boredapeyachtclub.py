import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_boredapeyachtclub_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countClothes',
       'trait_countEarring', 'trait_countEyes', 'trait_countFur',
       'trait_countHat', 'trait_countMouth']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current', 'traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
       'trait_countEarring', 'trait_countEyes', 'trait_countFur',
       'trait_countHat', 'trait_countMouth', 
       'valueBackground', 'valueClothes',
       'valueEarring', 'valueEyes', 'valueFur', 'valueHat', 'valueMouth']]

    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground', 'valueClothes',
       'valueEarring', 'valueEyes', 'valueFur', 'valueHat', 'valueMouth']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current', 'traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
       'trait_countEarring', 'trait_countEyes', 'trait_countFur',
       'trait_countHat', 'trait_countMouth']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current', 'traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
       'trait_countEarring', 'trait_countEyes', 'trait_countFur',
       'trait_countHat', 'trait_countMouth']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = 'boredapeyachtclub_xgb_model'
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/boredapeyachtclub/{0}.json".format(model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueFur_SolidGold',
   'avg_daily_vol',
   'valueFur_Trippy',
   'trait_countFur',
   'valueClothes_VietnamJacket',
   'valueHat_Beanie',
   'valueHat_FauxHawk',
   'valueClothes_BikerVest',
   'valueEyes_Sad',
   'valueMouth_BoredUnshavenDagger',
   'min_trait_count',
   'valueMouth_BoredCigarette',
   'valueEyes_LaserEyes',
   'trait_countEyes',
   'eth_price_usd',
   'valueEyes_Heart',
   'valueClothes_WorkVest',
   'valueHat_KingsCrown',
   'valueHat_FishermansHat',
   'valueEarring_SilverHoop',
   'valueFur_Robot',
   'trait_countHat',
   'trait_countClothes',
   'valueMouth_BoredDagger',
   'valueMouth_BoredUnshaven'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_valueFur_SolidGold', 'shap_avg_daily_vol', 'shap_valueFur_Trippy',
       'shap_trait_countFur', 'shap_valueClothes_VietnamJacket',
       'shap_valueHat_Beanie', 'shap_valueHat_FauxHawk',
       'shap_valueClothes_BikerVest', 'shap_valueEyes_Sad',
       'shap_valueMouth_BoredUnshavenDagger', 'shap_min_trait_count',
       'shap_valueMouth_BoredCigarette', 'shap_valueEyes_LaserEyes',
       'shap_trait_countEyes', 'shap_eth_price_usd', 'shap_valueEyes_Heart',
       'shap_valueClothes_WorkVest', 'shap_valueHat_KingsCrown',
       'shap_valueHat_FishermansHat', 'shap_valueEarring_SilverHoop',
       'shap_valueFur_Robot', 'shap_trait_countHat', 'shap_trait_countClothes',
       'shap_valueMouth_BoredDagger', 'shap_valueMouth_BoredUnshaven'
    ]]

    df["shap_valueFur"] = (df["shap_valueFur_SolidGold"]+df["shap_valueFur_Trippy"]+df["shap_trait_countFur"]+df["shap_valueFur_Robot"]).round(6)
    df["shap_valueHat"] = (df["shap_valueHat_Beanie"]+df["shap_valueHat_FauxHawk"]+df["shap_valueHat_KingsCrown"]+df["shap_valueHat_FishermansHat"]+df["shap_trait_countHat"]).round(6)
    df["shap_valueEyes"] = (df["shap_valueEyes_Sad"]+df["shap_valueEyes_LaserEyes"]+df["shap_trait_countEyes"]+df["shap_valueEyes_Heart"]).round(6)
    df["shap_valueMouth"] = (df["shap_valueMouth_BoredUnshavenDagger"]+df["shap_valueMouth_BoredCigarette"]+df["shap_valueMouth_BoredDagger"]+df["shap_valueMouth_BoredUnshaven"]).round(6)
    df["shap_valueClothes"] = (df["shap_valueClothes_VietnamJacket"]+df["shap_valueClothes_BikerVest"]+df["shap_valueClothes_WorkVest"]+df["shap_trait_countClothes"]).round(6)
    df["shap_valueEarring"] = (df["shap_valueEarring_SilverHoop"]).round(6)
    df["shap_valueEyes"] = (df["shap_valueEyes_Sad"]+df["shap_valueEyes_LaserEyes"]+df["shap_trait_countEyes"]+df["shap_valueEyes_Heart"]).round(6)
    df["shap_valueOther"] = (df["shap_avg_daily_vol"]+df["shap_min_trait_count"]+df["shap_eth_price_usd"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueClothes','shap_valueEarring',
    'shap_valueEyes','shap_valueFur', 'shap_valueHat',
    'shap_valueMouth','shap_valueOther'
    ]]

    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
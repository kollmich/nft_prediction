import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'coolcats'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_coolcats_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countbody', 'trait_countface', 'trait_counthats',
       'trait_countshirt', 'trait_counttier']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbody', 'trait_countface', 'trait_counthats',
       'trait_countshirt', 'trait_counttier',
        'valuebody', 'valueface',
       'valuehats', 'valueshirt', 'valuetier']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valuebody', 'valueface',
       'valuehats', 'valueshirt', 'valuetier']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbody', 'trait_countface', 'trait_counthats',
       'trait_countshirt', 'trait_counttier']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbody', 'trait_countface', 'trait_counthats',
       'trait_countshirt', 'trait_counttier']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valuehats_flowerpink',
    'valueface_tired',
    'valueshirt_ninjablack',
    'min_trait_count',
    'num_sales',
    'eth_price_usd',
    'trait_countface',
    'avg_daily_vol',
    'valuetier_wild_1',
    'valuetier_exotic_2',
    'trait_counthats',
    'trait_counttier',
    'valueshirt_robot',
    'valuehats_robot',
    'valueshirt_costumefrog',
    'valuehats_costumefrog',
    'valueface_robot',
    'valueface_beardpirate',
    'trait_countshirt',
    'valueshirt_astroorange',
    'valueface_glassesfunny',
    'valuehats_costumegorilla',
    'valuehats_bow',
    'valuehats_knightblack',
    'valuehats_tvheadgrey',
    'valueshirt_mononoke',
    'valueface_ninjablue',
    'valueface_demon',
    'valuehats_deepseabronze',
    'valueshirt_deepseabronze',
    'valueface_ninjablack',
    'valuehats_flowerred',
    'valueface_faceface',
    'valueshirt_shirtbowtie',
    'valueface_beardtan',
    'valuetier_classy_2',
    'valuehats_beretgreen',
    'valuehats_tvheadwhite',
    'valuehats_deepseaorange',
    'valuehats_buckethattan',
    'valuehats_tvheadpurple',
    'valuehats_apple',
    'valueshirt_labcoat',
    'valueface_dizzy',
    'valuehats_sunhattan',
    'valueshirt_robeking'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
    'shap_valuehats_flowerpink', 'shap_valueface_tired',
    'shap_valueshirt_ninjablack', 'shap_min_trait_count', 'shap_num_sales',
    'shap_eth_price_usd', 'shap_trait_countface', 'shap_avg_daily_vol',
    'shap_valuetier_wild_1', 'shap_valuetier_exotic_2',
    'shap_trait_counthats', 'shap_trait_counttier', 'shap_valueshirt_robot',
    'shap_valuehats_robot', 'shap_valueshirt_costumefrog',
    'shap_valuehats_costumefrog', 'shap_valueface_robot',
    'shap_valueface_beardpirate', 'shap_trait_countshirt',
    'shap_valueshirt_astroorange', 'shap_valueface_glassesfunny',
    'shap_valuehats_costumegorilla', 'shap_valuehats_bow',
    'shap_valuehats_knightblack', 'shap_valuehats_tvheadgrey',
    'shap_valueshirt_mononoke', 'shap_valueface_ninjablue',
    'shap_valueface_demon', 'shap_valuehats_deepseabronze',
    'shap_valueshirt_deepseabronze', 'shap_valueface_ninjablack',
    'shap_valuehats_flowerred', 'shap_valueface_faceface',
    'shap_valueshirt_shirtbowtie', 'shap_valueface_beardtan',
    'shap_valuetier_classy_2', 'shap_valuehats_beretgreen',
    'shap_valuehats_tvheadwhite', 'shap_valuehats_deepseaorange',
    'shap_valuehats_buckethattan', 'shap_valuehats_tvheadpurple',
    'shap_valuehats_apple', 'shap_valueshirt_labcoat',
    'shap_valueface_dizzy', 'shap_valuehats_sunhattan',
    'shap_valueshirt_robeking'
    ]]

    df["shap_valuehats"] = (df["shap_valuehats_flowerpink"]+df["shap_trait_counthats"]+df["shap_valuehats_robot"]+df["shap_valuehats_costumefrog"]+df["shap_valuehats_costumegorilla"]+df["shap_valuehats_bow"]+df["shap_valuehats_knightblack"]+df["shap_valuehats_tvheadgrey"]+df["shap_valuehats_deepseabronze"]+df["shap_valuehats_flowerred"]+df["shap_valuehats_beretgreen"]+df["shap_valuehats_tvheadwhite"]+df["shap_valuehats_deepseaorange"]+df["shap_valuehats_tvheadpurple"]+df["shap_valuehats_buckethattan"]+df["shap_valuehats_apple"]+df["shap_valuehats_sunhattan"]).round(6)
    df["shap_valueface"] = (df["shap_valueface_tired"]+df['shap_trait_countface']+df["shap_valueface_robot"]+df["shap_valueface_beardpirate"]+df["shap_valueface_glassesfunny"]+df["shap_valueface_ninjablue"]+df["shap_valueface_demon"]+df["shap_valueface_ninjablack"]+df["shap_valueface_faceface"]+df["shap_valueface_beardtan"]+df["shap_valueface_dizzy"]).round(6)
    df["shap_valueshirt"] = (df["shap_valueshirt_ninjablack"]+df["shap_valueshirt_robot"]+df['shap_valueshirt_costumefrog']+df['shap_trait_countshirt']+df["shap_valueshirt_astroorange"]+df["shap_valueshirt_mononoke"]+df["shap_valueshirt_deepseabronze"]+df["shap_valueshirt_shirtbowtie"]+df["shap_valueshirt_labcoat"]+df["shap_valueshirt_robeking"]).round(6)
    df["shap_valuetier"] = (df["shap_valuetier_wild_1"]+df['shap_valuetier_exotic_2']+df['shap_trait_counttier']+df['shap_valuetier_classy_2']).round(6)
    df["shap_valueOther"] = (df["shap_avg_daily_vol"]+df['shap_min_trait_count']+df['shap_num_sales']+df['shap_eth_price_usd']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valuehats', 'shap_valueface',
    'shap_valueshirt', 'shap_valuetier',
    'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'creatureworld'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_creatureworld_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countCreature', 'trait_countDecoration',
       'trait_countEyes', 'trait_countForeground', 'trait_countMouth',
       'trait_countOutfit', 'trait_countOverlay']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCreature', 'trait_countDecoration',
       'trait_countEyes', 'trait_countForeground', 'trait_countMouth',
       'trait_countOutfit', 'trait_countOverlay',
       'valueBackground','valueCreature', 'valueDecoration', 'valueEyes', 'valueForeground',
       'valueMouth', 'valueOutfit', 'valueOverlay']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground','valueCreature', 'valueDecoration', 'valueEyes', 'valueForeground',
       'valueMouth', 'valueOutfit', 'valueOverlay']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCreature', 'trait_countDecoration',
       'trait_countEyes', 'trait_countForeground', 'trait_countMouth',
       'trait_countOutfit', 'trait_countOverlay']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCreature', 'trait_countDecoration',
       'trait_countEyes', 'trait_countForeground', 'trait_countMouth',
       'trait_countOutfit', 'trait_countOverlay']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueOutfit_Sacred',
   'min_trait_count',
   'trait_countEyes',
   'avg_daily_vol',
   'eth_price_usd',
   'valueOutfit_Raincoat',
   'trait_countOutfit',
   'valueDecoration_Mint',
   'valueCreature_BloodOrange',
   'valueOverlay_TulipFlowerwithStripes',
   'valueCreature_Olive',
   'valueOutfit_Cape',
   'valueBackground_Thermal',
   'trait_countCreature',
   'valueCreature_Hiding',
   'valueDecoration_MossPolkadots',
   'valueCreature_Wizard',
   'valueOutfit_CloudSuit',
   'valueForeground_PileSkyline',
   'valueDecoration_Hole',
   'trait_countBackground',
   'trait_countMouth'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_valueOutfit_Sacred', 'shap_min_trait_count',
      'shap_trait_countEyes', 'shap_avg_daily_vol', 'shap_eth_price_usd',
      'shap_valueOutfit_Raincoat', 'shap_trait_countOutfit',
      'shap_valueDecoration_Mint', 'shap_valueCreature_BloodOrange',
      'shap_valueOverlay_TulipFlowerwithStripes', 'shap_valueCreature_Olive',
      'shap_valueOutfit_Cape', 'shap_valueBackground_Thermal',
      'shap_trait_countCreature', 'shap_valueCreature_Hiding',
      'shap_valueDecoration_MossPolkadots', 'shap_valueCreature_Wizard',
      'shap_valueOutfit_CloudSuit', 'shap_valueForeground_PileSkyline',
      'shap_valueDecoration_Hole', 'shap_trait_countBackground',
      'shap_trait_countMouth'
    ]]

    df["shap_valueOutfit"] = (df["shap_valueOutfit_Sacred"]+df["shap_valueOutfit_Raincoat"]+df["shap_trait_countOutfit"]+df["shap_valueOutfit_Cape"]+df["shap_valueOutfit_CloudSuit"]).round(6)
    df["shap_valueDecoration"] = (df["shap_valueDecoration_Mint"]+df["shap_valueDecoration_MossPolkadots"]+df["shap_valueDecoration_Hole"]).round(6)
    df["shap_valueCreature"] = (df["shap_valueCreature_BloodOrange"]+df["shap_valueCreature_Olive"]+df["shap_trait_countCreature"]+df["shap_valueCreature_Hiding"]+df["shap_valueCreature_Wizard"]).round(6)
    df["shap_valueBackground"] = (df["shap_valueBackground_Thermal"]+df["shap_trait_countBackground"]).round(6)
    df["shap_valueOverlay"] = (df["shap_valueOverlay_TulipFlowerwithStripes"]).round(6)
    df["shap_valueEyes"] = (df["shap_trait_countEyes"]).round(6)
    df["shap_valueMouth"] = (df["shap_trait_countMouth"]).round(6)
    df["shap_valueOther"] = (df["shap_min_trait_count"]+df['shap_avg_daily_vol']+df['shap_eth_price_usd']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueOutfit', 'shap_valueDecoration',
    'shap_valueCreature', 'shap_valueBackground',
    'shap_valueOverlay', 'shap_valueEyes',
    'shap_valueMouth', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'deadfellaz'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_deadfellaz_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countBody', 'trait_countBodyGrade',
       'trait_countEyes', 'trait_countEyesGrade', 'trait_countHead',
       'trait_countHeadGrade', 'trait_countMouth', 'trait_countMouthGrade',
       'trait_countNose', 'trait_countNoseGrade']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countBodyGrade',
       'trait_countEyes', 'trait_countEyesGrade', 'trait_countHead',
       'trait_countHeadGrade', 'trait_countMouth', 'trait_countMouthGrade',
       'trait_countNose', 'trait_countNoseGrade', 'valueBackground',
       'valueBody', 'valueBodyGrade', 'valueEyes', 'valueEyesGrade',
       'valueHead', 'valueHeadGrade', 'valueMouth', 'valueMouthGrade',
       'valueNose', 'valueNoseGrade']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground',
       'valueBody', 'valueBodyGrade', 'valueEyes', 'valueEyesGrade',
       'valueHead', 'valueHeadGrade', 'valueMouth', 'valueMouthGrade',
       'valueNose', 'valueNoseGrade']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countBodyGrade',
       'trait_countEyes', 'trait_countEyesGrade', 'trait_countHead',
       'trait_countHeadGrade', 'trait_countMouth', 'trait_countMouthGrade',
       'trait_countNose', 'trait_countNoseGrade']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countBodyGrade',
       'trait_countEyes', 'trait_countEyesGrade', 'trait_countHead',
       'trait_countHeadGrade', 'trait_countMouth', 'trait_countMouthGrade',
       'trait_countNose', 'trait_countNoseGrade']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueHead_GreyBucket',
    'valueEyes_Sleepy',
    'avg_daily_vol',
    'valueBody_PurpleFur',
    'eth_price_usd',
    'valueEyesGrade_Rotten',
    'trait_countEyes',
    'valueBody_WhiteTee',
    'valueBody_SpottyFur',
    'trait_countBodyGrade',
    'valueEyes_Surprised',
    'num_sales',
    'valueBody_SilverPuffer',
    'trait_countMouth',
    'trait_countEyesGrade',
    'valueBackground_Blue',
    'valueHead_Beret',
    'valueBody_BlackDungarees',
    'valueHead_GolfCap',
    'valueBackground_Charcoal',
    'trait_countHead',
    'valueHead_Headscarf',
    'valueHead_SideShave',
    'valueEyesGrade_Fresh',
    'valueHead_OrangeBucket',
    'valueHead_Flattop',
    'trait_countBody',
    'valueBody_Suspenders',
    'valueEyes_ThickRim',
    'valueBody_FancySuit',
    'valueHead_RedCap',
    'trait_countMouthGrade',
    'valueEyes_WhiteGlasses',
    'valueBody_RedBaseball',
    'trait_countNoseGrade',
    'valueBodyGrade_Damaged',
    'valueHead_FeltHat',
    'valueHeadGrade_Fresh',
    'valueBody_Fishnet',
    'valueMouthGrade_Fresh',
    'valueNoseGrade_Fresh',
    'valueMouth_Fang',
    'valueBody_RedBowTie',
    'valueBody_BlueStripe',
    'valueHead_BoxFade',
    'valueMouth_Surprised',
    'valueEyes_Lazy',
    'valueBody_BlackSinglet',
    'valueBody_GreenShirt',
    'valueEyes_Reptile',
    'valueBody_BlackHoodie',
    'valueBody_BlackTurtleneck',
    'valueEyes_Spooky',
    'valueMouth_Skully',
    'valueBackground_Purple',
    'valueHead_Betty',
    'valueBody_DenimShirt',
    'valueHead_Bandana',
    'valueHead_BlackCap',
    'valueBody_DenimDungarees',
    'valueMouth_GoldGrin',
    'trait_countBackground',
    'valueBody_DarkGreyBrandTee',
    'valueEyes_Scared',
    'valueHead_BantuKnot',
    'trait_countNose',
    'trait_countHeadGrade',
    'valueBody_BlueTracksuit',
    'valueHeadGrade_Damaged',
    'valueBody_Tatts',
    'valueBody_PurpleButtonup',
    'valueBody_OrangePuffvest'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
        'shap_valueHead_GreyBucket', 'shap_valueEyes_Sleepy',
        'shap_avg_daily_vol', 'shap_valueBody_PurpleFur', 'shap_eth_price_usd',
        'shap_valueEyesGrade_Rotten', 'shap_trait_countEyes',
        'shap_valueBody_WhiteTee', 'shap_valueBody_SpottyFur',
        'shap_trait_countBodyGrade', 'shap_valueEyes_Surprised',
        'shap_num_sales', 'shap_valueBody_SilverPuffer',
        'shap_trait_countMouth', 'shap_trait_countEyesGrade',
        'shap_valueBackground_Blue', 'shap_valueHead_Beret',
        'shap_valueBody_BlackDungarees', 'shap_valueHead_GolfCap',
        'shap_valueBackground_Charcoal', 'shap_trait_countHead',
        'shap_valueHead_Headscarf', 'shap_valueHead_SideShave',
        'shap_valueEyesGrade_Fresh', 'shap_valueHead_OrangeBucket',
        'shap_valueHead_Flattop', 'shap_trait_countBody',
        'shap_valueBody_Suspenders', 'shap_valueEyes_ThickRim',
        'shap_valueBody_FancySuit', 'shap_valueHead_RedCap',
        'shap_trait_countMouthGrade', 'shap_valueEyes_WhiteGlasses',
        'shap_valueBody_RedBaseball', 'shap_trait_countNoseGrade',
        'shap_valueBodyGrade_Damaged', 'shap_valueHead_FeltHat',
        'shap_valueHeadGrade_Fresh', 'shap_valueBody_Fishnet',
        'shap_valueMouthGrade_Fresh', 'shap_valueNoseGrade_Fresh',
        'shap_valueMouth_Fang', 'shap_valueBody_RedBowTie',
        'shap_valueBody_BlueStripe', 'shap_valueHead_BoxFade',
        'shap_valueMouth_Surprised', 'shap_valueEyes_Lazy',
        'shap_valueBody_BlackSinglet', 'shap_valueBody_GreenShirt',
        'shap_valueEyes_Reptile', 'shap_valueBody_BlackHoodie',
        'shap_valueBody_BlackTurtleneck', 'shap_valueEyes_Spooky',
        'shap_valueMouth_Skully', 'shap_valueBackground_Purple',
        'shap_valueHead_Betty', 'shap_valueBody_DenimShirt',
        'shap_valueHead_Bandana', 'shap_valueHead_BlackCap',
        'shap_valueBody_DenimDungarees', 'shap_valueMouth_GoldGrin',
        'shap_trait_countBackground', 'shap_valueBody_DarkGreyBrandTee',
        'shap_valueEyes_Scared', 'shap_valueHead_BantuKnot',
        'shap_trait_countNose', 'shap_trait_countHeadGrade',
        'shap_valueBody_BlueTracksuit', 'shap_valueHeadGrade_Damaged',
        'shap_valueBody_Tatts', 'shap_valueBody_PurpleButtonup',
        'shap_valueBody_OrangePuffvest'
    ]]

    df["shap_valueHead"] = (df["shap_valueHead_GreyBucket"]+df["shap_valueHead_Beret"]+df["shap_valueHead_GolfCap"]+df["shap_trait_countHead"]+df["shap_valueHead_Headscarf"]+df["shap_valueHead_SideShave"]+df["shap_valueHead_OrangeBucket"]+df["shap_valueHead_Flattop"]+df["shap_valueHead_RedCap"]+df["shap_valueHead_FeltHat"]+df["shap_valueHeadGrade_Fresh"]+df["shap_valueHead_BoxFade"]+df["shap_valueHead_Betty"]+df["shap_valueHead_Bandana"]+df["shap_valueHead_BlackCap"]+df["shap_valueHead_BantuKnot"]+df["shap_trait_countHeadGrade"]+df["shap_valueHeadGrade_Damaged"]).round(6)
    df["shap_valueEyes"] = (df["shap_valueEyes_Sleepy"]+df["shap_valueEyesGrade_Rotten"]+df["shap_trait_countEyes"]+df["shap_valueEyes_Surprised"]+df["shap_trait_countEyesGrade"]+df["shap_valueEyesGrade_Fresh"]+df["shap_valueEyes_ThickRim"]+df["shap_valueEyes_WhiteGlasses"]+df["shap_valueEyes_Lazy"]+df["shap_valueEyes_Reptile"]+df["shap_valueEyes_Spooky"]+df["shap_valueEyes_Scared"]).round(6)
    df["shap_valueBody"] = (df["shap_valueBody_PurpleFur"]+df["shap_valueBody_WhiteTee"]+df["shap_valueBody_SpottyFur"]+df["shap_trait_countBodyGrade"]+df["shap_valueBody_SilverPuffer"]+df["shap_valueBody_BlackDungarees"]+df["shap_trait_countBody"]+df["shap_valueBody_Suspenders"]+df["shap_valueBody_FancySuit"]+df["shap_valueBody_RedBaseball"]+df["shap_valueBodyGrade_Damaged"]+df["shap_valueBody_Fishnet"]+df["shap_valueBody_RedBowTie"]+df["shap_valueBody_BlueStripe"]+df["shap_valueBody_BlackSinglet"]+df["shap_valueBody_GreenShirt"]+df["shap_valueBody_BlackHoodie"]+df["shap_valueBody_BlackTurtleneck"]+df["shap_valueBody_DenimShirt"]+df["shap_valueBody_DenimDungarees"]+df["shap_valueBody_DarkGreyBrandTee"]+df["shap_valueBody_BlueTracksuit"]+df["shap_valueBody_Tatts"]+df["shap_valueBody_PurpleButtonup"]+df["shap_valueBody_OrangePuffvest"]).round(6)
    df["shap_valueBackground"] = (df["shap_valueBackground_Blue"]+df["shap_valueBackground_Charcoal"]+df["shap_valueBackground_Purple"]+df["shap_trait_countBackground"]).round(6)
    df["shap_valueMouthGrade"] = (df["shap_trait_countMouthGrade"]+df["shap_valueMouthGrade_Fresh"]).round(6)
    df["shap_valueMouth"] = (df["shap_trait_countMouth"]+df["shap_trait_countMouthGrade"]+df["shap_valueMouthGrade_Fresh"]+df["shap_valueMouth_Fang"]+df["shap_valueMouth_Surprised"]+df["shap_valueMouth_Skully"]+df["shap_valueMouth_GoldGrin"]).round(6)
    df["shap_valueHeadGrade"] = (df["shap_valueHeadGrade_Fresh"]+df["shap_trait_countHeadGrade"]+df["shap_valueHeadGrade_Damaged"]).round(6)
    df["shap_valueBodyGrade"] = (df["shap_trait_countBodyGrade"]+df["shap_valueBodyGrade_Damaged"]).round(6)
    df["shap_valueEyesGrade"] = (df["shap_valueEyesGrade_Rotten"]+df["shap_trait_countEyesGrade"]+df["shap_valueEyesGrade_Fresh"]).round(6)
    df["shap_valueOther"] = (df["shap_avg_daily_vol"]+df["shap_eth_price_usd"]+df["shap_num_sales"]).round(6)

    df = df[['token_id', 'pred_price',
        'shap_valueHead', 'shap_valueEyes',
        'shap_valueBody', 'shap_valueBackground',
        'shap_valueMouthGrade', 'shap_valueMouth',
        'shap_valueHeadGrade', 'shap_valueBodyGrade',
        'shap_valueEyesGrade', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'doodles'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_doodles_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countbackground', 'trait_countbody', 'trait_countface',
       'trait_counthair', 'trait_counthead', 'trait_countpiercing']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbackground', 'trait_countbody', 'trait_countface',
       'trait_counthair', 'trait_counthead', 'trait_countpiercing',
       'valuebackground', 'valuebody', 'valueface', 'valuehair', 'valuehead',
       'valuepiercing']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valuebackground', 'valuebody', 'valueface', 'valuehair', 'valuehead',
       'valuepiercing']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbackground', 'trait_countbody', 'trait_countface',
        'trait_counthair', 'trait_counthead', 'trait_countpiercing']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countbackground', 'trait_countbody', 'trait_countface',
        'trait_counthair', 'trait_counthead', 'trait_countpiercing']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valuehair_ape',
    'trait_counthead',
    'min_trait_count',
    'avg_daily_vol',
    'eth_price_usd',
    'traits_number',
    'trait_counthair',
    'trait_countbody',
    'valuebody_skelly',
    'valuehair_crown',
    'trait_countbackground',
    'valueface_ape',
    'valuehead_ape',
    'trait_countface',
    'valueface_whale',
    'valuehair_shaved',
    'valueface_madnote',
    'valuehead_skelly',
    'valueface_cat',
    'valueface_aviatorswithmustache',
    'valuebody_cat',
    'valuehair_bedhead',
    'valueface_rainbowpuke',
    'valuehead_cat',
    'valueface_bluecheck',
    'valuehair_pinkbucketcap',
    'valuepiercing_airpod',
    'valueface_cobainglasses',
    'valuehair_wizard',
    'valuebody_leopardhoodie',
    'valuehair_cat',
    'valuehair_halo'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
    'shap_valuehair_ape', 'shap_trait_counthead', 'shap_min_trait_count',
    'shap_avg_daily_vol', 'shap_eth_price_usd', 'shap_traits_number',
    'shap_trait_counthair', 'shap_trait_countbody', 'shap_valuebody_skelly',
    'shap_valuehair_crown', 'shap_trait_countbackground',
    'shap_valueface_ape', 'shap_valuehead_ape', 'shap_trait_countface',
    'shap_valueface_whale', 'shap_valuehair_shaved',
    'shap_valueface_madnote', 'shap_valuehead_skelly', 'shap_valueface_cat',
    'shap_valueface_aviatorswithmustache', 'shap_valuebody_cat',
    'shap_valuehair_bedhead', 'shap_valueface_rainbowpuke',
    'shap_valuehead_cat', 'shap_valueface_bluecheck',
    'shap_valuehair_pinkbucketcap', 'shap_valuepiercing_airpod',
    'shap_valueface_cobainglasses', 'shap_valuehair_wizard',
    'shap_valuebody_leopardhoodie', 'shap_valuehair_cat',
    'shap_valuehair_halo'
    ]]

    df["shap_valuebackground"] = (df['shap_trait_countbackground']).round(6)
    df["shap_valuebody"] = (df["shap_valuebody_cat"]+df['shap_valuebody_leopardhoodie']+df['shap_valuebody_skelly']+df['shap_trait_countbody']).round(6)
    df["shap_valueface"] = (df["shap_valueface_cobainglasses"]+df["shap_valueface_bluecheck"]+df["shap_valuebody_cat"]+df["shap_valueface_madnote"]+df["shap_valueface_cat"]+df['shap_valueface_ape']+df['shap_valueface_rainbowpuke']+df['shap_valueface_whale']+df['shap_trait_countface']).round(6)
    df["shap_valuehair"] = (df["shap_valuehair_halo"]+df["shap_valuehair_wizard"]+df["shap_valuehair_pinkbucketcap"]+df["shap_valuehair_shaved"]+df["shap_valuehair_crown"]+df['shap_valuehair_bedhead']+df['shap_valuehair_ape']+df['shap_valuehair_cat']+df['shap_trait_counthair']).round(6)
    df["shap_valuepiercing"] = (df["shap_valuepiercing_airpod"]).round(6)
    df["shap_valuehead"] = (df["shap_valuehead_cat"]+df["shap_valuehead_skelly"]+df['shap_valuehead_ape']+df['shap_trait_counthead']).round(6)
    df["shap_valueOther"] = (df["shap_min_trait_count"]+df['shap_avg_daily_vol']+df['shap_traits_number']+df['shap_eth_price_usd']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valuebackground', 'shap_valuebody',
    'shap_valueface', 'shap_valuehair',
    'shap_valuepiercing', 'shap_valuehead',
    'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'hashmasks'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_hashmasks_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countCharacter', 'trait_countEyeColor',
       'trait_countGlyph', 'trait_countItem', 'trait_countMask',
       'trait_countSet', 'trait_countSkinColor']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCharacter', 'trait_countEyeColor',
       'trait_countGlyph', 'trait_countItem', 'trait_countMask',
       'trait_countSet', 'trait_countSkinColor',
       'valueBackground', 'valueCharacter', 'valueEyeColor', 'valueGlyph',
       'valueItem', 'valueMask', 'valueSet', 'valueSkinColor']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground', 'valueCharacter', 'valueEyeColor', 'valueGlyph',
       'valueItem', 'valueMask', 'valueSet', 'valueSkinColor']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCharacter', 'trait_countEyeColor',
       'trait_countGlyph', 'trait_countItem', 'trait_countMask',
       'trait_countSet', 'trait_countSkinColor']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countCharacter', 'trait_countEyeColor',
       'trait_countGlyph', 'trait_countItem', 'trait_countMask',
       'trait_countSet', 'trait_countSkinColor']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueMask_Unique',
   'valueSkinColor_Transparent',
   'valueSkinColor_Freak',
   'trait_countMask',
   'trait_countSet',
   'trait_countSkinColor',
   'trait_countCharacter',
   'valueSet_TheGoldenRecord',
   'valueSet_Crown',
   'min_trait_count',
   'valueMask_Chinese',
   'valueItem_GoldenToiletPaper',
   'valueItem_NoItem',
   'valueGlyph_EgyptianHieroglyph',
   'valueCharacter_GoldenRobot',
   'trait_countItem',
   'valueSkinColor_Dark',
   'valueCharacter_Mystical',
   'avg_daily_vol',
   'valueSet_FrElise',
   'eth_price_usd',
   'valueSkinColor_Wood',
   'valueCharacter_Puppet',
   'valueBackground_Doodle',
   'trait_countBackground',
   'valueCharacter_Robot',
   'valueSkinColor_Blue',
   'valueItem_ToiletPaper',
   'valueEyeColor_Blue',
   'valueSet_Halo',
   'valueMask_Doodle',
   'valueSet_Lucy',
   'valueSkinColor_Steel',
   'valueSkinColor_Light',
   'valueSet_Phoenix',
   'valueMask_Animal',
   'trait_countEyeColor',
   'num_sales'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred, check_additivity=False)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_valueMask_Unique', 'shap_valueSkinColor_Transparent',
      'shap_valueSkinColor_Freak', 'shap_trait_countMask',
      'shap_trait_countSet', 'shap_trait_countSkinColor',
      'shap_trait_countCharacter', 'shap_valueSet_TheGoldenRecord',
      'shap_valueSet_Crown', 'shap_min_trait_count', 'shap_valueMask_Chinese',
      'shap_valueItem_GoldenToiletPaper', 'shap_valueItem_NoItem',
      'shap_valueGlyph_EgyptianHieroglyph', 'shap_valueCharacter_GoldenRobot',
      'shap_trait_countItem', 'shap_valueSkinColor_Dark',
      'shap_valueCharacter_Mystical', 'shap_avg_daily_vol',
      'shap_valueSet_FrElise', 'shap_eth_price_usd',
      'shap_valueSkinColor_Wood', 'shap_valueCharacter_Puppet',
      'shap_valueBackground_Doodle', 'shap_trait_countBackground',
      'shap_valueCharacter_Robot', 'shap_valueSkinColor_Blue',
      'shap_valueItem_ToiletPaper', 'shap_valueEyeColor_Blue',
      'shap_valueSet_Halo', 'shap_valueMask_Doodle', 'shap_valueSet_Lucy',
      'shap_valueSkinColor_Steel', 'shap_valueSkinColor_Light',
      'shap_valueSet_Phoenix', 'shap_valueMask_Animal',
      'shap_trait_countEyeColor', 'shap_num_sales'
    ]]

    df["shap_valueMask"] = (df["shap_valueMask_Unique"]+df["shap_trait_countMask"]+df["shap_valueMask_Chinese"]+df["shap_valueMask_Doodle"]+df["shap_valueMask_Animal"]).round(6)
    df["shap_valueSkinColor"] = (df["shap_valueSkinColor_Transparent"]+df["shap_valueSkinColor_Freak"]+df["shap_trait_countSkinColor"]+df["shap_valueSkinColor_Dark"]+df["shap_valueSkinColor_Wood"]+df["shap_valueSkinColor_Blue"]+df["shap_valueSkinColor_Steel"]+df["shap_valueSkinColor_Light"]).round(6)
    df["shap_valueSet"] = (df["shap_trait_countSet"]+df["shap_valueSet_TheGoldenRecord"]+df["shap_valueSet_Crown"]+df["shap_valueSet_FrElise"]+df["shap_valueSet_Halo"]+df["shap_valueSet_Lucy"]+df["shap_valueSet_Phoenix"]).round(6)
    df["shap_valueCharacter"] = (df["shap_trait_countCharacter"]+df["shap_valueCharacter_GoldenRobot"]+df["shap_valueCharacter_Mystical"]+df["shap_valueCharacter_Puppet"]+df["shap_valueCharacter_Robot"]).round(6)
    df["shap_valueItem"] = (df["shap_valueItem_GoldenToiletPaper"]+df["shap_valueItem_NoItem"]+df["shap_trait_countItem"]+df["shap_valueItem_ToiletPaper"]).round(6)
    df["shap_valueBackground"] = (df["shap_valueBackground_Doodle"]+df["shap_trait_countBackground"]).round(6)
    df["shap_valueEyeColor"] = (df["shap_valueEyeColor_Blue"]+df["shap_trait_countEyeColor"]).round(6)
    df["shap_valueOther"] = (df["shap_min_trait_count"]+df["shap_avg_daily_vol"]+df["shap_eth_price_usd"]+df["shap_num_sales"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueMask', 'shap_valueSkinColor',
    'shap_valueSet', 'shap_valueCharacter',
    'shap_valueItem', 'shap_valueBackground',
    'shap_valueEyeColor', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'karafuru'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_karafuru_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countBase', 'trait_countCompanion',
       'trait_countEyes', 'trait_countHeadgear', 'trait_countLegendary',
       'trait_countMouth', 'trait_countOutfit', 'trait_countSkin']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase', 'trait_countCompanion',
       'trait_countEyes', 'trait_countHeadgear', 'trait_countLegendary',
       'trait_countMouth', 'trait_countOutfit', 'trait_countSkin',
       'valueBackground', 'valueBase', 'valueCompanion', 'valueEyes',
       'valueHeadgear', 'valueLegendary', 'valueMouth', 'valueOutfit',
       'valueSkin']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground', 'valueBase', 'valueCompanion', 'valueEyes',
       'valueHeadgear', 'valueLegendary', 'valueMouth', 'valueOutfit',
       'valueSkin']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase', 'trait_countCompanion',
       'trait_countEyes', 'trait_countHeadgear', 'trait_countLegendary',
       'trait_countMouth', 'trait_countOutfit', 'trait_countSkin']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase', 'trait_countCompanion',
       'trait_countEyes', 'trait_countHeadgear', 'trait_countLegendary',
       'trait_countMouth', 'trait_countOutfit', 'trait_countSkin']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueLegendary_Kaiboxing',
      'trait_countBase',
      'valueLegendary_Shirokushi',
      'min_trait_count',
      'valueBase_Ikai',
      'valueLegendary_Furuji',
      'eth_price_usd',
      'valueLegendary_Gojigames',
      'valueLegendary_Jiisame',
      'valueLegendary_MonkeyKai',
      'valueLegendary_FutatsuNoKai',
      'valueLegendary_GojizardX',
      'valueLegendary_Zabimaru',
      'valueLegendary_Mekaji',
      'valueOutfit_Cosplay',
      'num_sales',
      'valueLegendary_Togoji',
      'valueLegendary_CyberKai',
      'valueOutfit_MegaJacketWhite',
      'valueBackground_Dusk',
      'valueLegendary_Sukaijan',
      'valueLegendary_Goruruji',
      'valueLegendary_Okashiji',
      'valueLegendary_Kurokai',
      'valueLegendary_Gojibat',
      'valueLegendary_Kaigutsuchi',
      'trait_countOutfit',
      'trait_countLegendary',
      'valueLegendary_Fuyuji',
      'valueLegendary_KX78',
      'valueLegendary_KaRian',
      'valueLegendary_Phoenix',
      'trait_countCompanion'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_valueLegendary_Kaiboxing', 'shap_trait_countBase',
      'shap_valueLegendary_Shirokushi', 'shap_min_trait_count',
      'shap_valueBase_Ikai', 'shap_valueLegendary_Furuji',
      'shap_eth_price_usd', 'shap_valueLegendary_Gojigames',
      'shap_valueLegendary_Jiisame', 'shap_valueLegendary_MonkeyKai',
      'shap_valueLegendary_FutatsuNoKai', 'shap_valueLegendary_GojizardX',
      'shap_valueLegendary_Zabimaru', 'shap_valueLegendary_Mekaji',
      'shap_valueOutfit_Cosplay', 'shap_num_sales',
      'shap_valueLegendary_Togoji', 'shap_valueLegendary_CyberKai',
      'shap_valueOutfit_MegaJacketWhite', 'shap_valueBackground_Dusk',
      'shap_valueLegendary_Sukaijan', 'shap_valueLegendary_Goruruji',
      'shap_valueLegendary_Okashiji', 'shap_valueLegendary_Kurokai',
      'shap_valueLegendary_Gojibat', 'shap_valueLegendary_Kaigutsuchi',
      'shap_trait_countOutfit', 'shap_trait_countLegendary',
      'shap_valueLegendary_Fuyuji', 'shap_valueLegendary_KX78',
      'shap_valueLegendary_KaRian', 'shap_valueLegendary_Phoenix',
      'shap_trait_countCompanion'
    ]]

    df["shap_valueLegendary"] = (df["shap_valueLegendary_Kaiboxing"]+df["shap_valueLegendary_Shirokushi"]+df["shap_valueLegendary_Furuji"]+df["shap_valueLegendary_Gojigames"]+df["shap_valueLegendary_Jiisame"]+df["shap_valueLegendary_MonkeyKai"]+df["shap_valueLegendary_FutatsuNoKai"]+df["shap_valueLegendary_GojizardX"]+df["shap_valueLegendary_Zabimaru"]+df["shap_valueLegendary_Mekaji"]+df["shap_valueLegendary_Togoji"]+df["shap_valueLegendary_CyberKai"]+df["shap_valueLegendary_Sukaijan"]+df["shap_valueLegendary_Goruruji"]+df["shap_valueLegendary_Okashiji"]+df["shap_valueLegendary_Kurokai"]+df["shap_valueLegendary_Gojibat"]+df["shap_valueLegendary_Kaigutsuchi"]+df["shap_trait_countLegendary"]+df["shap_valueLegendary_Fuyuji"]+df["shap_valueLegendary_KX78"]+df["shap_valueLegendary_KaRian"]+df["shap_valueLegendary_Phoenix"]).round(6)
    df["shap_valueBase"] = (df["shap_trait_countBase"]+df["shap_valueBase_Ikai"]).round(6)
    df["shap_valueOutfit"] = (df["shap_valueOutfit_Cosplay"]+df["shap_valueOutfit_MegaJacketWhite"]+df["shap_trait_countOutfit"]).round(6)
    df["shap_valueBackground"] = (df["shap_valueBackground_Dusk"]).round(6)
    df["shap_valueOther"] = (df["shap_min_trait_count"]+df["shap_eth_price_usd"]+df["shap_num_sales"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueLegendary', 'shap_valueBase',
    'shap_valueOutfit', 'shap_valueBackground',
    'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_meebits_assets` a
    CROSS JOIN  
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBeard', 'trait_countBeardColor',
    'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
    'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
    'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
    'trait_countOvershirt', 'trait_countOvershirtColor',
    'trait_countPantsColor', 'trait_countShirtColor',
    'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
    'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
    'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
    'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
    'trait_countOvershirt', 'trait_countOvershirtColor',
    'trait_countPantsColor', 'trait_countShirtColor',
    'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType', 
    'valueBeard','valueBeardColor','valueEarring','valueGlasses','valueGlassesColor','valueHairColor',
    'valueHairStyle','valueHat','valueHatColor','valueJerseyNumber','valueNecklace','valueOvershirt',
    'valueOvershirtColor','valuePants','valuePantsColor',
    'valueShirt','valueShirtColor','valueShoes','valueShoesColor','valueTattooMotif','valueType']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBeard','valueBeardColor','valueEarring','valueGlasses','valueGlassesColor','valueHairColor',
    'valueHairStyle','valueHat','valueHatColor','valueNecklace','valueOvershirt',
    'valueOvershirtColor','valuePants','valuePantsColor',
    'valueShirt','valueShirtColor','valueShoes','valueShoesColor','valueType']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
    'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
    'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
    'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
    'trait_countOvershirt', 'trait_countOvershirtColor',
    'trait_countPantsColor', 'trait_countShirtColor',
    'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
    'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
    'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
    'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
    'trait_countOvershirt', 'trait_countOvershirtColor',
    'trait_countPantsColor', 'trait_countShirtColor',
    'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = 'meebits_xgb_model'
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/meebits/{0}.json".format(model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueType_DoublePig',
    'valueType_Dissected',
    'valueType_Visitor',
    'valueShirt_StylizedHoodie',
    'valueShirt_CGAShirt',
    'min_trait_count',
    'valueType_Human',
    'valueType_Robot',
    'valueType_Pig',
    'valueType_Skeleton',
    'eth_price_usd',
    'valueHairStyle_Bald',
    'valueShirt_Hoodie',
    'valueType_Elephant',
    'valuePantsColor_DarkRed',
    'valueGlasses_3D',
    'valueShirt_SkullTee',
    'valueShirtColor_Yellow',
    'valueBeardColor_Blond',
    'valueShirtColor_Black',
    'valueShoesColor_White',
    'valueHatColor_Black',
    'valueShirt_HoodieUp',
    'valueBeard_MedicalMask',
    'valueShoes_Basketball',
    'avg_daily_vol',
    'trait_countPantsColor',
    'valueShoes_Sneakers',
    'trait_countBeardColor',
    'valueShirt_PunkTee',
    'valuePants_CargoPants',
    'traits_number',
    'valuePantsColor_Gray',
    'valueOvershirtColor_Posh',
    'trait_countShirtColor',
    'trait_countTattooMotif',
    'valueHat_Cap',
    'valuePantsColor_DarkGray',
    'valuePantsColor_Camo',
    'valueHat_TruckerCap',
    'valueShoes_NeonSneakers',
    'valueBeard_Big',
    'valueShoes_Running',
    'valueShirtColor_Camo',
    'valueShoes_Classic',
    'trait_countHairStyle',
    'trait_countOvershirtColor',
    'valueShirtColor_Posh',
    'trait_countGlasses',
    'valueShoesColor_Purple',
    'valueShirt_TiedyedTee',
    'valueNecklace_GoldChain',
    'valueShoes_LL86',
    'valuePantsColor_Purple',
    'valueShirt_Jersey',
    'trait_countHatColor',
    'valuePants_SuitPants',
    'valueHatColor_Gray',
    'valueShirt_InvaderTee',
    'valueShoes_LLMoonboots'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
       'shap_valueType_DoublePig', 'shap_valueType_Dissected',
       'shap_valueType_Visitor', 'shap_valueShirt_StylizedHoodie',
       'shap_valueShirt_CGAShirt', 'shap_min_trait_count',
       'shap_valueType_Human', 'shap_valueType_Robot', 'shap_valueType_Pig',
       'shap_valueType_Skeleton', 'shap_eth_price_usd',
       'shap_valueHairStyle_Bald', 'shap_valueShirt_Hoodie',
       'shap_valueType_Elephant', 'shap_valuePantsColor_DarkRed',
       'shap_valueGlasses_3D', 'shap_valueShirt_SkullTee',
       'shap_valueShirtColor_Yellow', 'shap_valueBeardColor_Blond',
       'shap_valueShirtColor_Black', 'shap_valueShoesColor_White',
       'shap_valueHatColor_Black', 'shap_valueShirt_HoodieUp',
       'shap_valueBeard_MedicalMask', 'shap_valueShoes_Basketball',
       'shap_avg_daily_vol', 'shap_trait_countPantsColor',
       'shap_valueShoes_Sneakers', 'shap_trait_countBeardColor',
       'shap_valueShirt_PunkTee', 'shap_valuePants_CargoPants',
       'shap_traits_number', 'shap_valuePantsColor_Gray',
       'shap_valueOvershirtColor_Posh', 'shap_trait_countShirtColor',
       'shap_trait_countTattooMotif', 'shap_valueHat_Cap',
       'shap_valuePantsColor_DarkGray', 'shap_valuePantsColor_Camo',
       'shap_valueHat_TruckerCap', 'shap_valueShoes_NeonSneakers',
       'shap_valueBeard_Big', 'shap_valueShoes_Running',
       'shap_valueShirtColor_Camo', 'shap_valueShoes_Classic',
       'shap_trait_countHairStyle', 'shap_trait_countOvershirtColor',
       'shap_valueShirtColor_Posh', 'shap_trait_countGlasses',
       'shap_valueShoesColor_Purple', 'shap_valueShirt_TiedyedTee',
       'shap_valueNecklace_GoldChain', 'shap_valueShoes_LL86',
       'shap_valuePantsColor_Purple', 'shap_valueShirt_Jersey',
       'shap_trait_countHatColor', 'shap_valuePants_SuitPants',
       'shap_valueHatColor_Gray', 'shap_valueShirt_InvaderTee',
       'shap_valueShoes_LLMoonboots'
    ]]

    df["shap_valueType"] = (df['shap_valueType_DoublePig']+df['shap_valueType_Dissected']+df['shap_valueType_Visitor']+df['shap_valueType_Human']+df['shap_valueType_Robot']+df['shap_valueType_Pig']+df['shap_valueType_Skeleton']+df['shap_valueType_Elephant']).round(6)
    df["shap_valueShirt"] = (df['shap_valueType_DoublePig']+df['shap_valueType_Dissected']+df['shap_valueType_Visitor']+df['shap_valueType_Human']+df['shap_valueType_Robot']+df['shap_valueType_Pig']+df['shap_valueType_Skeleton']+df['shap_valueType_Elephant']).round(6)
    df["shap_valuePants"] = (df['shap_valuePantsColor_DarkRed']+df['shap_trait_countPantsColor']+df['shap_valuePants_CargoPants']+df['shap_valuePantsColor_Gray']+df['shap_valuePantsColor_DarkGray']+df['shap_valuePantsColor_Camo']+df['shap_valuePantsColor_Purple']+df['shap_valuePants_SuitPants']).round(6)
    df["shap_valueShoes"] = (df['shap_valueShoesColor_White']+df['shap_valueShoes_Basketball']+df['shap_valueShoes_Sneakers']+df['shap_valueShoes_NeonSneakers']+df['shap_valueShoes_Running']+df['shap_valueShoes_Classic']+df['shap_valueShoesColor_Purple']+df['shap_valueShoes_LL86']+df['shap_valueShoes_LLMoonboots']).round(6)
    df["shap_valueOvershirt"] = (df['shap_valueOvershirtColor_Posh']+df['shap_trait_countOvershirtColor']).round(6)
    df["shap_valueHat"] = (df['shap_valueHatColor_Black']+df['shap_valueHat_Cap']+df['shap_valueHat_TruckerCap']+df['shap_trait_countHatColor']+df['shap_valueHatColor_Gray']).round(6)
    df["shap_valueHair"] = (df['shap_valueHairStyle_Bald']+df['shap_trait_countHairStyle']).round(6)
    df["shap_valueBeard"] = (df['shap_valueBeardColor_Blond']+df['shap_valueBeard_MedicalMask']+df['shap_trait_countBeardColor']+df['shap_valueBeard_Big']).round(6)
    df["shap_valueGlasses"] = (df['shap_valueGlasses_3D']+df['shap_trait_countGlasses']).round(6)
    df["shap_valueTattoo"] = (df['shap_trait_countTattooMotif']).round(6)
    df["shap_valueOther"] = (df['shap_min_trait_count']+df['shap_eth_price_usd']+df['shap_avg_daily_vol']+df['shap_traits_number']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueType', 'shap_valueShirt',
    'shap_valuePants', 'shap_valueShoes',
    'shap_valueOvershirt', 'shap_valueHat',
    'shap_valueHair', 'shap_valueBeard',
    'shap_valueGlasses',
    'shap_valueTattoo', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/meebits.csv'
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'mfers'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_mfers_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_count1_1', 'trait_count420watch', 'trait_countbackground',
        'trait_countbeard', 'trait_countchain', 'trait_counteyes',
        'trait_counthatoverheadphones', 'trait_counthatunderheadphones',
        'trait_countheadphones', 'trait_countlonghair', 'trait_countmouth',
        'trait_countshirt', 'trait_countshorthair', 'trait_countsmoke',
        'trait_counttype']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_count1_1', 'trait_count420watch', 'trait_countbackground',
       'trait_countbeard', 'trait_countchain', 'trait_counteyes',
       'trait_counthatoverheadphones', 'trait_counthatunderheadphones',
       'trait_countheadphones', 'trait_countlonghair', 'trait_countmouth',
       'trait_countshirt', 'trait_countshorthair', 'trait_countsmoke',
       'trait_counttype',
       'value1_1', 'value420watch', 'valuebackground',
       'valuebeard', 'valuechain', 'valueeyes', 'valuehatoverheadphones',
       'valuehatunderheadphones', 'valueheadphones', 'valuelonghair',
       'valuemouth', 'valueshirt', 'valueshorthair', 'valuesmoke',
       'valuetype']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['value1_1', 'value420watch', 'valuebackground',
       'valuebeard', 'valuechain', 'valueeyes', 'valuehatoverheadphones',
       'valuehatunderheadphones', 'valueheadphones', 'valuelonghair',
       'valuemouth', 'valueshirt', 'valueshorthair', 'valuesmoke',
       'valuetype']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_count1_1', 'trait_count420watch', 'trait_countbackground',
       'trait_countbeard', 'trait_countchain', 'trait_counteyes',
       'trait_counthatoverheadphones', 'trait_counthatunderheadphones',
       'trait_countheadphones', 'trait_countlonghair', 'trait_countmouth',
       'trait_countshirt', 'trait_countshorthair', 'trait_countsmoke',
       'trait_counttype']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_count1_1', 'trait_count420watch', 'trait_countbackground',
       'trait_countbeard', 'trait_countchain', 'trait_counteyes',
       'trait_counthatoverheadphones', 'trait_counthatunderheadphones',
       'trait_countheadphones', 'trait_countlonghair', 'trait_countmouth',
       'trait_countshirt', 'trait_countshorthair', 'trait_countsmoke',
       'trait_counttype']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueheadphones_blueheadphones',
    'trait_count1_1',
    'trait_countbackground',
    'value1_1_cdbmfer',
    'valuehatoverheadphones_hoodie',
    'trait_counthatoverheadphones',
    'avg_daily_vol',
    'valuebackground_tree',
    'trait_counttype',
    'trait_counteyes',
    'valuehatunderheadphones_headbandbluered',
    'valuehatoverheadphones_tophat',
    'value1_1_punkmfer',
    'eth_price_usd',
    'value1_1_squigglymfer',
    'min_trait_count'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
        'shap_valueheadphones_blueheadphones', 'shap_trait_count1_1',
        'shap_trait_countbackground', 'shap_value1_1_cdbmfer',
        'shap_valuehatoverheadphones_hoodie',
        'shap_trait_counthatoverheadphones', 'shap_avg_daily_vol',
        'shap_valuebackground_tree', 'shap_trait_counttype',
        'shap_trait_counteyes', 'shap_valuehatunderheadphones_headbandbluered',
        'shap_valuehatoverheadphones_tophat', 'shap_value1_1_punkmfer',
        'shap_eth_price_usd', 'shap_value1_1_squigglymfer',
        'shap_min_trait_count'
    ]]

    df["shap_valueheadphones"] = (df["shap_valueheadphones_blueheadphones"]).round(6)
    df["shap_valuebackground"] = (df["shap_trait_countbackground"]+df["shap_valuebackground_tree"]).round(6)
    df["shap_value1_1"] = (df["shap_trait_count1_1"]+df["shap_value1_1_cdbmfer"]+df["shap_value1_1_punkmfer"]+df["shap_value1_1_squigglymfer"]).round(6)
    df["shap_valuehatoverheadphones"] = (df["shap_valuehatoverheadphones_hoodie"]+df["shap_trait_counthatoverheadphones"]+df["shap_valuehatoverheadphones_tophat"]).round(6)
    df["shap_valueeyes"] = (df["shap_trait_counteyes"]).round(6)
    df["shap_valuehatunderheadphones"] = (df["shap_valuehatunderheadphones_headbandbluered"]).round(6)
    df["shap_valuehatype"] = (df["shap_trait_counttype"]).round(6)
    df["shap_valueOther"] = (df["shap_avg_daily_vol"]+df["shap_eth_price_usd"]+df["shap_min_trait_count"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueheadphones', 'shap_valuebackground',
    'shap_value1_1', 'shap_valuehatoverheadphones',
    'shap_valueeyes', 'shap_valuehatunderheadphones',
    'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
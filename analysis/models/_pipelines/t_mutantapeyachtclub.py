import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'mutantapeyachtclub'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_mutantapeyachtclub_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countClothes', 'trait_countEarrings',
       'trait_countEyes', 'trait_countFur', 'trait_countHat',
       'trait_countMouth', 'trait_countName']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes', 'trait_countEarrings',
       'trait_countEyes', 'trait_countFur', 'trait_countHat',
       'trait_countMouth', 'trait_countName',
       'valueBackground',
       'valueClothes', 'valueEarrings', 'valueEyes', 'valueFur', 'valueHat',
       'valueMouth', 'valueName']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground',
         'valueClothes', 'valueEarrings', 'valueEyes', 'valueFur', 'valueHat',
         'valueMouth', 'valueName']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes', 'trait_countEarrings',
       'trait_countEyes', 'trait_countFur', 'trait_countHat',
       'trait_countMouth', 'trait_countName']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes', 'trait_countEarrings',
       'trait_countEyes', 'trait_countFur', 'trait_countHat',
       'trait_countMouth', 'trait_countName']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['traits_number',
      'avg_daily_vol',
      'valueClothes_M1SpaceSuit',
      'valueEarrings_M2Cross',
      'valueClothes_M1RainbowSuspenders',
      'valueName_MegaSwamp',
      'trait_countFur',
      'valueMouth_M2BoredPipe',
      'valueEyes_M2LaserEyes',
      'valueHat_M2KingsCrown',
      'valueName_MegaRadioactive',
      'valueClothes_M2BlackSuit',
      'valueClothes_M1Bandolier',
      'trait_countBackground',
      'min_trait_count',
      'valueName_MegaDemon',
      'valueEyes_M2BlueBeams',
      'valueFur_M2SolidGold',
      'valueEyes_M13d',
      'valueMouth_M2GrinMulticolored',
      'valueMouth_M2BoredUnshavenPizza',
      'trait_countEarrings',
      'eth_price_usd',
      'valueEyes_M2Sunglasses',
      'valueHat_M2SmHat',
      'valueClothes_M2Hawaiian',
      'valueClothes_M2BoneNecklace',
      'valueEyes_M1Closed',
      'valueMouth_M2GrinGoldGrill',
      'valueEyes_M2Cyborg',
      'valueMouth_M2BoredKazoo',
      'valueMouth_M1BoredCigar',
      'valueMouth_M1GrinMulticolored',
      'valueMouth_M2GrinDiamondGrill',
      'valueMouth_M1BoredUnshavenPipe',
      'valueFur_M2Trippy',
      'valueHat_M1BaycFlippedBrim',
      'valueFur_M2Noise',
      'valueClothes_M1BlackSuit',
      'trait_countMouth',
      'valueBackground_M2NewPunkBlue',
      'valueHat_M2LaurelWreath'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_traits_number', 'shap_avg_daily_vol',
      'shap_valueClothes_M1SpaceSuit', 'shap_valueEarrings_M2Cross',
      'shap_valueClothes_M1RainbowSuspenders', 'shap_valueName_MegaSwamp',
      'shap_trait_countFur', 'shap_valueMouth_M2BoredPipe',
      'shap_valueEyes_M2LaserEyes', 'shap_valueHat_M2KingsCrown',
      'shap_valueName_MegaRadioactive', 'shap_valueClothes_M2BlackSuit',
      'shap_valueClothes_M1Bandolier', 'shap_trait_countBackground',
      'shap_min_trait_count', 'shap_valueName_MegaDemon',
      'shap_valueEyes_M2BlueBeams', 'shap_valueFur_M2SolidGold',
      'shap_valueEyes_M13d', 'shap_valueMouth_M2GrinMulticolored',
      'shap_valueMouth_M2BoredUnshavenPizza', 'shap_trait_countEarrings',
      'shap_eth_price_usd', 'shap_valueEyes_M2Sunglasses',
      'shap_valueHat_M2SmHat', 'shap_valueClothes_M2Hawaiian',
      'shap_valueClothes_M2BoneNecklace', 'shap_valueEyes_M1Closed',
      'shap_valueMouth_M2GrinGoldGrill', 'shap_valueEyes_M2Cyborg',
      'shap_valueMouth_M2BoredKazoo', 'shap_valueMouth_M1BoredCigar',
      'shap_valueMouth_M1GrinMulticolored',
      'shap_valueMouth_M2GrinDiamondGrill',
      'shap_valueMouth_M1BoredUnshavenPipe', 'shap_valueFur_M2Trippy',
      'shap_valueHat_M1BaycFlippedBrim', 'shap_valueFur_M2Noise',
      'shap_valueClothes_M1BlackSuit', 'shap_trait_countMouth',
      'shap_valueBackground_M2NewPunkBlue', 'shap_valueHat_M2LaurelWreath'
    ]]

    df["shap_valueClothes"] = (df["shap_valueClothes_M1SpaceSuit"]+df["shap_valueClothes_M1RainbowSuspenders"]+df["shap_valueClothes_M2BlackSuit"]+df["shap_valueClothes_M1Bandolier"]+df["shap_valueClothes_M2Hawaiian"]+df["shap_valueClothes_M2BoneNecklace"]+df["shap_valueClothes_M1BlackSuit"]).round(6)
    df["shap_valueEarrings"] = (df["shap_valueEarrings_M2Cross"]+df["shap_trait_countEarrings"]).round(6)
    df["shap_valueName"] = (df["shap_valueName_MegaSwamp"]+df["shap_valueName_MegaRadioactive"]+df["shap_valueName_MegaDemon"]).round(6)
    df["shap_valueEyes"] = (df["shap_valueEyes_M2LaserEyes"]+df["shap_valueEyes_M2BlueBeams"]+df["shap_valueEyes_M13d"]+df["shap_valueEyes_M2Sunglasses"]+df["shap_valueEyes_M1Closed"]+df["shap_valueEyes_M2Cyborg"]).round(6)
    df["shap_valueMouth"] = (df["shap_valueMouth_M2BoredPipe"]+df["shap_valueMouth_M2GrinMulticolored"]+df["shap_valueMouth_M2BoredUnshavenPizza"]+df["shap_valueMouth_M2GrinGoldGrill"]+df["shap_valueMouth_M2BoredKazoo"]+df["shap_valueMouth_M1BoredCigar"]+df["shap_valueMouth_M1GrinMulticolored"]+df["shap_valueMouth_M2GrinDiamondGrill"]+df["shap_valueMouth_M1BoredUnshavenPipe"]+df["shap_trait_countMouth"]).round(6)
    df["shap_valueHat"] = (df["shap_valueHat_M2KingsCrown"]+df["shap_valueHat_M2SmHat"]+df["shap_valueHat_M1BaycFlippedBrim"]+df["shap_valueHat_M2LaurelWreath"]).round(6)
    df["shap_valueBackground"] = (df["shap_trait_countBackground"]+df["shap_valueBackground_M2NewPunkBlue"]).round(6)
    df["shap_valueFur"] = (df["shap_trait_countFur"]+df["shap_valueFur_M2SolidGold"]+df["shap_valueFur_M2Trippy"]+df["shap_valueFur_M2Noise"]).round(6)
    df["shap_valueOther"] = (df["shap_traits_number"]+df["shap_avg_daily_vol"]+df["shap_min_trait_count"]+df["shap_eth_price_usd"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueClothes', 'shap_valueEarrings',
    'shap_valueName', 'shap_valueEyes',
    'shap_valueMouth', 'shap_valueHat',
    'shap_valueBackground', 'shap_valueFur',
    'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
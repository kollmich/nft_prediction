import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'pudgypenguins'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_pudgypenguins_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countBody', 'trait_countFace',
       'trait_countHead', 'trait_countSkin']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countFace',
       'trait_countHead', 'trait_countSkin', 'valueBackground', 'valueBody',
       'valueFace', 'valueHead', 'valueSkin']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground', 'valueBody',
       'valueFace', 'valueHead', 'valueSkin']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countFace',
       'trait_countHead', 'trait_countSkin']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBody', 'trait_countFace',
       'trait_countHead', 'trait_countSkin']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueBackground_Green',
      'valueBackground_OnTheBeach',
      'min_trait_count',
      'trait_countSkin',
      'valueBody_Mirrored',
      'traits_number',
      'valueHead_PineappleSuit',
      'valueHead_FishGold',
      'valueFace_PillowCase',
      'valueSkin_Ice',
      'valueHead_Crown',
      'valueBody_PineappleSuit',
      'eth_price_usd',
      'valueSkin_NavyBlue',
      'valueBackground_TrickOrTreating',
      'valueHead_None',
      'valueHead_RiceHat',
      'valueBody_KimonoGold',
      'valueBackground_Pink',
      'valueBody_LeiBlue',
      'valueHead_BucketHatGreen',
      'valueFace_Scouter',
      'valueBody_PillowCase',
      'avg_daily_vol',
      'trait_countFace',
      'valueFace_VillainMask',
      'trait_countHead',
      'valueBody_LeiPink'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
      'shap_valueBackground_Green', 'shap_valueBackground_OnTheBeach',
      'shap_min_trait_count', 'shap_trait_countSkin',
      'shap_valueBody_Mirrored', 'shap_traits_number',
      'shap_valueHead_PineappleSuit', 'shap_valueHead_FishGold',
      'shap_valueFace_PillowCase', 'shap_valueSkin_Ice',
      'shap_valueHead_Crown', 'shap_valueBody_PineappleSuit',
      'shap_eth_price_usd', 'shap_valueSkin_NavyBlue',
      'shap_valueBackground_TrickOrTreating', 'shap_valueHead_None',
      'shap_valueHead_RiceHat', 'shap_valueBody_KimonoGold',
      'shap_valueBackground_Pink', 'shap_valueBody_LeiBlue',
      'shap_valueHead_BucketHatGreen', 'shap_valueFace_Scouter',
      'shap_valueBody_PillowCase', 'shap_avg_daily_vol',
      'shap_trait_countFace', 'shap_valueFace_VillainMask',
      'shap_trait_countHead', 'shap_valueBody_LeiPink'
    ]]

    df["shap_valueBackground"] = (df["shap_valueBackground_Green"]+df["shap_valueBackground_OnTheBeach"]+df["shap_valueBackground_TrickOrTreating"]+df["shap_valueBackground_Pink"]).round(6)
    df["shap_valueBody"] = (df["shap_valueBody_Mirrored"]+df["shap_valueBody_PineappleSuit"]+df["shap_valueBody_KimonoGold"]+df["shap_valueBody_LeiBlue"]+df["shap_valueBody_PillowCase"]+df["shap_valueBody_LeiPink"]).round(6)
    df["shap_valueHead"] = (df["shap_valueHead_PineappleSuit"]+df["shap_valueHead_FishGold"]+df["shap_valueHead_Crown"]+df["shap_valueHead_None"]+df["shap_valueHead_RiceHat"]+df["shap_valueHead_BucketHatGreen"]+df["shap_trait_countHead"]).round(6)
    df["shap_valueSkin"] = (df["shap_trait_countSkin"]+df["shap_valueSkin_Ice"]+df["shap_valueSkin_NavyBlue"]).round(6)
    df["shap_valueFace"] = (df["shap_valueFace_PillowCase"]+df["shap_valueFace_Scouter"]+df["shap_trait_countFace"]+df["shap_valueFace_VillainMask"]).round(6)
    df["shap_valueOther"] = (df["shap_min_trait_count"]+df["shap_avg_daily_vol"]+df["shap_eth_price_usd"]).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueBackground', 'shap_valueBody',
    'shap_valueHead', 'shap_valueSkin',
    'shap_valueFace', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
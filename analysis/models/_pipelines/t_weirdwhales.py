import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_weirdwhales_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countBase',
    'trait_countEyeAccessory', 'trait_countHeadgear', 'trait_countMouthAccessory']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase',
       'trait_countEyeAccessory', 'trait_countHeadgear', 'trait_countMouthAccessory', 
       'valueBackground','valueBase','valueEyeAccessory','valueHeadgear','valueMouthAccessory']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground','valueBase','valueEyeAccessory','valueHeadgear','valueMouthAccessory']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase',
    'trait_countEyeAccessory', 'trait_countHeadgear', 'trait_countMouthAccessory']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countBase',
    'trait_countEyeAccessory', 'trait_countHeadgear', 'trait_countMouthAccessory']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = 'weirdwhales_xgb_model'
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/weirdwhales/{0}.json".format(model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['trait_countBase',
        'traits_number',
        'valueHeadgear_PoliceCap',
        'eth_price_usd',
        'trait_countHeadgear',
        'avg_daily_vol',
        'valueBase_Alien',
        'valueBackground_Cyan',
        'valueBackground_Peach',
        'valueHeadgear_Cap',
        'valueBackground_Red',
        'trait_countBackground',
        'valueHeadgear_PilotHelmet',
        'valueHeadgear_DoRag',
        'valueEyeAccessory_ClownEyesBlue',
        'valueBackground_Purple',
        'valueBase_Zombie',
        'valueHeadgear_Fez',
        'valueEyeAccessory_EyeMask',
        'trait_countEyeAccessory',
        'trait_countMouthAccessory',
        'valueBackground_Yellow',
        'valueHeadgear_Tiara',
        'valueMouthAccessory_Cigarette',
        'valueEyeAccessory_GreenEyeShadow',
        'valueHeadgear_VikingHat'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
        'shap_trait_countBase', 'shap_traits_number',
        'shap_valueHeadgear_PoliceCap', 'shap_eth_price_usd',
        'shap_trait_countHeadgear', 'shap_avg_daily_vol',
        'shap_valueBase_Alien', 'shap_valueBackground_Cyan',
        'shap_valueBackground_Peach', 'shap_valueHeadgear_Cap',
        'shap_valueBackground_Red', 'shap_trait_countBackground',
        'shap_valueHeadgear_PilotHelmet', 'shap_valueHeadgear_DoRag',
        'shap_valueEyeAccessory_ClownEyesBlue', 'shap_valueBackground_Purple',
        'shap_valueBase_Zombie', 'shap_valueHeadgear_Fez',
        'shap_valueEyeAccessory_EyeMask', 'shap_trait_countEyeAccessory',
        'shap_trait_countMouthAccessory', 'shap_valueBackground_Yellow',
        'shap_valueHeadgear_Tiara', 'shap_valueMouthAccessory_Cigarette',
        'shap_valueEyeAccessory_GreenEyeShadow',
        'shap_valueHeadgear_VikingHat'
    ]]

    df["shap_valueBase"] = (df["shap_trait_countBase"]+df["shap_valueBase_Alien"]+df["shap_valueBase_Zombie"]).round(6)
    df["shap_valueHeadgear"] = (df["shap_valueHeadgear_PoliceCap"]+df["shap_trait_countHeadgear"]+df["shap_valueHeadgear_Cap"]+df["shap_valueHeadgear_PilotHelmet"]+df["shap_valueHeadgear_DoRag"]+df["shap_valueHeadgear_Fez"]+df["shap_valueHeadgear_Tiara"]+df["shap_valueHeadgear_VikingHat"]).round(6)
    df["shap_valueEyeAccessory"] = (df["shap_valueEyeAccessory_ClownEyesBlue"]+df["shap_valueEyeAccessory_EyeMask"]+df["shap_trait_countEyeAccessory"]+df["shap_valueEyeAccessory_GreenEyeShadow"]).round(6)
    df["shap_valueBackground"] = (df["shap_valueBackground_Cyan"]+df["shap_valueBackground_Peach"]+df["shap_valueBackground_Red"]+df["shap_trait_countBackground"]+df["shap_valueBackground_Purple"]+df["shap_valueBackground_Yellow"]).round(6)
    df["shap_valueMouthAccessory"] = (df["shap_trait_countMouthAccessory"]+df["shap_valueMouthAccessory_Cigarette"]).round(6)
    df["shap_valueOther"] = (df['shap_avg_daily_vol']+df['shap_eth_price_usd']+df['shap_traits_number']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueBase', 'shap_valueHeadgear',
    'shap_valueEyeAccessory', 'shap_valueBackground',
    'shap_valueMouthAccessory', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/weirdwhales.csv'
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
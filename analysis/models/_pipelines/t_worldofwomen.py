import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh

collection = 'worldofwomen'

def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    sql = """
    select *
    FROM `trendspotting-294718.production.d_worldofwomen_assets` a
    CROSS JOIN 
    (   
        SELECT SAFE_DIVIDE(count(distinct transactionHash) , COUNT(DISTINCT CAST(date_trunc(timeStamp, day) AS DATETIME))) avg_daily_vol
        FROM `trendspotting-294718.staging.f_opensea_match_orders`
        where CAST(date_trunc(timeStamp, month) AS DATETIME) = CAST(date_trunc(current_date(), month) AS DATETIME)
    ) b
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    df['min_trait_count'] = df[['trait_countBackground', 'trait_countClothes',
       'trait_countEarrings', 'trait_countEyes', 'trait_countFaceAccessories',
       'trait_countFacialFeatures', 'trait_countHairstyle', 'trait_countLipsColor',
       'trait_countMouth', 'trait_countNecklace', 'trait_countSkinTone']].min(axis=1)

    df = df.reset_index(drop=True)

    data = df[['sale_price', 'avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
        'trait_countEarrings', 'trait_countEyes', 'trait_countFaceAccessories',
        'trait_countFacialFeatures', 'trait_countHairstyle', 'trait_countLipsColor',
        'trait_countMouth', 'trait_countNecklace', 'trait_countSkinTone',
        'valueBackground','valueClothes','valueEarrings','valueEyes','valueFaceAccessories','valueFacialFeatures',
        'valueHairstyle','valueLipsColor','valueMouth','valueNecklace','valueSkinTone']]


    # Split the dataset into categorical and numerical fields, convert dates to numerics
    data = data.drop_duplicates(subset=None, keep='first')

    # df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

    categories = data[['valueBackground','valueClothes','valueEarrings','valueEyes','valueFaceAccessories','valueFacialFeatures',
        'valueHairstyle','valueLipsColor','valueMouth','valueNecklace','valueSkinTone']].fillna('None')

    target = data[['sale_price']]

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
        'trait_countEarrings', 'trait_countEyes', 'trait_countFaceAccessories',
        'trait_countFacialFeatures', 'trait_countHairstyle', 'trait_countLipsColor',
        'trait_countMouth', 'trait_countNecklace', 'trait_countSkinTone']]

    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()

    data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

    data = pd.get_dummies(data, columns = categories.columns)

    numericals = data[['avg_daily_vol', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBackground', 'trait_countClothes',
        'trait_countEarrings', 'trait_countEyes', 'trait_countFaceAccessories',
        'trait_countFacialFeatures', 'trait_countHairstyle', 'trait_countLipsColor',
        'trait_countMouth', 'trait_countNecklace', 'trait_countSkinTone']]

    import re
    data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    from xgboost import XGBRegressor
    model = XGBRegressor()
    model_path = '{0}_xgb_model'.format(collection)
    model.load_model("/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/{0}/{1}.json".format(collection,model_path))

    data_all = data.copy()
    data_all['eth_price_usd'] = data_all['eth_price_usd_current']

    data_pred = data_all[['valueSkinTone_NightGoddess',
    'valueNecklace_WoWCoin',
    'valueHairstyle_FingerWaves',
    'trait_countSkinTone',
    'valueEyes_BlueToTheLeft',
    'valueFacialFeatures_RoseTattoo',
    'valueEarrings_WoWCoins',
    'valueClothes_Tuxedo',
    'valueEyes_BrownToTheRight',
    'valueFacialFeatures_HeartTattoo',
    'valueClothes_Naiade',
    'avg_daily_vol',
    'valueHairstyle_LuckyGreen',
    'valueHairstyle_CurlyPearlUpdo',
    'trait_countClothes',
    'traits_number',
    'eth_price_usd',
    'valueFacialFeatures_FlashyBlue',
    'valueHairstyle_PurpleRainbow',
    'valueFacialFeatures_Leader',
    'trait_countBackground',
    'valueBackground_OrangeYellow',
    'num_sales',
    'valueFaceAccessories_RoundGlasses',
    'valueSkinTone_MediumOlive',
    'valueHairstyle_BlackAndWhite',
    'min_trait_count',
    'valueFaceAccessories_HypnoticGlasses',
    'valueFacialFeatures_PearlEyes',
    'valueSkinTone_Golden',
    'valueHairstyle_CurlyPonytail',
    'valueEarrings_FlowerPower',
    'trait_countNecklace',
    'valueFaceAccessories_RedRoundSunglasses',
    'valueHairstyle_Bun',
    'valueEyes_YellowToTheLeft',
    'valueFaceAccessories_OversizedStatementSunglasses',
    'valueLipsColor_Space',
    'valueEarrings_Pearls',
    'trait_countLipsColor',
    'valueBackground_GreenPurple'
    ]]

    shap_values = sh.TreeExplainer(model).shap_values(data_pred)
    shap = pd.DataFrame(shap_values, columns='shap_'+data_pred.columns, index= data_pred.index)
    shap = shap.round(6)
    shap['token_id'] = df.loc[:,'token_id']

    prediction_all = model.predict(data_pred)
    data_pred.loc[:,'pred_price'] = prediction_all.round(6)

    data_pred['token_id'] = df.loc[:,'token_id']
    df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
    df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)

    df = pd.merge(df,shap,left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))

    df = df[['token_id', 'pred_price',
    'shap_valueSkinTone_NightGoddess', 'shap_valueNecklace_WoWCoin',
    'shap_valueHairstyle_FingerWaves', 'shap_trait_countSkinTone',
    'shap_valueEyes_BlueToTheLeft', 'shap_valueFacialFeatures_RoseTattoo',
    'shap_valueEarrings_WoWCoins', 'shap_valueClothes_Tuxedo',
    'shap_valueEyes_BrownToTheRight',
    'shap_valueFacialFeatures_HeartTattoo', 'shap_valueClothes_Naiade',
    'shap_avg_daily_vol', 'shap_valueHairstyle_LuckyGreen',
    'shap_valueHairstyle_CurlyPearlUpdo', 'shap_trait_countClothes',
    'shap_traits_number', 'shap_eth_price_usd',
    'shap_valueFacialFeatures_FlashyBlue',
    'shap_valueHairstyle_PurpleRainbow', 'shap_valueFacialFeatures_Leader',
    'shap_trait_countBackground', 'shap_valueBackground_OrangeYellow',
    'shap_num_sales', 'shap_valueFaceAccessories_RoundGlasses',
    'shap_valueSkinTone_MediumOlive', 'shap_valueHairstyle_BlackAndWhite',
    'shap_min_trait_count', 'shap_valueFaceAccessories_HypnoticGlasses',
    'shap_valueFacialFeatures_PearlEyes', 'shap_valueSkinTone_Golden',
    'shap_valueHairstyle_CurlyPonytail', 'shap_valueEarrings_FlowerPower',
    'shap_trait_countNecklace',
    'shap_valueFaceAccessories_RedRoundSunglasses',
    'shap_valueHairstyle_Bun', 'shap_valueEyes_YellowToTheLeft',
    'shap_valueFaceAccessories_OversizedStatementSunglasses',
    'shap_valueLipsColor_Space', 'shap_valueEarrings_Pearls',
    'shap_trait_countLipsColor', 'shap_valueBackground_GreenPurple'
    ]]

    df["shap_valueEarrings"] = (df["shap_valueEarrings_WoWCoins"]+df["shap_valueEarrings_FlowerPower"]+df["shap_valueEarrings_Pearls"]).round(6)
    df["shap_valueNecklace"] = (df["shap_valueNecklace_WoWCoin"]+df['shap_trait_countNecklace']).round(6)
    df["shap_valueSkinTone"] = (df["shap_trait_countSkinTone"]+df["shap_valueSkinTone_NightGoddess"]+df['shap_valueSkinTone_Golden']+df['shap_valueSkinTone_MediumOlive']).round(6)
    df["shap_valueHairstyle"] = (df["shap_valueHairstyle_LuckyGreen"]+df['shap_valueHairstyle_PurpleRainbow']+df['shap_valueHairstyle_CurlyPonytail']+df['shap_valueHairstyle_FingerWaves']+df['shap_valueHairstyle_CurlyPearlUpdo']+df['shap_valueHairstyle_BlackAndWhite']+df['shap_valueHairstyle_Bun']).round(6)
    df["shap_valueBackground"] = (df["shap_trait_countBackground"]+df['shap_valueBackground_GreenPurple']+df['shap_valueBackground_OrangeYellow']).round(6)
    df["shap_valueFacialFeatures"] = (df["shap_valueFacialFeatures_HeartTattoo"]+df['shap_valueFacialFeatures_RoseTattoo']+df['shap_valueFacialFeatures_Leader']+df['shap_valueFacialFeatures_PearlEyes']+df['shap_valueFacialFeatures_FlashyBlue']).round(6)
    df["shap_valueClothes"] = (df["shap_valueClothes_Tuxedo"]+df['shap_valueClothes_Naiade']+df['shap_trait_countClothes']).round(6)
    df["shap_valueEyes"] = (df["shap_valueEyes_BlueToTheLeft"]+df['shap_valueEyes_BrownToTheRight']+df['shap_valueEyes_YellowToTheLeft']).round(6)
    df["shap_valueFaceAccessories"] = (df["shap_valueFaceAccessories_RedRoundSunglasses"]+df['shap_valueFaceAccessories_RoundGlasses']+df['shap_valueFaceAccessories_HypnoticGlasses']+df['shap_valueFaceAccessories_OversizedStatementSunglasses']).round(6)
    df["shap_valueLipsColor"] = (df["shap_trait_countLipsColor"]+df["shap_valueLipsColor_Space"]).round(6)
    df["shap_valueOther"] = (df["shap_avg_daily_vol"]+df['shap_min_trait_count']+df['shap_traits_number']+df['shap_num_sales']+df['shap_eth_price_usd']).round(6)

    df = df[['token_id', 'pred_price',
    'shap_valueEarrings', 'shap_valueNecklace',
    'shap_valueSkinTone', 'shap_valueHairstyle',
    'shap_valueBackground', 'shap_valueFacialFeatures',
    'shap_valueClothes', 'shap_valueEyes',
    'shap_valueFaceAccessories', 'shap_valueLipsColor', 'shap_valueOther'
    ]]

    path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/{0}.csv'.format(collection)
    path = '/Users/michalkollar/trendspotting_nft/data/models/{0}.csv'.format(model_path)
    df = df.drop_duplicates(subset=['token_id'], keep='first')
    df.to_csv(path, index=False, sep='\t')

    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}.csv'.format(model_path))
    blob.upload_from_filename(path)

if __name__ == '__main__':
    main()
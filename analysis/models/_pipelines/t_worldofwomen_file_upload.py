import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from datetime import timedelta
from google.cloud import storage
import shap as sh


def main():
    credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'
    )

    collection = 'worldofwomen'

    sql = """
    SELECT * EXCEPT(avg_daily_vol)
    FROM `trendspotting-294718.production.f_worldofwomen_assets`
    """

    df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)

    current_timestamp = pd.to_datetime('now')
    df['timestamp'] = current_timestamp
    df['timestamp'] = df['timestamp'].values.astype('<M8[s]')

    current_day = current_timestamp.day_name()
    print(current_day=='Tuesday')

    path_daily = '/Users/michalkollar/trendspotting_nft/data/visualisation_input/{0}_daily.csv'.format(collection)
    path_weekly = '/Users/michalkollar/trendspotting_nft/data/visualisation_input/{0}_weekly.csv'.format(collection)

    if current_day=='Sunday':
        df.to_csv(path_daily, index=False, sep='\t')
        df.to_csv(path_weekly, index=False, sep='\t')

        storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
        buckets = list(storage_client.list_buckets())
        bucket = storage_client.get_bucket("trendspotting_general_storage")
        blob = bucket.blob('dw/collections/visualisation_input/{0}_daily.csv'.format(collection))
        blob.upload_from_filename(path_daily)
        blob = bucket.blob('dw/collections/visualisation_input/{0}_weekly.csv'.format(collection))
        blob.upload_from_filename(path_weekly)
    else:
        df.to_csv(path_daily, index=False, sep='\t')

        storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
        buckets = list(storage_client.list_buckets())
        bucket = storage_client.get_bucket("trendspotting_general_storage")
        blob = bucket.blob('dw/collections/visualisation_input/{0}_daily.csv'.format(collection))
        blob.upload_from_filename(path_daily)

if __name__ == '__main__':
    main()

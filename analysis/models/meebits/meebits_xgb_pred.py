import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
import time
import shap
start_time = time.time()

credentials = service_account.Credentials.from_service_account_file(
    '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/gcp_secret.json',
)

sql = """
SELECT *
FROM `trendspotting-294718.production.d_meebits_assets`
"""

df = pandas_gbq.read_gbq(sql, project_id="trendspotting-294718", credentials=credentials)


df['min_trait_count'] = df[['trait_countBeard', 'trait_countBeardColor',
       'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
       'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
       'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
       'trait_countOvershirt', 'trait_countOvershirtColor',
       'trait_countPantsColor', 'trait_countShirtColor',
       'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']].min(axis=1)

df = df.reset_index(drop=True)

df.head(1)

data = df[['sale_price', 'month_index', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
       'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
       'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
       'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
       'trait_countOvershirt', 'trait_countOvershirtColor',
       'trait_countPantsColor', 'trait_countShirtColor',
       'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType', 
       'valueBeard','valueBeardColor','valueEarring','valueGlasses','valueGlassesColor','valueHairColor',
       'valueHairStyle','valueHat','valueHatColor','valueJerseyNumber','valueNecklace','valueOvershirt',
       'valueOvershirtColor','valuePants','valuePantsColor',
       'valueShirt','valueShirtColor','valueShoes','valueShoesColor','valueTattooMotif','valueType']]


# Split the dataset into categorical and numerical fields, convert dates to numerics
data = data.drop_duplicates(subset=None, keep='first')

# df['last_sale_date'] = pd.to_datetime(df['last_sale_date']).dt.to_period('M').dt.to_timestamp()

categories = data[['valueBeard','valueBeardColor','valueEarring','valueGlasses','valueGlassesColor','valueHairColor',
       'valueHairStyle','valueHat','valueHatColor','valueNecklace','valueOvershirt',
       'valueOvershirtColor','valuePants','valuePantsColor',
       'valueShirt','valueShirtColor','valueShoes','valueShoesColor','valueType']].fillna('None')

target = data[['sale_price']]

numericals = data[['month_index', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
       'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
       'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
       'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
       'trait_countOvershirt', 'trait_countOvershirtColor',
       'trait_countPantsColor', 'trait_countShirtColor',
       'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']]

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()

data.loc[:,numericals.columns] = scaler.fit_transform(data.loc[:,numericals.columns])

data = pd.get_dummies(data, columns = categories.columns)

numericals = data[['month_index', 'eth_price_usd', 'eth_price_usd_current','traits_number', 'num_sales', 'min_trait_count', 'trait_countBeard', 
       'trait_countBeardColor', 'trait_countEarring', 'trait_countGlasses', 'trait_countGlassesColor',
       'trait_countHairColor', 'trait_countHairStyle', 'trait_countHat',
       'trait_countHatColor', 'trait_countJerseyNumber', 'trait_countNecklace',
       'trait_countOvershirt', 'trait_countOvershirtColor',
       'trait_countPantsColor', 'trait_countShirtColor',
       'trait_countShoesColor', 'trait_countTattooMotif', 'trait_countType']]

import re
data = data.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

from xgboost import XGBRegressor
model = XGBRegressor()
model_path = 'meebits_xgb_model_MAE'
model.load_model("/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/models/{0}.json".format(model_path))

data_all = data.copy()
data_all['month_index'] = 0
data_all['eth_price_usd'] = data_all['eth_price_usd_current']

data_pred = data_all[['valueType_DoublePig','valueType_Visitor','trait_countType','valueType_Robot','valueShirt_CGAShirt','valueType_Human','valueShirt_GlyphShirt','valueType_Skeleton','valueShoes_NeonSneakers','valueShoes_LLAlien',
'valueBeardColor_Blond','valueHat_Headphones','valueGlasses_3D','valueOvershirtColor_Posh','valueNecklace_GoldChain','valueHairStyle_FieryMohawk','valueHairStyle_Bald','valueShirt_PunkTee','valueShirt_HoodieUp',
'valueShirtColor_Black','valueBeard_MedicalMask','valueType_Elephant','trait_countBeardColor','trait_countOvershirtColor','valueHat_TruckerCap','valueShoes_LL86','valueShoesColor_White',
'trait_countGlasses','month_index','eth_price_usd','min_trait_count','valueHatColor_Gray','valueOvershirtColor_White','traits_number','valueShirtColor_Gray','valueBeardColor_Silver','valueShirt_SnoutzHoodie','valuePants_RegularPants','valueType_Pig',
'valueShirt_FlamingoTee','valueGlassesColor_Charcoal','valuePantsColor_Camo','valueOvershirtColor_Argyle','valuePantsColor_BlueCamo','valueHat_Cap','valuePants_RippedJeans','valueShoes_Classic',
'num_sales','valuePants_SuitPants','valueShoes_LLMoonboots','valueBeard_Mustache','trait_countTattooMotif','valueShirtColor_White','valueHairColor_DyedRed','valuePants_CargoPants','valueShirtColor_BlueCamo',
'valueShirtColor_Camo','valueOvershirtColor_Luxe','valueShoesColor_Gray','valueShirt_TiedyedTee','valueShirt_Hoodie','valueHairStyle_Halfshaved','trait_countHairStyle','valueShoesColor_Purple','valueHat_SnoutzCap',
'valueBeard_BikerMustache','valueHairStyle_Mohawk','valueGlassesColor_White','valueShoesColor_Yellow','valueShirtColor_Argyle','valueShoes_Running','valueShirt_InvaderTee','trait_countHatColor','valueGlasses_Elvis',
'valueShoes_LLBabyBlue','valueHairColor_Blond','valueShirtColor_Green','valueHairColor_Blonde','valueHatColor_Red','valueEarring_GoldEarring','trait_countShoesColor','valueShirt_Hawaiian','trait_countHairColor',
'trait_countShirtColor','valueOvershirtColor_GreenPlaid','valueHatColor_Yellow','valueHat_BackwardsCap','valueHat_WoolHat','valueShoes_Canvas','valueShirt_SnoutzTee','valuePantsColor_Argyle','trait_countPantsColor',
'valueShirt_SkullTee','valueBeard_Muttonchops','valueHairColor_Blue','valueShirtColor_Yellow','valueShoes_LLRGB','valueShirt_GhostTee','valueHatColor_White','valueGlasses_Specs','valueOvershirtColor_BlueCamo',
'valueHatColor_Purple','valuePantsColor_Yellow','valueShirt_Jersey','valueShoes_Sneakers','valueShirtColor_Posh','valueShirt_StylizedHoodie','valueHairColor_Dark','valueOvershirtColor_RedPlaid','valueShirt_HeartTee',
'valueHairStyle_VeryLong','valueShoes_Basketball','valuePantsColor_DarkRed','valueHat_Brimmed','valuePants_Trackpants','valueShirt_Suit','valuePantsColor_Purple','valuePants_Skirt','valuePantsColor_DarkGray','trait_countOvershirt',
'valuePants_AthleticShorts','valueShirtColor_LeopardPrint','valueShoes_LLHighTops','valuePantsColor_GreenPlaid','trait_countNecklace','valueHat_Bandana','valueBeard_Big','valueHatColor_Black','valueOvershirtColor_Red','valueOvershirtColor_Black','valuePantsColor_Gray']]

data_pred.head(1)

prediction_all = model.predict(data_pred)
data_pred.loc[:,'pred_price'] = prediction_all


data_pred['token_id'] = df['token_id']
df = pd.merge(df,data_pred[['token_id','pred_price']],left_on='token_id',right_on='token_id',how='inner',copy=False, suffixes=('', '_y'))
df.drop(df.filter(regex='_y$').columns.tolist(),axis=1, inplace=True)


df = df[['token_id', 'image_url','sale_date','sale_price', 'eth_price_usd', 'num_sales',
       'traits_number', 'valueBeard', 'valueBeardColor', 'valueEarring',
       'valueGlasses', 'valueGlassesColor', 'valueHairColor', 'valueHairStyle',
       'valueHat', 'valueHatColor', 'valueJerseyNumber', 'valueNecklace',
       'valueOvershirt', 'valueOvershirtColor', 'valuePants',
       'valuePantsColor', 'valueShirt', 'valueShirtColor', 'valueShoes',
       'valueShoesColor', 'valueTattooMotif', 'valueType', 'min_trait_count',
       'pred_price']]

df.head(1)

path = '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/output/meebits.csv'
df.to_csv(path, index=False, sep='\t')
print("--- %s seconds ---" % (time.time() - start_time))


import pandas as pd
import time
import matplotlib.pyplot as plt
import matplotlib as mpl
plt.rc("font", size=14)
import seaborn as sns
sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)
plt.style.use('fivethirtyeight')
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=["#B05E55"])
import plotly.graph_objects as go
import plotly.io as pio


colour_names = ['yellow','red','cyan','brown','orange','blue','purple','grey','darkblue']
colours = ['#edc951','#ee0000','#00a0b0','#4f372d','#eb6841','#336699','#8d2867','#949494','#2d454f']

colors = ['#ee0000','#edc951','#00a0b0','#336699','#8d2867','#949494','#4f372d','#eb6841','#2d454f']

df_all_sorted = df[df['sale_price'] <= df['sale_price'].quantile(q=0.8, interpolation='linear').round(0)]
df_all_sorted = df_all_sorted[df_all_sorted['sale_price'] <= df_all_sorted['pred_price']*10]
df_all_sorted = df_all_sorted.sort_values(by=['sale_price'], ascending=False)

fig = go.Figure()

fig.add_trace(go.Scatter(
                    name="Sale Price",
                    # x = y_test_sorted['valuetype'],
                    y = df_all_sorted['sale_price'].round(3),
                    text=df_all_sorted['token_id'],
                    mode = 'markers',
                    marker = dict(
                        size = 8
                        ,color = '#336699'
                        ,line = dict(width=0.2, color='#fff')
                        ,opacity=0.8
                    ),
                    hovertemplate=
                    "<b>Meebit #%{text}</b><br>" +
                    "Sale price (ETH): %{y:.2f}<br>" +
                    "<extra></extra>"
                    )
                )

fig.add_trace(go.Scatter(
                    name="Predicted Price",
                    # x=y_test_sorted['valuetype'],
                    y=df_all_sorted['pred_price'].round(3),
                    text=df_all_sorted['token_id'],
                    mode = 'markers',
                    marker = dict(
                        size = 8
                        ,color = '#ee0000'
                        ,line= dict(width=0.5, color='#fff')
                        ,opacity=0.3
                    ),
                    hovertemplate=
                    "<b>Meebit #%{text}</b><br>" +
                    "Prediction (ETH): %{y:.2f}<br>" +
                    "<extra></extra>"
                    )
                )
# Change the bar mode
fig.update_layout(
    # title={
    # 'text': value,
    # 'y':0.975,
    # 'x':0.5,
    # 'xanchor': 'center',
    # 'yanchor': 'top'
    # },
    xaxis = {'title':'Count',
            'titlefont': dict(family='sans-serif', size=10, color='#666'),
            'tickangle': 0,
            'showline': True,
            'linewidth': 0.1,
            'linecolor': '#fff',
            'tickformat': ',.0',
            'showspikes': True,
            'spikethickness': 0.5,
            # 'range': [0,1],
            'showgrid': True,
            'gridcolor': '#eee',
            'gridwidth': 0.1
            },
    yaxis = {'title': '',
            'titlefont': dict(family='sans-serif', size=10, color='#222'),
            'ticksuffix': "  ",
            'showspikes': False,
            'spikethickness': 0.5,
            'spikedash': 'solid',
            'showgrid': False,
            'gridcolor': '#fff',
            'gridwidth': 0.01,
            },
    margin = {'l': 50, 'b': 50, 't': 100, 'r': 50},
    showlegend = True,
    # legend = {'x': 0.9, 'y': 1, 'font': dict(family='sans-serif', size=12, color='#222')},#, 'title': dict(text=' WORD ', font=dict(size=15))},
    plot_bgcolor = "#fff",
    paper_bgcolor = "#fff",
    hovermode = 'closest',
    font = dict(
        family="Futura, Helvetica, sans-serif",
        size=10,
        color="#666"
        ),
    legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="left",
        x=0.90)
    )

fig.update_xaxes(categoryorder='total ascending')

# Customize aspect
# fig.update_traces(marker_line_color='rgba(200, 200, 200, 1)',marker_line_width=1, opacity=1)

fig.show()
pio.write_html(fig, file='/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/analysis/charts/price_analysis.html', auto_open=True, config=dict(displaylogo=False, displayModeBar=False, showTips=False))

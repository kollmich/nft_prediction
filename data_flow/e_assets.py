import requests
import pandas as pd
from pandas import json_normalize
import numpy as np
import os
from prefect import task, Flow, Parameter
from datetime import timedelta

# from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import os

from google.cloud import storage
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]='/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1)
def extract_asset_data(collection):
    def parse_asset_data(asset_dict):
        
        token_id = asset_dict['token_id']

        try:
            image_url = asset_dict['image_url']
        except:
            image_url = None
        try:
            image_preview_url = asset_dict['image_preview_url']
        except:
            image_preview_url = None
        try:
            image_thumbnail_url = asset_dict['image_thumbnail_url']
        except:
            image_thumbnail_url = None
        try:
            creator_username = asset_dict['creator']['user']['username']
        except:
            creator_username = None
        try:
            creator_address = asset_dict['creator']['address']
        except:
            creator_address = None

        try:
            contract_address = asset_dict['asset_contract']['address']
        except:
            contract_address = None

        try:
            contract_created_date = asset_dict['asset_contract']['created_date']
        except:
            contract_created_date = None

        try:
            contract_description = asset_dict['asset_contract']['description'].replace('\t',' ').replace('\n',' ').replace('\v',' ').replace('\b',' ').replace('\r',' ').replace('\f',' ').replace('\a',' ')
            contract_description = " ".join(contract_description.splitlines())
        except:
            contract_description = None

        try:
            owner_username = asset_dict['owner']['user']['username']
        except:
            owner_username = None

        try:
            event_type = asset_dict['last_sale']['event_type'] 
        except:
            event_type = None

        try:
            last_sale_date = asset_dict['last_sale']['event_timestamp']
        except:
            last_sale_date = None

        try:
            price_usd = asset_dict['last_sale']['payment_token']['usd_price']
        except:
            price_usd = None

        try:
            price_original = float(asset_dict['last_sale']['total_price'])
        except:
            price_original = None

        try:
            decimals = float(asset_dict['last_sale']['payment_token']['decimals'])
        except:
            decimals = None

        try:
            currency_original = asset_dict['last_sale']['payment_token']['symbol']
        except:
            currency_original = None

        try:
            sell_order_price = float(asset_dict['sell_orders'][0]['current_price'])
        except:
            sell_order_price = None

        try:
            sell_order_token = asset_dict['sell_orders'][0]['payment_token_contract']['symbol']
        except:
            sell_order_token = None

        try:
            sell_order_decimals = asset_dict['sell_orders'][0]['payment_token_contract']['decimals']
        except:
            sell_order_decimals = None

        try:
            sell_order_eth = float(asset_dict['sell_orders'][0]['payment_token_contract']['eth_price'])
        except:
            sell_order_eth = None

        owner_address = asset_dict['owner']['address']
        
        try:
            traits = asset_dict['traits']
        except:
            traits = None

        try:
            traits_number = len(asset_dict['traits'])
        except:
            traits_number = None

        num_sales = int(asset_dict['num_sales'])
        
        collection = asset_dict['collection']['name']


        result = {'token_id': token_id,
                'image_url': image_url,
                'image_preview_url': image_preview_url,
                'image_thumbnail_url': image_thumbnail_url,
                'collection': collection,
                'creator_username': creator_username,
                'creator_address': creator_address,
                'contract_address': contract_address,
                'contract_created_date': contract_created_date,
                'contract_description': contract_description,
                'owner_username': owner_username,
                'owner_address': owner_address,
                'traits': traits,
                'traits_number': traits_number,
                'event_type': event_type,
                'last_sale_date': last_sale_date,
                'price_usd': price_usd,
                'price_original': price_original,
                'decimals': decimals,
                'currency_original': currency_original,
                'num_sales': num_sales,
                'sell_order_price': sell_order_price,
                'sell_order_token': sell_order_token,
                'sell_order_decimals': sell_order_decimals,
                'sell_order_eth': sell_order_eth}
        
        return result

    url = "https://api.opensea.io/api/v1/assets"
    temp_df = pd.DataFrame() #Temporary empty dataframe

    for i in range(0, 10000):
        headers = {"X-API-KEY": "60140ff243934c11b726748f41e0b5b8"}
        
        querystring = {
                    # "token_ids": list(range((i*30)+1, (i*30)+31)),
                    "token_ids": list(range((i*30)+1, (i*30)+31)),
                    "asset_contract_address":collection,
                    # "order_by":"token_id",
                    "order_direction":"desc",
                    # "offset":str(i*50),
                    "limit":"30"
                    }

        response = requests.request("GET", url, headers=headers,params=querystring)
        
        if i%10 == 0:
            print(i, end=" ")
        if response.status_code != 200:
            print('error')
            break

        #Getting meebits data
        resp = response.json()['assets']
        if resp == []:
            break
        #Parsing meebits data
        parsed = [parse_asset_data(bit) for bit in resp]
        new_rows = pd.DataFrame(parsed, columns = ['token_id','collection','creator_username','creator_address','owner_username','owner_address','contract_address','contract_created_date','contract_description','traits','traits_number','event_type','image_url','image_preview_url','image_thumbnail_url','last_sale_date','price_usd','price_original','decimals','currency_original','num_sales','sell_order_price','sell_order_token','sell_order_decimals','sell_order_eth'])
        temp_df = temp_df.append(new_rows, ignore_index=True)

    step1 = (pd.concat({i: pd.DataFrame(x) for i, x in temp_df.pop('traits').items()})
            .reset_index(level=1, drop=True)
            .join(temp_df)
            .reset_index(drop=True))

    expanded = step1.pivot_table(index=['token_id']
                        ,columns='trait_type'
                        ,values=['value','max_value','display_type','trait_count']
                        ,aggfunc='first').reset_index()

    # Flattens hierarchical column index
    expanded.columns = expanded.columns.map(lambda x: ''.join([*map(str, x)]))
    expanded.columns = expanded.columns.str.replace(' ','')

    collection = pd.merge(temp_df, expanded, on='token_id')

    traits = [col for col in collection if col.startswith('trait_count')]
    traits = [col.replace('trait_count','') for col in traits]

    for x in traits:
        collection['trait_count'+x] = pd.to_numeric(collection['trait_count'+x].fillna(collection['value'+x].isna().sum()), downcast="integer")
    
    collection['price_original'] = (collection['price_original'].fillna(0))/(10**collection['decimals'])
    collection['sell_order_price'] = collection['sell_order_eth'] * (collection['sell_order_price'].fillna(0))/(10**collection['sell_order_decimals']) 
    collection.insert(0, 'timestamp_extracted', pd.to_datetime('now').replace(microsecond=0))
    collection['contract_description'] = collection['contract_description'].str[:400]

    return collection

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=2)
def load_asset_data(collection):
    collection_name = collection.collection.unique()[0].lower().replace(' ','')
    collection_path = '/Users/michalkollar/trendspotting_nft/data/assets/d_{0}_assets.csv'.format(collection_name)
    collection.to_csv(collection_path, index=False, sep='\t')
    return collection_path, collection_name
    
@task(max_retries=3, retry_delay=timedelta(seconds=60))
def load_asset_data_to_GCP(collection_path, collection_name):
    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    buckets = list(storage_client.list_buckets())
    bucket = storage_client.get_bucket("trendspotting_general_storage")
    blob = bucket.blob('dw/collections/assets/d_{0}_assets.csv'.format(collection_name))
    blob.upload_from_filename(collection_path)

with Flow("assets-extract") as flow:
    asset_contract = Parameter('asset_contract', default = '0x7Bd29408f11D2bFC23c34f18275bBf23bB716Bc7')
    collection = extract_asset_data(asset_contract)
    collection_path, collection_name = load_asset_data(collection)
    load_asset_data_to_GCP(collection_path, collection_name)

# Register the flow under the "tutorial" project
flow.register(project_name="NFT")
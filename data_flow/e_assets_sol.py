
# import requests

# url = "https://api-mainnet.magiceden.dev/v2/collections/btest/listings?offset=0&limit=20"

# payload={}
# headers = {}

# response = requests.request("GET", url, headers=headers, data=payload)

# print(response.text)

# url = "https:api.opensea.io/api/v1/assets"
# temp_df = pd.DataFrame() #Temporary empty dataframe

# for i in range(0, 10000):
#     headers = {"X-API-KEY": "60140ff243934c11b726748f41e0b5b8"}

#     querystring = {
#                 # "token_ids": list(range((i*30)+1, (i*30)+31)),
#                 "token_ids": list(range((i*30)+1, (i*30)+31)),
#                 "asset_contract_address":collection,
#                 # "order_by":"token_id",
#                 "order_direction":"desc",
#                 # "offset":str(i*50),
#                 "limit":"30"
#                 }


# url = "https:api-mainnet.magiceden.dev/v2/tokens/FvpJ6xguJfMHFKpYSS9nsToYPUk5BXyrxDFzq3ByaT3L"

# payload={}
# headers = {}

# response = requests.request("GET", url, headers=headers, data=payload)

# print(response.text)


# get a list of all activities for a collection
import requests

url = "https://api-mainnet.magiceden.dev/v2/collections/famous_fox_federation/activities?offset=0&limit=100"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response)


# {"signature":"22cM5C3jHjSAckTuvLRCyChBLXKxNEhDi7uzg2PU3Q3uMLfroeuZBcHnnLWZw2pz953X7Z7HN4vKDy8r8WW3frvw","type":"buyNow","source":"magiceden_v2","tokenMint":"FvpJ6xguJfMHFKpYSS9nsToYPUk5BXyrxDFzq3ByaT3L","collection":"famous_fox_federation","slot":126632132,"blockTime":1648225128,"buyer":"4dy52XeHFgXTe8Q6wqw1jVYoUyjXLDnKcmXbFjP38jRL","buyerReferral":"","seller":"BTfZbQvqTh7iVfUiaZq2Si1a6q3ve2GuwNHttrGammHe","sellerReferral":"","price":15.69}


#  # On Solana, we can obtain all tokens owned by a public key thanks to that method:

#  # connection
#  # .getParsedTokenAccountsByOwner(
#  #   new PublicKey(publicKey.toBase58()),
#  #   {
#  #     programId: TOKEN_PROGRAM_ID
#  #   }
#  # )
#  # .then((b) => {
#  #   const owner = b?.value?.[0].account.owner;
#  #   const pb = b?.value?.[0].pubkey;

#  #   const nonZeroAccounts = b?.value?.filter(
#  #     (obj) => obj.account.data.parsed.info.tokenAmount.uiAmount > 0
#  #   );
#  #   setTokens(JSON.stringify(nonZeroAccounts, null, 2));
#  # and we can retrieve the metadata of that token using this:

#  # import * as metadata from "@metaplex-foundation/mpl-token-metadata";
#  # const nftsmetadata:metadata.MetadataData[] = await metadata.Metadata.findDataByOwner(connection, publicKey);
#  # But before we can access this metadata, the token need to be minted. Is there a way to retrieve all the metadata (minted NFTs and unminted NFTs) of a collection using the CandyMachine ID or Token Metadata Program?


import pandas as pd
from pandas import json_normalize
import pandas as pd
# from oauth2client.service_account import ServiceAccountCredentials
from google.cloud import storage
# from prefect import task, Flow, Parameter
from datetime import timedelta
from pycoingecko import CoinGeckoAPI
# 1546300801 -- 1.Jan2019
# 1577836799 -- 31.Dec2019
# 1577836801 -- 1.Jan2020
# 1609459199 -- 31.Dec2020
# 1609459200 -- 1.Jan2021
# 1640995199 -- 31.Dec2021

from_t = '1609459200'
to_t = '1640995199'

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1)
def get_exchange_rates():

    cg = CoinGeckoAPI()
    weth = cg.get_coin_market_chart_by_id(id='weth',vs_currency='usd',days='365')
    # weth = cg.get_coin_market_chart_range_by_id(id='weth',vs_currency='usd',from_timestamp=from_t,to_timestamp=to_t)
    weth = weth['prices']
    eth = cg.get_coin_market_chart_by_id(id='ethereum',vs_currency='usd',days='365')
    # eth = cg.get_coin_market_chart_range_by_id(id='ethereum',vs_currency='usd',from_timestamp=from_t,to_timestamp=to_t)
    eth = eth['prices']

    mana = cg.get_coin_market_chart_by_id(id='decentraland',vs_currency='usd',days='365')
    # mana = cg.get_coin_market_chart_range_by_id(id='decentraland',vs_currency='usd',from_timestamp=from_t,to_timestamp=to_t)
    mana = mana['prices']

    nct = cg.get_coin_market_chart_by_id(id='name-changing-token',vs_currency='usd',days='365')
    # nct = cg.get_coin_market_chart_range_by_id(id='name-changing-token',vs_currency='usd',from_timestamp=from_t,to_timestamp=to_t)
    nct = nct['prices']

    mat = cg.get_coin_market_chart_by_id(id='matic-network',vs_currency='usd',days='365')
    # mat = cg.get_coin_market_chart_range_by_id(id='name-changing-token',vs_currency='usd',from_timestamp=from_t,to_timestamp=to_t)
    mat = mat['prices']

    dfw = pd.DataFrame(weth, columns = ['date','weth'])
    dfw['date'] = pd.to_datetime(dfw['date'],unit='ms').dt.date
    dfw = dfw.groupby('date').mean()
    dfe = pd.DataFrame(eth, columns = ['date','eth'])
    dfe['date'] = pd.to_datetime(dfe['date'],unit='ms').dt.date
    dfe = dfe.groupby('date').mean()
    dfm = pd.DataFrame(mana, columns = ['date','mana'])
    dfm['date'] = pd.to_datetime(dfm['date'],unit='ms').dt.date
    dfm = dfm.groupby('date').mean()
    dfn = pd.DataFrame(nct, columns = ['date','nct'])
    dfn['date'] = pd.to_datetime(dfn['date'],unit='ms').dt.date
    dfn = dfn.groupby('date').mean()
    dmat = pd.DataFrame(mat, columns = ['date','matic'])
    dmat['date'] = pd.to_datetime(dmat['date'],unit='ms').dt.date
    dmat = dmat.groupby('date').mean()

    prices = pd.merge(dfw, dfe, on='date', how='outer').reset_index()
    prices = pd.merge(prices, dfm, on='date', how='outer').reset_index().drop('index',axis=1)
    prices = pd.merge(prices, dfn, on='date', how='outer').reset_index().drop('index',axis=1)
    prices = pd.merge(prices, dmat, on='date', how='outer').reset_index().drop('index',axis=1)
    prices = prices.sort_values(by=['date'])
    print(prices)

    # df17 = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2017.csv', sep='\t')
    # df18 = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2018.csv', sep='\t')
    # df19 = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2019.csv', sep='\t')
    # df20 = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2020.csv', sep='\t')
    # df21 = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2021.csv', sep='\t')

    # df = df17.append(df18, ignore_index=True)
    # df = df.append(df19, ignore_index=True)
    # df = df.append(df20, ignore_index=True)
    # df = df.append(df21, ignore_index=True)
    df = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates.csv', sep='\t')

    df = df.append(prices, ignore_index=True)

    df = df.drop_duplicates(subset=['date'], keep='last')
    df.to_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates.csv', index=False, sep='\t')
    # df.to_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates_2021.csv', index=False, sep='\t')

#     df = df.append(prices, ignore_index=True)
#     df = df.drop_duplicates(subset=['date'], keep='last')
#     df.to_csv('/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates.csv', index=False, sep='\t')

#     output = '/Users/michalkollar/trendspotting_nft/data/exchange_rates/f_exchange_rates.csv'
#     storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
#     bucket = storage_client.get_bucket("trendspotting_general_storage")
#     blob = bucket.blob('dw/collections/exchange_rates/f_exchange_rates.csv')
#     blob.upload_from_filename(output)


# with Flow("exchange-rates-extract") as flow:
#     get_exchange_rates()

# # Register the flow under the "NFT" project
# flow.register(project_name="NFT")
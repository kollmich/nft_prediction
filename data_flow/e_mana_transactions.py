import requests
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import os
import glob
from oauth2client.service_account import ServiceAccountCredentials
from google.cloud import storage
import re
from prefect import task, Flow, Parameter
from datetime import timedelta
import numpy as np
import shutil

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1)
def extract_mana_transactions():
    etherscan_api_key = '2EMVJHENHMWH2Y5ZNGWARRQF19R28EZBKC'
    contract_address = '0x3f4a885ed8d9cdf10f3349357e3b243f3695b24a'
    opensea = '0x7be8076f4ea4a4ad08075c2508e481d6c946d12b'
    opensea_fee = '0x5b3256965e7c3cf26e11fcaf296dfc8807c01073' 
    opensea_fee = '0x0000000000000000000000005b3256965e7c3cf26e11fcaf296dfc8807c01073'
    dai = '0x6B175474E89094C44Da98b954EedeAC495271d0F'
    nct = '0x8A9c4dfe8b9D8962B31e4e16F8321C44d48e246E'
    usdc = '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'
    mana = '0x0F5D2fB29fb7d3CFeE444a200298f468908cC942'
    me = '0xea484e8074b60fe64bd1539ea3164ef96d2fa4c1'
    whales = '0x96ed81c7f4406eff359e27bff6325dc3c9e042bd'
    transfer_event = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef'
    order_match_event = '0xc4109843e0b7d514e4c093114b863f8e7d8d9a458c372cd51bfe526b588006c9'

    url = "https://api.etherscan.io/api/"

    temp_df = pd.DataFrame() #Temporary empty dataframe

    go = 1
    i = 0

    raw_data_folder = '/Users/michalkollar/trendspotting_nft/data/mana/raw'
    output_data_folder = '/Users/michalkollar/trendspotting_nft/data/mana/output'

    files_in_raw= glob.glob('{0}/*'.format(raw_data_folder))
    blocks = []
    for f in files_in_raw:
        block = re.search(r'\)_(.*?).parquet', f)
        blocks.append(int(float(block.group(1))))

    # minBlock = saved_match_orders['blockNumber'].min() 
    fromBlockOriginal = 12856383
    firstEver = 3919706
    maxBlock = max(blocks) if len(blocks) > 0 else firstEver
    print(maxBlock)
    # maxBlock = fromBlockOriginal
    df0 = pd.DataFrame()

    s = requests.Session()

    retries = Retry(total=5,
                    backoff_factor=0.1,
                    status_forcelist=[ 500, 502, 503, 504 ])

    s.mount('https://', HTTPAdapter(max_retries=retries))

    # s.get('http://httpstat.us/500')

    while go==1:
        i += 1
        querystring = {
                    "module":"logs",
                    "action":"getLogs",
                    "fromBlock": maxBlock, 
                    "toBlock": "latest",
                    "address": mana,
                    "topic0": transfer_event, # hash of the order match event
                    "topic2": opensea_fee, # hash of the order match event
                    "apikey": etherscan_api_key
                    }

        try:
            response = s.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            print ("Http Error:",errh)
        except requests.exceptions.ConnectionError as errc:
            print ("Error Connecting:",errc)
        except requests.exceptions.Timeout as errt:
            print ("Timeout Error:",errt)
        except requests.exceptions.RequestException as err:
            print ("OOps: Something Else",err)

        df = pd.json_normalize(response.json()['result'])
        df = pd.DataFrame(df)
        print(df)
        print(pd.DataFrame(df.topics.tolist(), index = df.index))
        df[['transferEvent','fromAddress','toAddress']] = pd.DataFrame(df.topics.tolist(), index = df.index)
        df = df.drop('topics', axis=1)
        df['fromAddress'] = '0x' + df['fromAddress'].str[-40:]
        df['toAddress'] = '0x' + df['toAddress'].str[-40:]
        # 0x0000000000000000000000008a502e0e3eda70eae505a6fa0fa49eb29b85fe5b
        df['blockNumber'] = df['blockNumber'].apply(int, base=16)
        maxBlock = df['blockNumber'].max()
        go=0 if df.equals(df0) else 1
        df0 = df
        print(maxBlock)
        temp_df = temp_df.append(df, ignore_index=True)

        if i%50 == 0:
            temp_df = temp_df.drop_duplicates()
            pq.write_table(pa.Table.from_pandas(temp_df), '{0}/orders_{1}.parquet'.format(raw_data_folder,maxBlock))
            temp_df = pd.DataFrame() #Temporary empty dataframe
        else:
            continue

    final = temp_df.drop_duplicates()
    maxFinal = final['blockNumber'].max()
    pq.write_table(pa.Table.from_pandas(final), '{0}/orders_{1}.parquet'.format(raw_data_folder,maxBlock))
    
    raw = pd.read_parquet('{0}/'.format(raw_data_folder))
    df = raw.drop_duplicates()
    df = df.reset_index(drop=True)
    df['batch'] = np.ceil(df.index / 2000000 ).astype(int)
    maxBatch = df['batch'].max()
    maxBlock = df['blockNumber'].max()
    table = pa.Table.from_pandas(df)
    pq.write_to_dataset(
        table,
        root_path='{0}'.format(output_data_folder),
        partition_cols=['batch'],
        partition_filename_cb=lambda x:'-'.join(str(x)) + '_{0}.parquet'.format(str(maxBlock))
    )

    storage.blob._DEFAULT_CHUNKSIZE = 5 * 1024* 1024  # 5 MB
    storage.blob._MAX_MULTIPART_SIZE = 5 * 1024* 1024  # 5 MB

    files_in_output = glob.glob('{0}/*'.format(output_data_folder))
    for file in files_in_output:
        contents = glob.glob('{0}/*'.format(file))
        for content in contents:
            shutil.move(content, '{0}/'.format(output_data_folder))

    files_in_raw = glob.glob('{0}/*'.format(raw_data_folder))
    for f in files_in_raw:
        os.remove(f)

    files_in_output = glob.glob('{0}/*'.format(output_data_folder))
    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    bucket = storage_client.get_bucket("trendspotting_general_storage")

    blobs = bucket.list_blobs(prefix='dw/collections/mana_transactions/')
    for blob in blobs:
        print('deleting')
        print(blob.name)
        print(blob.size)
        blob.delete()

    for f in files_in_output:
        if f.endswith('.parquet'):
            filename = f[ f.find("/(")+1 : f.find(".parquet") ]
            filename = filename.replace('(','').replace('-','').replace(',','').replace(')','')
            blob = bucket.blob('dw/collections/mana_transactions/f_{}.parquet'.format(filename))
            blob.upload_from_filename(f)
        else:
            os.rmdir(f)

    files_in_output = glob.glob('{0}/*'.format(output_data_folder))
    for f in files_in_output:
        shutil.move(f, '{0}/'.format(raw_data_folder))

with Flow("mana-transactions-extract") as flow:
    extract_mana_transactions()

# Register the flow under the "NFT" project
flow.register(project_name="NFT")

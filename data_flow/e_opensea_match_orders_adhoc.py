import requests
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import os
import glob
# from oauth2client.service_account import ServiceAccountCredentials
from google.cloud import storage
import re
# from prefect import task, Flow, Parameter
from datetime import timedelta
import numpy as np
import shutil

# @task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1)
def extract_opensea_match_orders():
    etherscan_api_key = '2EMVJHENHMWH2Y5ZNGWARRQF19R28EZBKC'
    contract_address = '0x3f4a885ed8d9cdf10f3349357e3b243f3695b24a'
    opensea1 = '0x7be8076f4ea4a4ad08075c2508e481d6c946d12b'
    opensea2 = '0x7f268357a8c2552623316e2562d90e642bb538e5'
    me = '0xea484e8074b60fe64bd1539ea3164ef96d2fa4c1'
    whales = '0x96ed81c7f4406eff359e27bff6325dc3c9e042bd'
    transfer_event = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef'
    order_match_event = '0xc4109843e0b7d514e4c093114b863f8e7d8d9a458c372cd51bfe526b588006c9'

    url = "https://api.etherscan.io/api/"

    temp_df = pd.DataFrame() #Temporary empty dataframe

    go = 1
    i = 0

    # saved_match_orders = pd.read_parquet('/Users/michalkollar/trendspotting_nft/data/opensea/raw')

    # files_in_raw= glob.glob('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw/*')
    # blocks = []
    # for f in files_in_raw:
    #     block = re.search(r'\)_(.*?).parquet', f)
    #     blocks.append(int(float(block.group(1))))

    # minBlock = saved_match_orders['blockNumber'].min() 
    fromBlockOriginal = 14276023
    firstEver = 3919706
    maxBlock = fromBlockOriginal
    opensea = opensea1 if maxBlock < 14276023 else opensea2
    # maxBlock = max(blocks) if len(blocks) > 0 else fromBlockOriginal
    # maxBlock = fromBlockOriginal
    df0 = pd.DataFrame()

    s = requests.Session()

    retries = Retry(total=5,
                    backoff_factor=0.1,
                    status_forcelist=[ 500, 502, 503, 504 ])

    s.mount('https://', HTTPAdapter(max_retries=retries))

    # s.get('http://httpstat.us/500')

    while go==1:
        i += 1
        querystring = {
                    "module":"logs",
                    "action":"getLogs",
                    "fromBlock": maxBlock, 
                    "toBlock": "latest",
                    "address": opensea,
                    "topic0": order_match_event, # hash of the order match event
                    "apikey": etherscan_api_key
                    }

        try:
            response = s.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            print ("Http Error:",errh)
        except requests.exceptions.ConnectionError as errc:
            print ("Error Connecting:",errc)
        except requests.exceptions.Timeout as errt:
            print ("Timeout Error:",errt)
        except requests.exceptions.RequestException as err:
            print ("OOps: Something Else",err)

        df = pd.json_normalize(response.json()['result'])
        print(df)
        df = pd.DataFrame(df)
        df[['eventHash','fromAddress','toAddress','metadata']] = pd.DataFrame(df.topics.tolist(), index = df.index)
        df = df.drop('topics', axis=1)
        df['fromAddress'] = '0x' + df['fromAddress'].str[-40:]
        df['toAddress'] = '0x' + df['toAddress'].str[-40:]
        # 0x0000000000000000000000008a502e0e3eda70eae505a6fa0fa49eb29b85fe5b
        df['blockNumber'] = df['blockNumber'].apply(int, base=16)
        maxBlock = df['blockNumber'].max()
        go=0 if df.equals(df0) else 1
        df0 = df
        print(maxBlock)
        temp_df = temp_df.append(df, ignore_index=True)

        if i%50 == 0:
            temp_df = temp_df.drop_duplicates()    
            temp_df['timeStamp'] = temp_df['timeStamp'].apply(int, base=16)
            temp_df['timeStamp'] = pd.to_datetime(temp_df['timeStamp'],unit='s')
            temp_df['sale_price'] = temp_df['data'].str[-64:]
            temp_df['sale_price'] = '0x'+temp_df['sale_price'].astype(str)
            temp_df['sale_price'] = temp_df['sale_price'].apply(int, base=16)
            temp_df['sale_price'] = temp_df['sale_price'] / (10 ** 18)
            # temp_df.to_csv('/Users/michalkollar/trendspotting_nft/data/result_match_orders_{0}.csv'.format(i), index=False, sep='\t')
            # pq.write_table(pa.Table.from_pandas(temp_df), '/Users/michalkollar/trendspotting_nft/data/opensea/raw/orders_{}.parquet'.format(maxBlock))
            pq.write_table(pa.Table.from_pandas(temp_df), '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw/orders_{}.parquet'.format(maxBlock))
            temp_df = pd.DataFrame() #Temporary empty dataframe
        else:
            continue

    final = temp_df.drop_duplicates()
    final['timeStamp'] = final['timeStamp'].apply(int, base=16)
    final['timeStamp'] = pd.to_datetime(final['timeStamp'],unit='s')
    final['sale_price'] = final['data'].str[-64:]
    final['sale_price'] = '0x'+final['sale_price'].astype(str)
    final['sale_price'] = final['sale_price'].apply(int, base=16)
    final['sale_price'] = final['sale_price'] / (10 ** 18)
    maxFinal = final['blockNumber'].max()
    pq.write_table(pa.Table.from_pandas(final), '/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw/orders_{}.parquet'.format(maxFinal))
    
    raw = pd.read_parquet('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw')
    df = raw.drop_duplicates()
    df = df.reset_index(drop=True)
    df['batch'] = np.ceil(df.index / 2000000 ).astype(int)
    maxBatch = df['batch'].max()
    maxBlock = df['blockNumber'].max()
    table = pa.Table.from_pandas(df)
    pq.write_to_dataset(
        table,
        root_path='/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/output'.format(maxBlock),
        partition_cols=['batch'],
        partition_filename_cb=lambda x:'-'.join(str(x)) + '_{0}.parquet'.format(str(maxBlock))
    )
    # pq.write_table(pa.Table.from_pandas(final), '/Users/michalkollar/trendspotting_nft/data/opensea/raw/match_orders_{}.parquet'.format(maxFinal))

    storage.blob._DEFAULT_CHUNKSIZE = 5 * 1024* 1024  # 5 MB
    storage.blob._MAX_MULTIPART_SIZE = 5 * 1024* 1024  # 5 MB

    files_in_output = glob.glob('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/output/*')
    for file in files_in_output:
        contents = glob.glob('{0}/*'.format(file))
        for content in contents:
            shutil.move(content, "/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/output/")

    files_in_raw = glob.glob('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw/*')
    for f in files_in_raw:
        os.remove(f)

    files_in_output = glob.glob('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/output/*')
    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/nft_prediction/gcp_secret.json')
    bucket = storage_client.get_bucket("trendspotting_general_storage")

    # blobs = bucket.list_blobs(prefix='dw/collections/opensea_match_orders/')
    # for blob in blobs:
    #     print('deleting')
    #     print(blob.name)
    #     print(blob.size)
    #     blob.delete()

    # for f in files_in_output:
    #     if f.endswith('.parquet'):
    #         filename = f[ f.find("/(")+1 : f.find(".parquet") ]
    #         filename = filename.replace('(','').replace('-','').replace(',','').replace(')','')
    #         blob = bucket.blob('dw/collections/opensea_match_orders/f_{}.parquet'.format(filename))
    #         blob.upload_from_filename(f)
    #     else:
    #         os.rmdir(f)

    files_in_output = glob.glob('/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/output/*')
    for f in files_in_output:
        shutil.move(f, "/Users/michalkollar/Desktop/Personal/Coding/trendspotting_nft/data/opensea/raw/")

extract_opensea_match_orders()

# with Flow("opensea-match-orders-extract") as flow:
#     extract_opensea_match_orders()

# # Register the flow under the "NFT" project
# flow.register(project_name="NFT")

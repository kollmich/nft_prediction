import requests
import pandas as pd
from pandas import json_normalize
import numpy as np
import os
from prefect import task, Flow, Parameter
from datetime import timedelta,datetime
import time
# from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import os
import pyarrow.parquet as pq
import pyarrow as pa
import glob
from google.cloud import storage
import math
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]='/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json'

@task(max_retries=3, retry_delay=timedelta(seconds=60))
def extract_sale_offers():
    print(datetime.now())
    def parse_asset_data(asset_dict):
        
        token_id = asset_dict['asset']['token_id']
        try:
            collection = asset_dict['asset']['asset_contract']['name']
        except:
            collection = None
        try:
            contract_address = asset_dict['asset']['asset_contract']['address']
        except:
            contract_address = None
        try:
            contract_schema_name = asset_dict['asset']['asset_contract']['schema_name']
        except:
            contract_schema_name = None

        try:
            contract_created_date = asset_dict['asset']['asset_contract']['created_date']
        except:
            contract_created_date = None
        # 0 for fixed-price sales or min-bid auctions, and 1 for declining-price Dutch Auctions.
        try:
            auction_type = asset_dict['auction_type']
        except:
            auction_type = None
        try:
            event_date = asset_dict['created_date']
        except:
            event_date = None
        try:
            event_closing_date = asset_dict['closing_date']
        except:
            event_closing_date = None
        try:
            expiration_time = asset_dict['expiration_time']
        except:
            expiration_time = None
        try:
            current_price = asset_dict['current_price']
        except:
            current_price = None
        try:
            fee_method = asset_dict['fee_method']
        except:
            fee_method = None
        try:
            sale_kind = asset_dict['sale_kind']
        except:
            sale_kind = None
        try:
            how_to_call = asset_dict['how_to_call']
        except:
            how_to_call = None
        try:
            payment_token_symbol = asset_dict['payment_token_contract']['symbol']
        except:
            payment_token_symbol = None
        try:
            payment_token_decimals = asset_dict['payment_token_contract']['decimals']
        except:
            payment_token_decimals = None
        try:
            base_price = asset_dict['base_price']
        except:
            base_price = None
        try:
            extra = asset_dict['extra']
        except:
            extra = None
        try:
            cancelled = asset_dict['cancelled']
        except:
            cancelled = None
        try:
            approved_on_chain = asset_dict['approved_on_chain']
        except:
            approved_on_chain = None
        try:
            finalized = asset_dict['finalized']
        except:
            finalized = None
        try:
            marked_invalid = asset_dict['marked_invalid']
        except:
            marked_invalid = None


        result = {
                'token_id': token_id,
                'contract_schema_name': contract_schema_name,
                'collection': collection,
                'auction_type': auction_type,
                'contract_address': contract_address,
                'contract_created_date': contract_created_date,
                'sale_kind': sale_kind,
                'event_date': event_date,
                'event_closing_date': event_closing_date,
                'current_price': current_price,
                'fee_method': fee_method,
                'how_to_call': how_to_call,
                'expiration_time': expiration_time,
                'payment_token_symbol': payment_token_symbol,
                'payment_token_decimals': payment_token_decimals,
                'base_price': base_price,
                'extra': extra,
                'approved_on_chain': approved_on_chain,
                'finalized': finalized,
                'marked_invalid': marked_invalid,
                'cancelled': cancelled
                }
        
        return result


    collections = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/collections_transfers.csv', sep=',')

    final = pd.DataFrame() 

    for index, row in collections.iterrows():
        name = row['collection']
        address = row['address']
        transfer_event = row['transferTopic']
        vol = row['volume']
        volFrac = int(math.ceil(vol/30))

        url = "https://api.opensea.io/wyvern/v1/orders"
        # url = "https://api.opensea.io/api/v1/events"

        temp_df = pd.DataFrame() 

        for i in range(0, volFrac):
            headers = {"Accept": "application/json",'User-agent': 'your bot 0.1', "X-API-KEY": "60140ff243934c11b726748f41e0b5b8"}

            querystring = {
                        "token_ids": list(range((i*30)+1, (i*30)+31)),
                        "asset_contract_address": address,
                        "include_invalid": "false",
                        "include_bundled": "false",
                        "side": "1",
                        # "occurred_after": '0',
                        "order_by":"created_date",
                        "order_direction":"desc",
                        # "offset":str(i*50),
                        "limit":"50"
                        }

            response = requests.request("GET", url, headers=headers, params=querystring)

            if i%10 == 0:
                print(i, end=" ")
            if response.status_code == 429:
                time.sleep(int(response.headers["Retry-After"]))
                continue
            elif response.status_code != 200:
                print('error', response.status_code)
                break

            resp = response.json()['orders']
            # if resp == []:
            #     time.sleep(0.1)
            # Parsing meebits data
            parsed = [parse_asset_data(bit) for bit in resp]
            new_rows = pd.DataFrame(parsed, columns = ['token_id','contract_schema_name','collection','auction_type','contract_address','contract_created_date','sale_kind','event_date','event_closing_date','current_price','fee_method','how_to_call','expiration_time','payment_token_symbol','payment_token_decimals','base_price','extra','approved_on_chain','finalized','marked_invalid','cancelled'])
            temp_df = temp_df.append(new_rows, ignore_index=True)
        
        final = final.append(temp_df, ignore_index=True)
    
    final = final.drop_duplicates() 
    final.insert(0, 'timestamp_extracted', pd.to_datetime('now').replace(microsecond=0))
    final[['contract_created_date', 'event_date', 'event_closing_date']] = final[['contract_created_date', 'event_date', 'event_closing_date']].apply(pd.to_datetime)
    final['current_price'] = final['current_price'].astype(float) / (10 ** final['payment_token_decimals'].astype(float))  
    final['base_price'] = final['base_price'].astype(float) / (10 ** final['payment_token_decimals'].astype(float))  
    final['fee_method'] = final['fee_method'].astype(float)
    final['sale_kind'] = final['sale_kind'].astype(int)
    final['approved_on_chain'] = final['approved_on_chain'].astype(bool)
    final['finalized'] = final['finalized'].astype(bool)
    final['marked_invalid'] = final['marked_invalid'].astype(bool)

    final.to_csv('/Users/michalkollar/trendspotting_nft/data/opensea/sale_offers/offers.csv', index=False, sep='\t')
    fields = [						
        pa.field('timestamp_extracted', pa.timestamp('ns')),        
        pa.field('token_id', pa.string()),
        pa.field('contract_schema_name', pa.string()),
        pa.field('collection', pa.string()),
        pa.field('auction_type', pa.string()),
        pa.field('contract_address', pa.string()),
        pa.field('contract_created_date', pa.timestamp('ns')),
        pa.field('sale_kind', pa.int64()),
        pa.field('event_date', pa.timestamp('us')),
        pa.field('event_closing_date', pa.timestamp('ns')),
        pa.field('current_price', pa.float64()),
        pa.field('fee_method', pa.int64()),
        pa.field('how_to_call', pa.int64()),
        pa.field('expiration_time', pa.timestamp('us')),
        pa.field('payment_token_symbol', pa.string()),
        pa.field('payment_token_decimals', pa.int64()),
        pa.field('base_price', pa.float64()),
        pa.field('extra', pa.string()),
        pa.field('approved_on_chain', pa.bool_()),
        pa.field('finalized', pa.bool_()),
        pa.field('marked_invalid', pa.bool_()),
        pa.field('cancelled', pa.bool_()),
    ]

    my_schema = pa.schema(fields)

    table = pa.Table.from_pandas(final, schema=my_schema, preserve_index=False)
    pq.write_table(table, '/Users/michalkollar/trendspotting_nft/data/opensea/sale_offers/f_opensea_offers.parquet')
    print(table)

    files_in_output = glob.glob('/Users/michalkollar/trendspotting_nft/data/opensea/sale_offers/f_opensea_offers.parquet')
    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    bucket = storage_client.get_bucket("trendspotting_general_storage")

    blobs = bucket.list_blobs(prefix='dw/collections/opensea_sale_offers/')
    for blob in blobs:
        print('deleting')
        print(blob.name)
        print(blob.size)
        blob.delete()

    for f in files_in_output:
        filename = f[ f.find("/f_opensea_offers")+1 : f.find(".parquet") ]
        blob = bucket.blob('dw/collections/opensea_sale_offers/{}.parquet'.format(filename))
        blob.upload_from_filename(f)
    print(datetime.now())

with Flow("sale-offers-extract") as flow:
    extract_sale_offers()

# Register the flow under the "tutorial" project
flow.register(project_name="NFT")
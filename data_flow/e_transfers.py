import requests
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
import os
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import os
import glob
from oauth2client.service_account import ServiceAccountCredentials
from google.cloud import storage
from prefect import task, Flow, Parameter
from datetime import timedelta

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1)
def extract_transfers():

    s = requests.Session()

    retries = Retry(total=5,
                    backoff_factor=0.1,
                    status_forcelist=[ 500, 502, 503, 504 ])

    s.mount('https://', HTTPAdapter(max_retries=retries))

    etherscan_api_key = '2EMVJHENHMWH2Y5ZNGWARRQF19R28EZBKC'

    contract_address = '0x3f4a885ed8d9cdf10f3349357e3b243f3695b24a'
    opensea = '0x7be8076f4ea4a4ad08075c2508e481d6c946d12b'
    me = '0xea484e8074b60fe64bd1539ea3164ef96d2fa4c1'

    order_match_event = '0xc4109843e0b7d514e4c093114b863f8e7d8d9a458c372cd51bfe526b588006c9'

    url = "https://api.etherscan.io/api/"

    collections = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/collections_transfers.csv', sep=',')

    temp_df = pd.DataFrame() #Temporary empty dataframe

    for index, row in collections.iterrows():
        name = row['collection']
        address = row['address']
        transfer_event = row['transferTopic']

        print(name)
        if os.path.exists('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name)):
            print('exists')
            saved = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name), sep='\t', error_bad_lines=False)
            print(saved)
            fromBlock = saved['blockNumber'].max()
            print(fromBlock)
        else:
            print('new')
            fromBlock = row['fromBlock']
            
        print(fromBlock)
        go = 1
        i = 0

        df0 = pd.DataFrame()

        while go==1: 
            i += 1
            querystring = {
                        "module":"logs",
                        "action":"getLogs",
                        "fromBlock": fromBlock,
                        "toBlock": "latest",
                        "address": address, # address of the Weird Whale contract
                        "topic0": transfer_event, # hash of the transfer event
                        "apikey": etherscan_api_key
                        }

            try:
                response = s.request("GET", url, params=querystring)
                response.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)


            df = pd.json_normalize(response.json()['result'])
            if df.empty:
                break
            df = pd.DataFrame(df)
            if name == 'cryptopunks':
                df[['eventHash','fromAddress','toAddress']] = pd.DataFrame(df.topics.tolist(), index = df.index)
                df['tokenId'] = df['data']
                df = df.drop('topics', axis=1)
            else:    
                print(df.topics.tolist())
                df[['eventHash','fromAddress','toAddress','tokenId']] = pd.DataFrame(df.topics.tolist(), index = df.index)
                df = df.drop('topics', axis=1)

            df['blockNumber'] = df['blockNumber'].apply(int, base=16)
            fromBlock = df['blockNumber'].max()
            go=0 if df.equals(df0) else 1 
            df0 = df
            print(fromBlock)
            temp_df = temp_df.append(df, ignore_index=True)
            
        final = temp_df.drop_duplicates()    
        final['timeStamp'] = final['timeStamp'].apply(int, base=16)
        final['timeStamp'] = pd.to_datetime(final['timeStamp'],unit='s')
        final['tokenId'] = final['tokenId'].apply(int, base=16)

        if os.path.exists('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name)):
            savedCollection = pd.read_csv('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name), sep='\t', error_bad_lines=True)
            savedCollection = savedCollection.append(final, ignore_index=True)
            # savedCollection = savedCollection.dropna(subset=['blockNumber'])
            savedCollection = savedCollection.drop_duplicates(subset=['transactionHash','tokenId'], keep='last').sort_values(by='blockNumber', ascending=True)
            savedCollection.to_csv('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name), index=False, sep='\t')
        else:
            final = final.drop_duplicates(subset=['transactionHash','tokenId'], keep='last').sort_values(by='blockNumber', ascending=True)
            final.to_csv('/Users/michalkollar/trendspotting_nft/data/transfers/f_{0}_transfers.csv'.format(name), index=False, sep='\t')
        
        temp_df = pd.DataFrame()


    files_in_transfers = glob.glob('/Users/michalkollar/trendspotting_nft/data/transfers/*')
    storage_client = storage.Client.from_service_account_json('/Users/michalkollar/trendspotting_nft/nft_prediction/gcp_secret.json')
    bucket = storage_client.get_bucket("trendspotting_general_storage")

    for f in files_in_transfers:
        print(f)
        filename = f[ f.find("/transfers/")+1 : f.find("_transfers.csv") ]
        blob = bucket.blob('dw/collections/{0}_transfers.csv'.format(filename))
        blob.upload_from_filename(f)

with Flow("transfers-extract") as flow:
    extract_transfers()

# Register the flow under the "NFT" project
flow.register(project_name="NFT")
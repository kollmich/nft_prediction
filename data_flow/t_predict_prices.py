import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from prefect import task, Flow, Parameter
from datetime import timedelta
from google.cloud import storage

from runpy import run_path
from subprocess import call,run
import sys

@task(max_retries=3, retry_delay=timedelta(seconds=60), nout=1, checkpoint=False)
def predict_asset_price():
    sys.path.insert(1, '/Users/michalkollar/trendspotting_nft/nft_prediction/analysis/models/_pipelines')
    import t_deadfellaz
    import t_deadfellaz_file_upload
    import t_mfers
    import t_mfers_file_upload
    import t_mutantapeyachtclub
    import t_mutantapeyachtclub_file_upload
    import t_karafuru
    import t_karafuru_file_upload
    import t_pudgypenguins
    import t_pudgypenguins_file_upload
    import t_hashmasks
    import t_hashmasks_file_upload
    import t_meebits
    import t_meebits_file_upload
    import t_weirdwhales
    import t_weirdwhales_file_upload
    import t_boredapeyachtclub
    import t_boredapeyachtclub_file_upload
    import t_worldofwomen
    import t_worldofwomen_file_upload
    import t_doodles
    import t_doodles_file_upload
    import t_creatureworld
    import t_creatureworld_file_upload
    import t_coolcats
    import t_coolcats_file_upload

    t_deadfellaz.main()
    t_deadfellaz_file_upload.main()
    t_mfers.main()
    t_mfers_file_upload.main()
    t_mutantapeyachtclub.main()
    t_mutantapeyachtclub_file_upload.main()
    t_karafuru.main()
    t_karafuru_file_upload.main()
    t_pudgypenguins.main()
    t_pudgypenguins_file_upload.main()
    t_hashmasks.main()
    t_hashmasks_file_upload.main()
    t_meebits.main()
    t_meebits_file_upload.main()
    t_weirdwhales.main()
    t_weirdwhales_file_upload.main()
    t_boredapeyachtclub.main()
    t_boredapeyachtclub_file_upload.main()
    t_worldofwomen.main()
    t_worldofwomen_file_upload.main()
    t_doodles.main()
    t_doodles_file_upload.main()
    t_creatureworld.main()
    t_creatureworld_file_upload.main()
    t_coolcats.main()
    t_coolcats_file_upload.main()


with Flow("price-prediction") as flow:
    predict_asset_price()

# Register the flow under the "tutorial" project
flow.register(project_name="NFT")
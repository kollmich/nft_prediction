!function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="wowHYiSopEUZNPZwqkPblHGInsdLdJ7a";;analytics.SNIPPET_VERSION="4.15.3";
analytics.load("wowHYiSopEUZNPZwqkPblHGInsdLdJ7a");
analytics.page('Visit', {
    // title: 'Segment Pricing',
    url: window.location.href,
    path: window.location.pathname
    });
analytics.identify('f4ca124298', {
    name: 'Michael Brown',
    email: 'mbrown@example.com'
  });
}}();

window.addEventListener("load", function(e){
    document.getElementById("open-tour").addEventListener("click", function(e) {
        console.log("clicked:", this);
        analytics.track('clicked', properties={
            'button': 'tour',
            'page': window.location.pathname,
        })
    });
    document.getElementById("open-offcanvas").addEventListener("click", function(e) {
        console.log("clicked:", this);
        analytics.track('clicked', properties={
            'button': 'explore',
            'page': window.location.pathname,
        })
    });

    let dropdowns = document.querySelectorAll('button[id^=url-dropdown]');

    dropdowns.forEach(dropdown => {
        dropdown.addEventListener("change", function(e) {
            console.log("clicked:", this);
            analytics.track('clicked', properties={
                'button': 'dropdown',
                'page': window.location.pathname,
        })
        });
    });

    let collections = document.getElementById("open-offcanvas").querySelectorAll('a');

    collections.forEach(dropdown => {
        collection.addEventListener("change", function(e) {
            console.log("clicked:", this);
            analytics.track('clicked', properties={
                'button': 'link',
                'page': window.location.pathname,
        })
        });
    });
});
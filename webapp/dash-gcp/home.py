import dash
# dash.register_page(__name__)
from dash import dcc, html, callback, dash_table
from dash.dependencies import Input, Output, State
import pandas as pd
import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots
import pandas as pd
import pathlib

colors = ['#ee0000','#edc951','#00a0b0','#336699','#8d2867','#949494','#4f372d','#eb6841','#2d454f']

dfv = pd.read_csv('https://storage.googleapis.com/trendspotting_general_storage/dw/collections/visualisation_input/meebits_weekly.csv'
    ,nrows=100
    ,sep='\t'
    ,low_memory=True
    ,dtype={
        "token_id": "int16",
        "image_url": "object",
        "rate_date_current":  object,
        "eth_price_usd_current": "float64", 
        "month_index": "float64", 
        "sale_date": object, 
        "flag_is_latest": "int16", 
        "sale_price": "float64", 
        "pred_price": "float64", 
        "current_sale_price": "float64", 
        "opportunity_coef": "float64", 
        "eth_price_usd": "float64", 
        "num_sales": "int16", 
        "traits_number": "int16", 
        "trait_countBeard": "int16", 
        "trait_countBeardColor": "int16", 
        "trait_countEarring": "int16", 
        "trait_countGlasses": "int16", 
        "trait_countGlassesColor": "int16", 
        "trait_countHairColor": "int16", 
        "trait_countHairStyle": "int16", 
        "trait_countHat": "int16", 
        "trait_countHatColor": "int16", 
        "trait_countJerseyNumber": "int16", 
        "trait_countNecklace": "int16", 
        "trait_countOvershirt": "int16", 
        "trait_countOvershirtColor": "int16", 
        "trait_countPants": "int16", 
        "trait_countPantsColor": "int16", 
        "trait_countShirt": "int16", 
        "trait_countShirtColor": "int16", 
        "trait_countShoes": "int16", 
        "trait_countShoesColor": "int16", 
        "trait_countTattooMotif": "int16", 
        "trait_countType": "int16",
        "valueBeard": "category",
        "valueBeardColor": "category",
        "valueEarring": "category",
        "valueGlasses": "category",
        "valueGlassesColor": "category",
        "valueHairColor": "category",
        "valueHairStyle": "category",
        "valueHat": "category",
        "valueHatColor": "category",
        "valueJerseyNumber": "category",
        "valueNecklace": "category",
        "valueOvershirt": "category",
        "valueOvershirtColor": "category",
        "valuePants": "category",
        "valuePantsColor": "category",
        "valueShirt": "category",
        "valueShirtColor": "category",
        "valueShoes": "category",
        "valueShoesColor": "category",
        "valueTattooMotif": "category",
        "valueType": "category",
        "shap_valueType": "float64", 
        "shap_valueShirt": "float64", 
        "shap_valuePants": "float64", 
        "shap_valueShoes": "float64", 
        "shap_valueOvershirt": "float64", 
        "shap_valueHat": "float64", 
        "shap_valueHair": "float64", 
        "shap_valueBeard": "float64", 
        "shap_valueNecklace": "float64", 
        "shap_valueGlasses": "float64", 
        "shap_valueEarring": "float64", 
        "shap_valueTattoo": "float64", 
        "shap_valueOther": "float64", 
        "timestamp": "object"
        }    
    )

dfv['valuetype_cat'] = dfv['valueType'].astype('category').cat.codes
dfv['token_id'] = dfv['token_id'].astype('int16')
dfv['pred_price'] = dfv['pred_price'].astype(float)
dfv['color'] = np.where(dfv['opportunity_coef']> 1, 1, 0)

types_list = dfv.valueType.unique()
asset_list = sorted(dfv.token_id.unique()) 

layout = html.Div([
    html.Div([], id='overlay'),    
    html.H3(' Meebits', id='title-main', style={"textAlign": "left", "display": "inline", "verticalAlign": "middle"}),
    html.Div([
        html.H6('Character attributes, price predictions and current purchase opportunities. '),
    ], className='topnav'),

    html.Div([
        html.Div(dcc.Dropdown(
            id='url-dropdown', value=1, clearable=False,
            persistence=True, persistence_type='memory',
            options=[{'label': x, 'value': x} for x in asset_list]
        ), className='six columns'),
        html.Div(dcc.Dropdown(
            id='type-dropdown', value='Human', clearable=False,
            options=[{'label': x, 'value': x} for x in types_list]
        ), className='six columns')
    ], className='row'),

    html.Div([
        html.Div([
            html.Div([
                html.Img(
                    className="asset-image",
                    id="asset-image",
                    src="https://lh3.googleusercontent.com/zDUaYYOE8Jg5MgMuPU90gN9pqNd3soCS205bi6N8AhKJvTpcchhF8VHo0Jq8-bx8zDvEKL_LY5MIfP97lzUEIQWxzN-WqaAssa7rjEU"
                )
            ], className='six columns'),
            html.Div([
                html.Div(['Token ID:  ',
                    html.B(id='textarea-output-token_id'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Number of traits:  ',
                    html.B(id='textarea-output-traits_number'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Ask Price (ETH):  ',
                    html.B(id='textarea-output-sell_order_price_eth'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Times resold:  ',
                    html.B(id='textarea-output-num_sales'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Last Sale:  ',
                    html.B(id='textarea-output-last_sale_date'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Last Purchase Price (ETH):  ',
                    html.B(id='textarea-output-price_original_eth'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail'),
                html.Div(['Predicted Price (ETH):  ',
                    html.B(id='textarea-output-pred_price'),
                    ], style={'whiteSpace': 'pre-line'}, className='detail')
                # html.Div([
                #     dcc.Graph(id='bullet', figure={})
                #     ], style={'whiteSpace': 'pre-line'})
            ], className='six columns intro')
        ],id = 'bio', className='twelve columns'),
        dcc.Graph(id='bottom-left', className='twelve columns', style={'height': '38vh'}, figure={}, config={'displayModeBar': False})
    ], className='six columns left'),

    html.Div([
        dcc.Graph(id='top-center', className='twelve columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False}),
        dcc.Graph(id='bottom-center', className='twelve columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False})
    ], className='six columns center'),
    html.Div([
        html.Div([
            dash_table.DataTable(
                id='top-right',
                # columns=[
                #     {"name": i, "id": i, "deletable": False, "selectable": True, "hideable": False}
                #     if i == "token_id"
                #     else {"name": i, "id": i, "deletable": False, "selectable": True, "hideable": False}
                #     for i in dfv[['token_id','current_sale_price','pred_price', 'opportunity_coef']].columns
                # ],
                columns=[
                    {"name": "token_id", "id": "token_id", "type": "string"},
                    {"name": "Current Listing Price", "id": "current_sale_price", "type": "numeric", "format": {"specifier": ",.2f"}},
                    {"name": "Estimated Price", "id": "pred_price", "type": "numeric","format": {"specifier": ",.2f"}},
                    {"name": "Coefficient of Opportunity", "id": "opportunity_coef", "type": "numeric", "format": {"specifier": ",.2f"}},
                ],
                data=dfv.to_dict('records'),  # the contents of the table
                editable=False,              # allow editing of data inside all cells
                filter_action="none",     # allow filtering of data by user ('native') or not ('none')
                sort_action="native",       # enables data to be sorted per-column by user or not ('none')
                sort_mode="single",         # sort across 'multi' or 'single' columns
                # column_selectable="multi",  # allow users to select 'multi' or 'single' columns
                # row_selectable="multi",     # allow users to select 'multi' or 'single' rows
                row_deletable=False,         # choose if user can delete a row (True) or not (False)
                selected_columns=[],        # ids of columns that user selects
                selected_rows=[],           # indices of rows that user selects
                page_action="native",       # all data is passed to the table up-front or not ('none')
                page_current=0,             # page number that user is on
                page_size=50,                # number of rows visible per page
                style_table={'height': '36vh', 'overflowY': 'auto'},
                style_cell={                # ensure adequate header width when text is shorter than cell's text
                    'minWidth': 95, 'maxWidth': 95, 'width': 95, 
                    'font-family':"Futura, Helvetica, sans-serif",
                    'textAlign': 'left',
                    'padding': '0 10px',
                },
                # style_cell_conditional=[    # align text columns to left. By default they are aligned to right
                #     {
                #         'if': {'column_id': 'token_id'},
                #         'textAlign': 'left'
                #     } for c in ['country', 'iso_alpha3']
                # ],
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold',
                    'border': '1px solid #eee',
                    'fontSize':12.5,
                    'font-family':"Futura, Helvetica, sans-serif",
                    'padding': '0 10px',
                },
                style_data={                # overflow cells' content into multiple lines
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    'border': '1px solid #eee'
                }
            ),
            html.Div()
        ], className='twelve columns',style={'height': '38vh'}),
        html.Br(),
        dcc.Graph(id='bottom-right', className='twelve columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False})

    ], className='six columns right'),
    html.Div([
        html.A([
                html.Img(
                    id='disclaimer-logo',
                    src='https://trendspotting.site/wp-content/uploads/sites/2/2021/10/trendspotting_logo_1.svg'
                    )
        ], href='https://www.trendspotting.site', target='_blank'),
        html.P('  '),
        html.A('trendspotting.site', href='https://www.trendspotting.site', target='_blank'),
        html.P(' is not an investment adviser and does not give recommendations on any investment products. The content of this webpage serves for general purposes only and does not take into account your individual preferences, objectives and specific financial circumstances.')
    ], id='disclaimer')
])


# token_id -> type
@callback(Output(component_id='type-dropdown', component_property='value'),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data].valueType
    src = src.iloc[0]
    return src


# type -> token_id
@callback(Output("url-dropdown", 'value'),
            [Input(component_id='type-dropdown', component_property='value')],
            [Input(component_id="url-dropdown", component_property='value')])

def update_body_image(type, token):
    type_sel = dfv[dfv['token_id']==token].valueType
    type_sel = type_sel.iloc[0]
    if type_sel == type:
        return token
    else:
        src = dfv[dfv['valueType']==type].token_id.min()
        return src


# token_id
@callback(Output("textarea-output-token_id", "children"),
            [Input("url-dropdown", 'value')])
            # Input('my-bar', 'hoverData')])

def update_body_image(filterData):
    src = dfv[dfv['token_id']==filterData].token_id.iloc[0]
    src = '#{}'.format(src)
    return src


# traits_number
@callback(Output("textarea-output-traits_number", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data].traits_number
    src = src.iloc[0]
    return src

# sell_order_price_eth
@callback(Output("textarea-output-sell_order_price_eth", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data].current_sale_price.round(4)
    src = 'Currently Not For Sale' if pd.isna(src.iloc[0]) == True else src.iloc[0]
    return src

# last_sale_date
@callback(Output("textarea-output-last_sale_date", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[(dfv['token_id']==hover_data)] 
    src = 'No Historic Sale Data Available' if src['sale_date'].isnull().values.all() else src['sale_date'].max()
    return src

# price_original_eth
@callback(Output("textarea-output-price_original_eth", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data]
    src = 'No Historic Sale Data Available' if src['sale_date'].isnull().values.all() else src[(src['sale_date']==src['sale_date'].max())].sale_price.round(4)
    return src

# pred_price
@callback(Output("textarea-output-pred_price", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data].pred_price.round(4)
    src = 'No Predicted Price' if pd.isna(src.iloc[0]) == True else src.iloc[0]
    return src

# num_sales
@callback(Output("textarea-output-num_sales", "children"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data]
    src = src['sale_date'].count()
    return src

# Image
@callback(Output("asset-image", "src"),
            [Input("url-dropdown", 'value')])

def update_body_image(hover_data):
    src = dfv[dfv['token_id']==hover_data].image_url
    src = src.iloc[0]
    return src


# Trait Uniqueness
@callback(Output(component_id='bottom-left', component_property='figure'),
             [Input(component_id="url-dropdown", component_property='value')])

def display_value(token_chosen):
    shaps = [col for col in dfv if (col.startswith('trait_count') & ~col.endswith('Color'))]
    dfv_fltrd = dfv[dfv['token_id']==token_chosen]
    dfv_fltrd = dfv_fltrd[dfv_fltrd['flag_is_latest'] == 1]
    dfv_fltrd = dfv_fltrd.iloc[0]
    dfv_t = dfv_fltrd.transpose().reset_index()
    dfv_t.rename(columns={dfv_t.columns[0]: 'variable', dfv_t.columns[1]: 'value'},inplace = True)
    dfv_t = dfv_t[dfv_t['variable'].isin(shaps)]
    dfv_t['value'] = 100* (20000 - dfv_t['value'].astype(float)) / 20000
    dfv_t['value'] = dfv_t['value'].round(1)
    dfv_t['value_abs'] = dfv_t['value'].abs()
    dfv_t = dfv_t.nlargest(20, 'value_abs')
    dfv_t['variable'] = dfv_t['variable'].str.replace('trait_count','')
    dfv_t = dfv_t.sort_values(by=['value'], ascending=True)
    background = '#fff'

    fig = go.Figure()
  
    fig.add_trace(go.Bar(name='count', 
                        x=dfv_t['value'].round(3), 
                        y=dfv_t['variable'],
                        marker = dict(
                            color = '#00a0b0'
                            ,line = dict(width=2, color='#fff')
                            ,opacity=0.3
                        ),   
                        opacity=0.3, 
                        orientation='h',
                        hovertemplate=
                        "<b>%{y}</b><br>" +
                        "Uniqueness: %{x:%.1f}% of characters have a different trait value<br>" +
                        "<extra></extra>"
                        ),
                    )
    fig.add_trace(go.Scatter(x = dfv_t['value'].round(3), 
                        y = dfv_t['variable'],
                        mode = 'markers',
                        marker = dict(
                            size = 5
                            ,color = '#aaa'
                            ,line= dict(width=1, color='#fff')
                        ),
                        hovertemplate=
                        "<b>%{y}</b><br>" +
                        "Uniqueness: %{x:%.1f}% of characters have a different trait value<br>" +
                        "<extra></extra>",
                        xaxis="x2")
                    )

    fig.add_annotation(text="Uniqueness of Traits Indicator",
            xref="paper", yref="paper",
            x=0.5, y=0.5, showarrow=False,
            font=dict(
                family="Futura, Helvetica, sans-serif",
                size=22,
                color="#333"
                ),
            align="center",
            bordercolor="#fff",
            borderwidth=0,
            borderpad=0,
            bgcolor='rgba(250, 250, 250, 0.1)',
            opacity=1
        )

    # Change the bar mode
    fig.update_layout(
        title={
        'text': "<br>Trait uniqueness % <br><sup style='color:#aaa;'>Calculated as a total proportion of tokens with a different trait value.</sup>",
        'y':0.975,
        'x':0,
        'xanchor': 'left',
        'yanchor': 'top',
        'font': dict(
            family="Futura, Helvetica, sans-serif",
            size=12.5,
            color="#222"
            ),
        },
        xaxis = {'title':'',
                'titlefont': dict(family='sans-serif', size=10, color='#666'),
                'tickangle': 0,
                'showline': True,
                'linewidth': 0.1,
                'linecolor': '#fff',
                # 'tickformat': ',.0',
                'showspikes': True,
                'spikethickness': 0.5,
                # 'range': [0,1],
                'showgrid': True,
                'gridcolor': '#eee',
                'gridwidth': 0.1
                },
        xaxis2 = {'title':'',
            'titlefont': dict(family='sans-serif', size=10, color='#666'),
            'tickangle': 0,
            'showline': False,
            'linewidth': 0,
            'linecolor': '#fff',
            'showticklabels': False,
            'tickformat': '',
            'showspikes': False,
            'spikethickness': 0.5,
            # 'range': [0,1],
            'showgrid': False,
            'gridcolor': '#eee',
            'gridwidth': 0.1,
            'side':"top",
            'overlaying':'x'
            },
        yaxis = {'title': '',
                'titlefont': dict(family='sans-serif', size=10, color='#222'),
                'ticksuffix': "  ",
                'showspikes': False,
                'spikethickness': 0.5,
                'spikedash': 'solid',
                'showgrid': False,
                'gridcolor': '#fff',
                'gridwidth': 0.01,
                },
        margin = {'l': 0, 'b': 0, 't': 50, 'r': 0},
        showlegend = False,
        # legend = {'x': 0.9, 'y': 1, 'font': dict(family='sans-serif', size=12, color='#222')},#, 'title': dict(text=' WORD ', font=dict(size=15))},
        plot_bgcolor = background,
        paper_bgcolor = background,
        hovermode = 'closest',
        font = dict(
            family="Futura, Helvetica, sans-serif",
            size=10,
            color="#666"
            )
        )
    # Customize aspect
    fig.update_traces(marker_line_color='rgba(200, 200, 200, 1)',
                    marker_line_width=1, opacity=0.3)

    return fig

# Sale History
@callback(
    Output(component_id='top-center', component_property='figure'),
    [Input(component_id='url-dropdown', component_property='value')]
)

def display_value(token_chosen):
    dfv_fltrd2 = dfv[dfv['token_id']==token_chosen]
    dfv_fltrd2 = dfv_fltrd2.sort_values(by=['sale_date'], ascending=True)
    colors = ['#ee0000','#edc951','#00a0b0','#336699','#8d2867','#949494','#4f372d','#eb6841','#2d454f']
    ci_colors = ['#ee0000','#00a0b0']
    background = '#fff'
    fig_none = go.Figure()
    fig_none.add_trace(go.Scatter(
        x=[0, 1, 2, 3, 4, 5, 6, 7, 8, 10],
        y=[0, 4, 5, 1, 2, 3, 2, 4, 2, 1],
        mode="lines+markers+text",
        marker = dict(
            size = 8
            ,color = '#ffffff'
            ,line = dict(width=0, color='#fff')
            ,opacity=0.5
            ),
        hoverinfo='skip'
    ))

    fig_none.add_annotation(text="History of Sales",
            xref="paper", yref="paper",
            x=0.5, y=0.5, showarrow=False,
            font=dict(
                family="Futura, Helvetica, sans-serif",
                size=22,
                color="#333"
                ),
            align="center",
            bordercolor="#fff",
            borderwidth=0,
            borderpad=0,
            bgcolor='rgba(250, 250, 250, 0.1)',
            opacity=1
        )

    fig_none.update_layout(
        paper_bgcolor=background,
        plot_bgcolor=background,
        margin = {'l': 70, 'b': 70, 't': 50, 'r': 50}
    )
    fig_none.update_layout(
        xaxis = dict(
            showgrid=False,
            gridcolor=background,
            zerolinecolor=background
            ),
        yaxis = dict(
            showgrid=False,
            gridcolor=background,
            zerolinecolor=background
            )
        )

    fig_none.update_traces(marker_line_color='rgba(255, 255, 255, 1)',marker_line_width=0, opacity=0.3)
    fig_none.update_xaxes(showticklabels=False)
    fig_none.update_yaxes(showticklabels=False)

    if dfv_fltrd2['sale_date'].isnull().values.all() == True:
        return fig_none

    else:
        fig = go.Figure()
        # fig = make_subplots(specs=[[{"secondary_y": True}]])

        fig.add_trace(go.Scatter(
                            name="Sale Price",
                            x = dfv_fltrd2['sale_date'],
                            y = dfv_fltrd2['sale_price'],
                            text=dfv_fltrd2['token_id'],
                            customdata=dfv_fltrd2['sale_date'],
                            mode = 'lines+markers',
                            marker = dict(
                                size = 14
                                ,color = '#00a0b0'
                                ,line = dict(width=1, color='#fff')
                                ,opacity=0.3
                            ),
                            hovertemplate=
                            "<b>Meebit #%{text}</b><br>" +
                            "Sale Price (ETH): %{y:.2f}<br>" +
                            "on %{x}<br>" +
                            "<extra></extra>"
                            )
                            # secondary_y=True
                        )

        fig.add_annotation(text="History of Sales",
                xref="paper", yref="paper",
                x=0.5, y=0.5, showarrow=False,
                font=dict(
                    family="Futura, Helvetica, sans-serif",
                    size=22,
                    color="#333"
                    ),
                align="center",
                bordercolor="#fff",
                borderwidth=0,
                borderpad=0,
                bgcolor='rgba(250, 250, 250, 0.1)',
                opacity=1
            )
        # Change the bar mode
        fig.update_layout(
            title={
            'text': "<br>Sale history <br><sup style='color:#aaa;'>Data sourced from etherscan.io</sup>",
            'y':0.975,
            'x':0,
            'xanchor': 'left',
            'yanchor': 'top',
            'font': dict(
                family="Futura, Helvetica, sans-serif",
                size=12.5,
                color="#222"
                ),
            },
            xaxis = {'title':'',
                    'titlefont': dict(family='sans-serif', size=10, color='#666'),
                    'tickangle': 0,
                    'showline': True,
                    'linewidth': 0.1,
                    'linecolor': '#fff',
                    # 'tickformat': ',.0',
                    'showspikes': True,
                    'spikethickness': 0.5,
                    'showgrid': True,
                    'gridcolor': '#eee',
                    'gridwidth': 0.1
                    },
            yaxis = {'title': 'Price',
                    'titlefont': dict(family='sans-serif', size=10, color='#222'),
                    'ticksuffix': "  ",
                    'showspikes': False,
                    'spikethickness': 0.5,
                    'spikedash': 'solid',
                    'showgrid': False,
                    'gridcolor': '#fff',
                    'gridwidth': 0.01,
                    },
            margin = {'l': 0, 'b': 0, 't': 50, 'r': 0},
            showlegend = False,
            plot_bgcolor = background,
            paper_bgcolor = background,
            hovermode = 'closest',
            font = dict(
                family="Futura, Helvetica, sans-serif",
                size=11,
                color="#666"
                ),
            legend=dict(
                yanchor="top",
                y=0.99,
                xanchor="left",
                x=0.05)
            )

        fig.update_xaxes(categoryorder='total ascending')

        return fig


# Shap Values
@callback(
    Output(component_id='bottom-center', component_property='figure'),
    [Input(component_id="url-dropdown", component_property='value')]
)

def display_value(token_chosen):
    # dfv_fltrd = dfv.iloc[['shap_valueType_DoublePig','shap_valueType_Visitor','shap_trait_countType','shap_valueType_Robot','shap_valueShirt_CGAShirt','shap_valueType_Human','shap_valueShirt_GlyphShirt','shap_valueType_Skeleton','shap_valueShoes_NeonSneakers','shap_valueShoes_LLAlien','shap_valueBeardColor_Blond','shap_valueHat_Headphones','shap_valueGlasses_3D','shap_valueOvershirtColor_Posh','shap_valueNecklace_GoldChain','shap_valueHairStyle_FieryMohawk','shap_valueHairStyle_Bald','shap_valueShirt_PunkTee','shap_valueShirt_HoodieUp','shap_valueShirtColor_Black','shap_valueBeard_MedicalMask','shap_valueType_Elephant','shap_trait_countBeardColor','shap_trait_countOvershirtColor','shap_valueHat_TruckerCap','shap_valueShoes_LL86','shap_valueShoesColor_White','shap_trait_countGlasses','shap_month_index','shap_eth_price_usd','shap_min_trait_count','shap_valueHatColor_Gray','shap_valueOvershirtColor_White','shap_traits_number','shap_valueShirtColor_Gray','shap_valueBeardColor_Silver','shap_valueShirt_SnoutzHoodie','shap_valuePants_RegularPants','shap_valueType_Pig','shap_valueShirt_FlamingoTee','shap_valueGlassesColor_Charcoal','shap_valuePantsColor_Camo','shap_valueOvershirtColor_Argyle','shap_valuePantsColor_BlueCamo','shap_valueHat_Cap','shap_valuePants_RippedJeans','shap_valueShoes_Classic','shap_num_sales','shap_valuePants_SuitPants','shap_valueShoes_LLMoonboots','shap_valueBeard_Mustache','shap_trait_countTattooMotif','shap_valueShirtColor_White','shap_valueHairColor_DyedRed','shap_valuePants_CargoPants','shap_valueShirtColor_BlueCamo','shap_valueShirtColor_Camo','shap_valueOvershirtColor_Luxe','shap_valueShoesColor_Gray','shap_valueShirt_TiedyedTee','shap_valueShirt_Hoodie','shap_valueHairStyle_Halfshaved','shap_trait_countHairStyle','shap_valueShoesColor_Purple','shap_valueHat_SnoutzCap','shap_valueBeard_BikerMustache','shap_valueHairStyle_Mohawk','shap_valueGlassesColor_White','shap_valueShoesColor_Yellow','shap_valueShirtColor_Argyle','shap_valueShoes_Running','shap_valueShirt_InvaderTee','shap_trait_countHatColor','shap_valueGlasses_Elvis','shap_valueShoes_LLBabyBlue','shap_valueHairColor_Blond','shap_valueShirtColor_Green','shap_valueHairColor_Blonde','shap_valueHatColor_Red','shap_valueEarring_GoldEarring','shap_trait_countShoesColor','shap_valueShirt_Hawaiian','shap_trait_countHairColor','shap_trait_countShirtColor','shap_valueOvershirtColor_GreenPlaid','shap_valueHatColor_Yellow','shap_valueHat_BackwardsCap','shap_valueHat_WoolHat','shap_valueShoes_Canvas','shap_valueShirt_SnoutzTee','shap_valuePantsColor_Argyle','shap_trait_countPantsColor','shap_valueShirt_SkullTee','shap_valueBeard_Muttonchops','shap_valueHairColor_Blue','shap_valueShirtColor_Yellow','shap_valueShoes_LLRGB','shap_valueShirt_GhostTee','shap_valueHatColor_White','shap_valueGlasses_Specs','shap_valueOvershirtColor_BlueCamo','shap_valueHatColor_Purple','shap_valuePantsColor_Yellow','shap_valueShirt_Jersey','shap_valueShoes_Sneakers','shap_valueShirtColor_Posh','shap_valueShirt_StylizedHoodie','shap_valueHairColor_Dark','shap_valueOvershirtColor_RedPlaid','shap_valueShirt_HeartTee','shap_valueHairStyle_VeryLong','shap_valueShoes_Basketball','shap_valuePantsColor_DarkRed','shap_valueHat_Brimmed','shap_valuePants_Trackpants','shap_valueShirt_Suit','shap_valuePantsColor_Purple','shap_valuePants_Skirt','shap_valuePantsColor_DarkGray','shap_trait_countOvershirt','shap_valuePants_AthleticShorts','shap_valueShirtColor_LeopardPrint','shap_valueShoes_LLHighTops','shap_valuePantsColor_GreenPlaid','shap_trait_countNecklace','shap_valueHat_Bandana','shap_valueBeard_Big','shap_valueHatColor_Black','shap_valueOvershirtColor_Red','shap_valueOvershirtColor_Black','shap_valuePantsColor_Gray']]
    shaps = [col for col in dfv if col.startswith('shap_value')]
    dfv_fltrd = dfv[dfv['token_id']==token_chosen]
    dfv_fltrd = dfv_fltrd[dfv_fltrd['flag_is_latest'] == 1]
    dfv_fltrd = dfv_fltrd.iloc[0]
    dfv_t = dfv_fltrd.transpose().reset_index()
    dfv_t.rename(columns={dfv_t.columns[0]: 'variable', dfv_t.columns[1]: 'value'},inplace = True)
    dfv_t = dfv_t[dfv_t['variable'].isin(shaps)]
    dfv_t['value'] = dfv_t['value'].astype(float)
    dfv_t['value_abs'] = dfv_t['value'].abs()
    dfv_t = dfv_t.nlargest(10, 'value_abs')
    dfv_t['variable'] = dfv_t['variable'].str.replace('shap_value','')
    dfv_t = dfv_t.sort_values(by=['value'], ascending=False)
    background = '#fff'

    fig = go.Figure()

    # Draw lines
    for i in range(0, len(dfv_t)):
        fig.add_shape(type='line',
                        x0 = 0, y0 = i,
                        x1 = dfv_t['value'].iloc[i],
                        y1 = i,
                        opacity=0.3,
                        line=dict(
                            color='#ee0000'
                            , width = 3 
                        ))

    # Draw points
    fig.add_trace(go.Scatter(x = dfv_t['value'], 
                            y = dfv_t['variable'],
                            mode = 'markers',
                            marker = dict(color='#ee0000'
                                ,size = 10
                                ,colorscale=[[0.0, "rgb(238, 0, 0)"],
                                [1.0, "rgb(0, 160, 176)"]]   
                            ),
                            opacity=0.3,
                            hovertemplate=
                            "<b>%{y}</b><br>" +
                            "Impact on price: %{x:,.3f}<br>" +
                            "<extra></extra>",
                            marker_size  = 10))

    fig.add_annotation(text="Trait Impact on Price",
            xref="paper", yref="paper",
            x=0.5, y=0.5, showarrow=False,
            font=dict(
                family="Futura, Helvetica, sans-serif",
                size=22,
                color="#333"
                ),
            align="center",
            bordercolor="#fff",
            borderwidth=0,
            borderpad=0,
            bgcolor='rgba(250, 250, 250, 0.1)',
            opacity=1
        )

    # Change the bar mode
    fig.update_layout(
        title={
        'text': "<br>Traits by impact on price <br><sup style='color:#aaa;'>Based on Shapley values of the prediction model</sup>",
        'y':0.975,
        'x':0,
        'xanchor': 'left',
        'yanchor': 'top',
        'font': dict(
            family="Futura, Helvetica, sans-serif",
            size=12.5,
            color="#222"
            ),
        },
        xaxis = {'titlefont': dict(family='sans-serif', size=10, color='#222'),
                'tickangle': 0,
                'showline': True,
                'linewidth': 0.3,
                'linecolor': '#fff',
                'zeroline': True,
                'zerolinecolor': '#aaa',  
                'zerolinewidth': 0.3,
                'showspikes': True,
                'spikethickness': 0.5,
                'showgrid': True,
                'gridcolor': '#eee',
                'gridwidth': 0.1
                },
        yaxis = {'title': '',
                'titlefont': dict(family='sans-serif', size=10, color='#222'),
                'ticksuffix': "  ",
                'showspikes': False,
                'spikethickness': 0.5,
                'spikedash': 'solid',
                'showgrid': False,
                'gridcolor': '#aaa',
                'gridwidth': 0.1
                },
        margin = {'l': 0, 'b': 15, 't': 50, 'r': 0},
        # legend = {'x': 0.9, 'y': 1, 'font': dict(family='sans-serif', size=12, color='#222')},#, 'title': dict(text=' WORD ', font=dict(size=15))},
        plot_bgcolor = background,
        paper_bgcolor = background,
        hovermode = 'closest',
        font = dict(
            family="Helvetica, sans-serif",
            size=11,
            color="#7f7f7f"
            )
        )
    return fig

# Violin Plot
@callback(
    Output(component_id='bottom-right', component_property='figure'),
    [Input(component_id='type-dropdown', component_property='value')]
)

def display_value(type_chosen):
    dfv_fltrd = dfv[dfv['valueType'] == type_chosen]
    clr = dfv_fltrd['valuetype_cat'].unique()
    dfv_fltrd2 = dfv_fltrd.groupby('valueType').agg({'sale_price':'median'}, axis="columns")
    dfv_fltrd2 = dfv_fltrd2.sort_values(by=['sale_price'], ascending=True)
    types = dfv_fltrd2.index.tolist()

    colors = ['#ee0000','#edc951','#00a0b0','#336699','#4f372d','#949494','#8d2867','#eb6841','#2d454f']

    fig2 = go.Figure()

    fig2.add_trace(go.Violin(
                            x=dfv_fltrd['sale_price'].round(4),
                            name=type_chosen,
                            fillcolor=colors[2],
                            box_visible=False, 
                            # line_color=colors[ix],
                            line_color='#ddd',
                            line_width=1,
                            meanline_visible=True,
                            points='all',
                            showlegend=False,
                            marker= dict(color=colors[5], 
                                    opacity=0.3, 
                                    size=10,
                                    line=dict(width=0,color='#ddd')
                                    )
                            # hoverinfo="skip"
                            ))

    fig2.add_annotation(text="Price Distribution View",
            xref="paper", yref="paper",
            x=0.5, y=0.5, showarrow=False,
            font=dict(
                family="Futura, Helvetica, sans-serif",
                size=22,
                color="#333"
                ),
            align="center",
            bordercolor="#fff",
            borderwidth=0,
            borderpad=0,
            bgcolor='rgba(250, 250, 250, 0.1)',
            opacity=1
        )

    fig2.update_layout(
            title={
            'text': "<br>Price distribution of the type<br><sup style='color:#aaa;'>Includes historic prices achieved by the same token.</sup>",
            'y':0.975,
            'x':0,
            'xanchor': 'left',
            'yanchor': 'top',
            'font': dict(
                family="Futura, Helvetica, sans-serif",
                size=12.5,
                color="#222"
                ),            
            },
            # barmode='stack', 
            xaxis = {'title': 'Value in ETH',
                    'titlefont': dict(family='Futura, sans-serif', size=10, color='#666'),
                    'tickangle': 0,
                    'showline': True,
                    'linewidth': 0.1,
                    'linecolor': '#fff',
                    # 'tickformat': ',.0',
                    'showspikes': True,
                    'spikethickness': 0.5,
                    # 'range': [0,1],
                    'showgrid': True,
                    'gridcolor': '#eee',
                    'gridwidth': 0.1,
                    'zeroline':True
                    },
            yaxis = {'title': '',
                    'titlefont': dict(family='sans-serif', size=12, color='#222'),
                    'ticksuffix': "  ",
                    'showline': True,
                    'showspikes': False,
                    'spikethickness': 0.5,
                    'spikedash': 'solid',
                    'showgrid': False,
                    'gridcolor': '#fff',
                    'gridwidth': 0.01,
                    'zeroline':True
                    },
            margin = {'l': 10, 'b': 0, 't': 50, 'r': 0},
            plot_bgcolor = "#fff",
            font = dict(
                family="Futura, Helvetica, sans-serif",
                size=11,
                color="#666"
            ),
            paper_bgcolor = "#fff",
            hovermode = 'closest',
            )
    # Customize aspect
    fig2.data[0].update(span = [-1/1.9*dfv_fltrd['sale_price'].max(), dfv_fltrd['sale_price'].max()*1.5], spanmode='manual');
    fig2.update_traces(marker_line_color='rgba(200, 200, 200, 1)', marker_line_width=1, opacity=0.3, jitter=0, scalemode='count')

    return fig2

# Table
# Highlight selected column
@callback(
    Output('top-right', 'style_data_conditional'),
    [Input('top-right', 'selected_columns')]
)
def update_styles(selected_columns):
    return [{
        'if': {'column_id': i},
        'background_color': '#D2F3FF'
    } for i in selected_columns]

# Update on type filter
@callback(
    Output(component_id='top-right', component_property='data'),
    [Input(component_id='type-dropdown', component_property='value')]
)
def update_df_div(type_chosen):
    dfv_fltrd = dfv[dfv['valueType'] == type_chosen]
    dfv_fltrd = dfv_fltrd[dfv_fltrd['flag_is_latest'] == 1]
    dfv_fltrd = dfv_fltrd.sort_values(by=['sale_price'], ascending=True)
    dfv_fltrd['pred_price'] = dfv_fltrd['pred_price'].round(4)
    dfv_fltrd['opportunity_coef'] = dfv_fltrd['opportunity_coef'].round(4)
    dfv_fltrd['current_sale_price'] = dfv_fltrd['current_sale_price'].round(4)
    data = dfv_fltrd[['token_id','current_sale_price','pred_price', 'opportunity_coef']].to_dict('records')
    return data

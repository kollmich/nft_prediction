from dash import Dash, callback, html, dcc, dash_table, Input, Output, State, MATCH, ALL
from dash import html
from dash.dependencies import Input, Output, State
# from pyarrow import csv
from plotly.subplots import make_subplots
import dash
import dash_labs as dl
import dash_bootstrap_components as dbc
import analytics
import datetime
from collections import OrderedDict

analytics.write_key = 'KhqE5KXcLJ4cxmEGQlcuIFqq9zuCMDfr'

app = dash.Dash(
    __name__,  plugins=[dl.plugins.pages], suppress_callback_exceptions=True
    ,external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP], meta_tags=[{'name': 'viewport','content': 'width=device-width, initial-scale=1.0'}]
    ,update_title='Loading Data...'
)

app.title = 'NFT Valuation'

modal_1 = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("1. Dashboard")),
        dbc.ModalBody("The dashboard tracks past token sales and tries to make sense of the link between character traits and token prices. General token characteristics like image, number of traits, current listing price and predicted price are shown in the top-left section. The history of their past sales is available in the center-top chart."),
        dbc.ModalFooter(
            dbc.Button(
                "Next",
                id="open-toggle-modal-2",
                className="ms-auto",
                n_clicks=0,
            )
        ),
    ],
    id="toggle-modal-1",
    is_open=False,
    centered=True,
    # style={"max-width": "none", "width": "50%"}
)

modal_2 = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("2. Uniqueness")),
        dbc.ModalBody("Most NFT collections are made up by a finite number of characters. Each usually has a different combination of traits, which makes them unique. The bottom-left chart indicates what is the uniqueness of each character trait. It is measured as a total share of characters that have a different value of this trait."),
        dbc.ModalFooter(
            dbc.Button(
                "Next",
                id="open-toggle-modal-3",
                className="ms-auto",
                n_clicks=0,
            )
        ),
    ],
    id="toggle-modal-2",
    is_open=False,
    centered=True,
)

modal_3 = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("3. Price Prediction")),
        dbc.ModalBody("The prices are then estimated by a machine learning algorithm. The accuracy of predictions largely depends on the available data and honest practices of the token traders."),
        dbc.ModalFooter(
            dbc.Button(
                "Next",
                id="open-toggle-modal-4",
                className="ms-auto",
                n_clicks=0,
            )
        ),
    ],
    id="toggle-modal-3",
    is_open=False,
    centered=True,
)

modal_4 = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("4. Before you buy a token")),
        dbc.ModalBody("As a general rule, thoroughly investigate historic prices and relative uniqueness of the token you wish to buy. Do not rely on the prediction outputs only!"),
        dbc.ModalFooter(
            dbc.Button(
                "Back to Start",
                id="open-toggle-modal-1",
                className="ms-auto",
                n_clicks=0,
            )
        ),
    ],
    id="toggle-modal-4",
    is_open=False,
    centered=True,
)

offcanvas = html.Div(
    [
        dcc.Location(id='url', refresh=False),
        dbc.Button("Collections",id="open-offcanvas",outline=True,color="secondary",size="sm",n_clicks=0),
        dbc.Button("Take a Tour",id="open-tour",outline=True,color="secondary",size="sm",n_clicks=0),
        dbc.Offcanvas(
            dbc.ListGroup(
                [
                    dbc.ListGroupItem(page["name"], href=page["path"])
                    for page in dash.page_registry.values() 
                    if page["module"] != "pages.not_found_404"
                    # dbc.ListGroupItem("Item 1", href="google.com")
                ]
            ),
            # dbc.ListGroup(
            #     [
            #         dbc.ListGroupItem("Item 1", href="google.com"),
            #     ]
            # ),
            # placement="end",
            id="offcanvas",
            is_open=False,
        ),
        # dbc.Spinner(children=[html.Div(id='intermediate-value', style={'display': 'none'})],spinner_style={"width": "5rem", "height": "5rem"}, color="#ee0000", type="border", fullscreen=True, delay_hide=100),
        # dcc.Loading(children=[html.Div(id='intermediate-value', style={'display': 'none'})], color="#ee0000", type="dot", fullscreen=True,),
        html.Div(id='intermediate-value', style={'display': 'none'}),
        html.Div(
            [
                modal_1,
                modal_2,
                modal_3,
                modal_4
            ]
        ),
    ],
    className="my-3"
)

def serve_layout():
    analytics.identify('11111111', {
            'name': 'Anonymous',
            'email': 'xxx@yyy.com',
            'created_at': datetime.datetime.now()
        })
    analytics.track('11111111', 'visit/refresh', {
    'plan': 'Free'
    })
    return dbc.Container(
        [offcanvas, 
        dbc.Spinner(children=[dl.plugins.page_container], fullscreen=True, delay_hide=500, delay_show=700, spinnerClassName="spinner",
        spinner_style={"width": "10rem", "height": "10rem"}, color="#ee0000", type="border"),],
        fluid=True,
    )

app.layout = serve_layout

@app.callback(
    Output("offcanvas", "is_open"),
    Input("open-offcanvas", "n_clicks"),
    [State("offcanvas", "is_open")],
)
def toggle_offcanvas(n1, is_open):
    if n1:
        return not is_open
    return is_open

@app.callback(Output('intermediate-value', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    analytics.track(pathname, 'clicked', {
    'plan': 'Free'
    })
    return pathname

# TOUR CALLBACKS
@app.callback(
    Output("toggle-modal-1", "is_open"),
    [
        Input("open-tour", "n_clicks"),
        Input("open-toggle-modal-1", "n_clicks"),
        Input("open-toggle-modal-2", "n_clicks"),
    ],
    [State("toggle-modal-1", "is_open")],
)
def toggle_modal_1(n0, n1, n2, is_open):
    if n0 or n1 or n2:
        return not is_open
    return is_open

@app.callback(
    Output("toggle-modal-2", "is_open"),
    [
        Input("open-toggle-modal-2", "n_clicks"),
        Input("open-toggle-modal-3", "n_clicks"),
    ],
    [State("toggle-modal-2", "is_open")],
)
def toggle_modal_2(n2, n1, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
    Output("toggle-modal-3", "is_open"),
    [
        Input("open-toggle-modal-3", "n_clicks"),
        Input("open-toggle-modal-4", "n_clicks"),
    ],
    [State("toggle-modal-3", "is_open")],
)
def toggle_modal_3(n3, n4, is_open):
    if n4 or n3:
        return not is_open
    return is_open

@app.callback(
    Output("toggle-modal-4", "is_open"),
    [
        Input("open-toggle-modal-4", "n_clicks"),
        Input("open-toggle-modal-1", "n_clicks"),
    ],
    [State("toggle-modal-4", "is_open")],
)
def toggle_modal_3(n4, n1, is_open):
    if n1 or n4:
        return not is_open
    return is_open

if __name__ == '__main__':
    app.run_server(debug=False, port=8001)

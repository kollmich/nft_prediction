import dash
dash.register_page(__name__, title='Coolcats', name='Coolcats') 
from dash import dcc, html, callback, dash_table
import pandas as pd
import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots
import pandas as pd
import time
import dash_bootstrap_components as dbc
from pages.template import footer, get_callbacks

start = time.time()

collection = 'coolcats'
collection_long = 'Coolcats'

main_trait = 'valueface'

colors = ['#ee0000','#edc951','#00a0b0','#336699','#8d2867','#949494','#4f372d','#eb6841','#2d454f']
dfv = pd.read_csv('https://storage.googleapis.com/trendspotting_general_storage/dw/collections/visualisation_input/{0}_daily.csv'.format(collection)
    ,engine="pyarrow"
    ,sep='\t'
    ,low_memory=True
    ,dtype={
        "token_id": "int16",
        "contract_description": "object",
        "image_url": "object",
        "url": "object",
        "rate_date_current":  object,
        "eth_price_usd_current": "float64", 
        "month_index": "float64", 
        "sale_date": object, 
        "flag_is_latest": "int16", 
        "sale_price": "float64", 
        "pred_price": "float64",
        "timestamp_extracted": "object", 
        "current_sale_price": "float64", 
        "opportunity_coef": "float64", 
        "eth_price_usd": "float64", 
        "num_sales": "int16", 
        "rarity_ranking": "int16",
        "traits_number": "int16", 
        "trait_countbody": "int16",
        "trait_countface": "int16",
        "trait_counthats": "int16",
        "trait_countshirt": "int16",
        "trait_counttier": "int16",
        "valuebody": "category",
        "valueface": "category",
        "valuehats": "category",
        "valueshirt": "category",
        "valuetier": "category",
        "shap_valueface": "float64", 
        "shap_valuehats": "float64", 
        "shap_valueshirt": "float64", 
        "shap_valuetier": "float64", 
        "shap_valueOther": "float64",
        "timestamp": "object"
        }
)
dfv['valuetype_cat'] = dfv[main_trait].astype('category').cat.codes
dfv['token_id'] = dfv['token_id'].astype('int16')
dfv['pred_price'] = dfv['pred_price'].astype(float)
dfv['color'] = np.where(dfv['opportunity_coef']> 1, 1, 0)

initial_active_cell = {"row": 1, "column": 0, "column_id": "token_id", "row_id": 1}
dfv['id'] = dfv['token_id']
dfv.set_index('id', inplace=True, drop=False)

refresh_date_sale = dfv['timestamp_extracted'].max()
refresh_date = dfv['timestamp'].min()
description = dfv['contract_description'].str.rsplit('.', n=1).str.get(0).max()

types_list = sorted(dfv[main_trait].unique())
asset_list = sorted(dfv.token_id.unique()) 

end = time.time()
print(end - start)

layout = html.Div([
    dcc.Location(id='url', refresh=False),

    html.H3('{0}'.format(collection_long), id='title-main'),
    html.Div([
        html.H6('Character attributes, price predictions and current purchase opportunities. Updated on {0} '.format(refresh_date), style={"margin-left": "5px"}),
    ], className='topnav'),

    html.Div([
        # html.Div(id="url_meebits", children=[], className='six columns'),
        html.Div(dcc.Dropdown(
            id='url-dropdown_{0}'.format(collection), value=1, clearable=False,
            # persistence=True, persistence_type='memory',
            options=[{'label': x, 'value': x} for x in asset_list]
        ), className='six columns', id='url-dropdown_div_{0}'),
        # html.Div(dcc.Dropdown(
        #     id='type-dropdown_{0}'.format(collection), value='tired', clearable=False,
        #     options=[{'label': x, 'value': x} for x in types_list]
        # ), className='six columns', id='type-dropdown_div_{0}')
    ], className='row'),

    html.Div([
            html.Label("As of {0}".format(refresh_date_sale), style={'margin':'0px 0px 5px 5px'}),
            dash_table.DataTable(
            id='first_{0}'.format(collection),
            # columns=[
            #     {"name": i, "id": i, "deletable": False, "selectable": True, "hideable": False}
            #     if i == "token_id"
            #     else {"name": i, "id": i, "deletable": False, "selectable": True, "hideable": False}
            #     for i in dfv[['token_id','current_sale_price','pred_price', 'opportunity_coef']].columns
            # ],
            columns=[
                {"name": "ID", "id": "token_id", "type": "string"},
                {"name": "Listing", "id": "current_sale_price", "type": "numeric", "format": {"specifier": ",.2f"}},
                {"name": "Estimate", "id": "pred_price", "type": "numeric","format": {"specifier": ",.2f"}},
                {"name": "Ratio", "id": "opportunity_coef", "type": "numeric", "format": {"specifier": ",.2f"}},
                {"name": " ", "id": "url", "type": "text", "presentation":"markdown"},
            ],
            # data=dfv[['token_id','current_sale_price','pred_price', 'opportunity_coef']].to_dict('records'),  # the contents of the table
            editable=False,              # allow editing of data inside all cells
            filter_action="none",     # allow filtering of data by user ('native') or not ('none')
            sort_action="native",       # enables data to be sorted per-column by user or not ('none')
            sort_mode="single",         # sort across 'multi' or 'single' columns
            # column_selectable="multi",  # allow users to select 'multi' or 'single' columns
            # row_selectable="multi",     # allow users to select 'multi' or 'single' rows
            row_deletable=False,         # choose if user can delete a row (True) or not (False)
            selected_columns=[],        # ids of columns that user selects
            selected_rows=[],           # indices of rows that user selects
            page_action="native",       # all data is passed to the table up-front or not ('none')
            page_current=0,             # page number that user is on
            page_size=50,                # number of rows visible per page
            style_as_list_view=True,
            style_table={'height': '33vh', 'overflowY': 'auto'},
            style_cell={                # ensure adequate header width when text is shorter than cell's text
                # 'minWidth': 95, 'maxWidth': 95, 'width': 95, 
                'font-family':"Futura, Helvetica, sans-serif",
                'textAlign': 'left',
                'padding': '0 10px',
            },
            style_cell_conditional=[
                {'if': {'column_id': 'token_id'},
                'width': '3%'},
                {'if': {'column_id': 'current_sale_price'},
                'width': '30%'},
                {'if': {'column_id': 'pred_price'},
                'width': '30%'},
                {'if': {'column_id': 'opportunity_coef'},
                'width': '35%'},
                {'if': {'column_id': 'url'},
                'width': '2%'},
            ],
            style_header={
                'backgroundColor': 'white',
                'fontWeight': 'bold',
                'border': '1px solid #eee',
                'fontSize':12.5,
                'font-family':"Futura, Helvetica, sans-serif",
                'padding': '0 10px',
            },
            style_data={                # overflow cells' content into multiple lines
                'whiteSpace': 'normal',
                'height': 'auto',
                'border': '1px solid #eee'
            }
        )], className='six columns',style={'height': '38vh'}),
    html.Br(),

    html.Div([
        html.Div([
            html.Img(
                className="asset-image",
                id="asset-image_{0}".format(collection),
                src="https://trendspotting.site/wp-content/uploads/sites/2/2022/03/transition.gif"
            ),
            html.Div([
                dbc.Button(
                    " About".format(collection_long.capitalize()),
                    id="hover-target",
                    className="bi bi-info-circle-fill",
                    color="secondary",
                    n_clicks=0,
                    style={"margin": "0px auto", "text-align": "center", "width": "90%", "display": "block", "color": "#6c757d", "border-color": "#fff", "white-space": "normal","word-wrap": "break-word"}
                ),
                dbc.Popover(
                    children=[html.P('{0}.'.format(description) , style={"font-size": "12.5px",'font-family':"Futura, Helvetica, sans-serif", "margin": "5px auto", "text-align": "center", "width": "90%;"}),],
                    target="hover-target",
                    trigger="hover",
                ),
            ], className='twelve columns', style={"max-height": "13vh"}),
        ], className='six columns'),
        html.Div([
            html.Div(['Token ID:  ',
                html.B(id='textarea-output-token_id_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Number of traits:  ',
                html.B(id='textarea-output-traits_number_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Ask Price (ETH):  ',
                html.B(id='textarea-output-sell_order_price_eth_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Times resold:  ',
                html.B(id='textarea-output-num_sales_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Last Sale:  ',
                html.B(id='textarea-output-last_sale_date_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Last Purchase Price (ETH):  ',
                html.B(id='textarea-output-price_original_eth_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Last Purchase Price (USD):  ',
                html.B(id='textarea-output-price_original_usd_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail'),
            html.Div(['Predicted Price (ETH):  ',
                html.B(id='textarea-output-pred_price_{0}'.format(collection)),
                ], style={'whiteSpace': 'pre-line'}, className='detail')
        ], className='six columns intro')
    ],id = 'second', className='six columns'),
    dcc.Graph(id='third_{0}'.format(collection), className='six columns', style={'height': '38vh'}, figure={}, config={'displayModeBar': False}),

    dcc.Graph(id='fourth_{0}'.format(collection), className='six columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False}),
    dcc.Graph(id='fifth_{0}'.format(collection), className='six columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False}),


    dcc.Graph(id='sixth_{0}'.format(collection), className='six columns',style={'height': '38vh'}, figure={}, config={'displayModeBar': False}),
    html.Div([], className='twelve columns',style={'height': '3vh'}),
],id='html'),footer

get_callbacks(collection, dfv, 'token_id', main_trait)

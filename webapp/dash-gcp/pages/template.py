from xml.dom import NoModificationAllowedErr
from dash import dcc, html, callback, dash_table
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots
import pandas as pd
import dash_bootstrap_components as dbc
import json
import datetime
from sklearn.linear_model import LinearRegression

footer = html.Div([
        html.A([
                html.Img(
                    id='disclaimer-logo',
                    src='https://trendspotting.site/wp-content/uploads/sites/2/2021/10/trendspotting_logo_1.svg'
                    )
        ], href='https://www.trendspotting.site', target='_blank'),
        html.P('  '),
        html.A('trendspotting.site', href='https://www.trendspotting.site', target='_blank'),
        html.P(' is not an investment adviser and does not give recommendations on any investment products.')
    ], id='disclaimer')

interval = dcc.Interval(
    id="load_interval", 
    n_intervals=0, 
    max_intervals=0, #<-- only run once
    interval=1
),

def custom_round(x, base=5):
    return int(base * round(float(x)/base))

def get_callbacks(collection, df, id, base_type):
    # https://dash.plotly.com/datatable/interactivity
    # define callback        
    @callback(
        Output('url-dropdown_{0}'.format(collection), 'value'),
        [Input('first_{0}'.format(collection), component_property='active_cell')],
        [Input('url-dropdown_{0}'.format(collection), component_property='value')],
        [State('url-dropdown_{0}'.format(collection), 'value')]
    )

    def display_click_data(active_cell, token_id, token_id_state):
        if active_cell:
            token_id_selected = active_cell["row_id"]

            if token_id_selected == token_id_state:
                return token_id_state
            else:
                return token_id_selected
        else:
            return token_id

    ###################################################################################################

    # MAIN CHARTS
    @callback([
        Output('textarea-output-token_id_{0}'.format(collection), 'children'),
        Output('textarea-output-traits_number_{0}'.format(collection), 'children'),
        Output('textarea-output-sell_order_price_eth_{0}'.format(collection), 'children'),
        Output('textarea-output-last_sale_date_{0}'.format(collection), 'children'),
        Output('textarea-output-price_original_eth_{0}'.format(collection), 'children'),
        Output('textarea-output-price_original_usd_{0}'.format(collection), 'children'),
        Output('textarea-output-pred_price_{0}'.format(collection), 'children'),
        Output('textarea-output-num_sales_{0}'.format(collection), 'children'),
        # Output('textarea-output-rarity_ranking_{0}'.format(collection), 'children'),
        Output('asset-image_{0}'.format(collection), 'src'),
        Output(component_id='sixth_{0}'.format(collection), component_property='figure'),
        Output(component_id='fourth_{0}'.format(collection), component_property='figure'),
        Output(component_id='fifth_{0}'.format(collection), component_property='figure'),
        Output('first_{0}'.format(collection), 'selected_cells'),
        Output('first_{0}'.format(collection), 'active_cell')
    ], [Input('url-dropdown_{0}'.format(collection), 'value')])

    def update_textarea(token_id):
        total = len(df[df['flag_is_latest'] == 1])
        dfv_token = df[df[id]==token_id]
        token = dfv_token.token_id.iloc[0]
        token = '#{}'.format(token)
        traits_no = dfv_token.traits_number.iloc[0]
        price = dfv_token.current_sale_price.round(4)
        price = 'Currently Not For Sale' if pd.isna(price.iloc[0]) == True else price.iloc[0]
        hist = 'No Historic Sale Data Available' if dfv_token['sale_date'].isnull().values.all() else dfv_token['sale_date'].max()
        hist2 = 'No Historic Sale Data Available' if dfv_token['sale_date'].isnull().values.all() else dfv_token[(dfv_token['sale_date']==dfv_token['sale_date'].max())].sale_price.round(4)
        hist3 = 'No Historic Sale Data Available' if dfv_token['sale_date'].isnull().values.all() else round(dfv_token[(dfv_token['sale_date']==dfv_token['sale_date'].max())].sale_price.round(4) * dfv_token[(dfv_token['sale_date']==dfv_token['sale_date'].max())].eth_price_usd.round(4),0)
        pred = dfv_token.pred_price.round(4)
        pred = 'No Predicted Price' if pd.isna(pred.iloc[0]) == True else pred.iloc[0]
        num_sales = dfv_token['sale_date'].count()
        rarity_ranking = dfv_token.rarity_ranking.round(4)
        image = dfv_token.image_url.iloc[0]

        # TRAIT UNIQUENESS
        counts = [col for col in df if (col.startswith('trait_count') & ~col.endswith('Color'))]
        traits = [col for col in df if (col.startswith('value') & ~col.endswith('Color'))]

        dfv_fltrd = dfv_token[dfv_token['flag_is_latest'] == 1]
        dfv_fltrd = dfv_fltrd.iloc[0]
        dfv_t = dfv_fltrd.transpose().reset_index()
        dfv_t.rename(columns={dfv_t.columns[0]: 'variable', dfv_t.columns[1]: 'value'},inplace = True)
        dfv_counts = dfv_t[dfv_t['variable'].isin(counts)]
        dfv_counts.index = dfv_counts['variable'].str.replace('trait_count','')

        dfv_traits = dfv_t[dfv_t['variable'].isin(traits)]
        dfv_traits.index = dfv_traits['variable'].str.replace('value','')
        dfv_counts = pd.concat([dfv_counts, dfv_traits], axis=1, join="inner")
        dfv_counts.columns = ['variable','value','variable_trait','value_trait']

        dfv_counts['value'] = 100* (total - dfv_counts['value'].astype(float)) / total
        dfv_counts['value'] = dfv_counts['value'].round(1)
        dfv_counts['value_abs'] = dfv_counts['value'].abs()
        dfv_counts = dfv_counts.nlargest(20, 'value_abs')
        dfv_counts['variable'] = dfv_counts['variable'].str.replace('trait_count','')
        dfv_counts = dfv_counts.sort_values(by=['value'], ascending=True)
        dfv_counts['value_trait'] = dfv_counts['value_trait'].fillna('None')
        dfv_counts = dfv_counts.replace(r'^\s*$', 'None', regex=True)
        background = '#fff'

        fig = go.Figure()

        fig.add_trace(go.Bar(name='count', 
                            x=dfv_counts['value'].round(3), 
                            y=dfv_counts['variable'],
                            customdata= dfv_counts['value_trait'],
                            marker = dict(
                                color = '#00a0b0'
                                ,line = dict(width=2, color='#fff')
                                ,opacity=0.8
                            ),   
                            opacity=1, 
                            orientation='h',
                            hovertemplate=
                            "<b>%{y}- %{customdata}</b><br>" +
                            # "Trait Value: %{customdata}<br>"+
                            "Uniqueness: %{x:%.1f}% of characters have a different kind of %{y}<br>" +
                            "<extra></extra>"
                            ),
                        )

        # Change the bar mode
        fig.update_layout(
            title={
            'text': "<br>Trait uniqueness % <br><sup style='color:#aaa;'>Calculated as a total proportion of tokens with a different trait value.</sup>",
            'y':0.975,
            'x':0,
            'xanchor': 'left',
            'yanchor': 'top',
            'font': dict(
                family="Futura, Helvetica, sans-serif",
                size=16,
                color="#222"
                ),
            },
            xaxis = {'title':'',
                    'titlefont': dict(family='sans-serif', size=10, color='#666'),
                    'tickangle': 0,
                    'showline': True,
                    'linewidth': 0.1,
                    'linecolor': '#fff',
                    # 'tickformat': ',.0',
                    'showspikes': True,
                    'spikethickness': 0.5,
                    # 'range': [0,1],
                    'showgrid': True,
                    'gridcolor': '#eee',
                    'gridwidth': 0.1
                    },
            yaxis = {'title': '',
                    'titlefont': dict(family='sans-serif', size=10, color='#222'),
                    'ticksuffix': "  ",
                    'showspikes': False,
                    'spikethickness': 0.5,
                    'spikedash': 'solid',
                    'showgrid': False,
                    'gridcolor': '#fff',
                    'gridwidth': 0.01,
                    },
            margin = {'l': 0, 'b': 0, 't': 50, 'r': 0},
            showlegend = False,
            # legend = {'x': 0.9, 'y': 1, 'font': dict(family='sans-serif', size=12, color='#222')},#, 'title': dict(text=' WORD ', font=dict(size=16))},
            plot_bgcolor = background,
            paper_bgcolor = background,
            hovermode = 'closest',
            font = dict(
                family="Futura, Helvetica, sans-serif",
                size=10,
                color="#666"
                )
            )
        # Customize aspect
        fig.update_traces(marker_line_color='rgba(200, 200, 200, 1)',
                        marker_line_width=1, opacity=1)

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        # # SHAP VALUES
        shaps = [col for col in df if col.startswith('shap_value')]
        traits = [col for col in df if col.startswith('value')]

        dfv_fltrd3 = dfv_token[dfv_token['flag_is_latest'] == 1]
        dfv_fltrd3 = dfv_fltrd3.iloc[0]
        dfv_t3 = dfv_fltrd3.transpose().reset_index()
        dfv_t3.rename(columns={dfv_t3.columns[0]: 'variable', dfv_t3.columns[1]: 'value'},inplace = True)
        dfv_shaps = dfv_t3[dfv_t3['variable'].isin(shaps)]
        dfv_shaps.index = dfv_shaps['variable'].str.replace('shap_value','')

        dfv_shaps['value'] = dfv_shaps['value'].astype(float)
        dfv_shaps['value_abs'] = dfv_shaps['value'].abs()
        dfv_shaps = dfv_shaps.nlargest(10, 'value_abs')

        dfv_shaps['variable'] = dfv_shaps['variable'].str.replace('shap_value','')
        dfv_shaps = dfv_shaps.sort_values(by=['value'], ascending=False)
        background = '#fff'

        fig3 = go.Figure()

        # Draw lines
        for i in range(0, len(dfv_shaps)):
            fig3.add_shape(type='line',
                            x0 = 0, y0 = i,
                            x1 = dfv_shaps['value'].iloc[i],
                            y1 = i,
                            line=dict(
                                color='#ee0000'
                                , width = 3 
                            ))

        # Draw points
        fig3.add_trace(go.Scatter(x = dfv_shaps['value'], 
                                y = dfv_shaps['variable'],
                                mode = 'markers',
                                marker = dict(color='#ee0000'
                                    ,size = 10
                                    ,colorscale=[[0.0, "rgb(238, 0, 0)"],
                                    [1.0, "rgb(0, 160, 176)"]]   
                                ),
                                hovertemplate=
                                "<b>%{y}</b><br>" +
                                "Impact on price: %{x:,.3f}<br>" +
                                "<extra></extra>",
                                marker_size  = 10))

        # Change the bar mode
        fig3.update_layout(
            title={
            'text': "<br>Traits by impact on price <br><sup style='color:#aaa;'>Based on Shapley values of the prediction model</sup>",
            'y':0.975,
            'x':0,
            'xanchor': 'left',
            'yanchor': 'top',
            'font': dict(
                family="Futura, Helvetica, sans-serif",
                size=16,
                color="#222"
                ),
            },
            xaxis = {'titlefont': dict(family='sans-serif', size=10, color='#222'),
                    'tickangle': 0,
                    'showline': True,
                    'linewidth': 0.3,
                    'linecolor': '#fff',
                    'zeroline': True,
                    'zerolinecolor': '#aaa',  
                    'zerolinewidth': 0.3,
                    'showspikes': True,
                    'spikethickness': 0.5,
                    'showgrid': True,
                    'gridcolor': '#eee',
                    'gridwidth': 0.1
                    },
            yaxis = {'title': '',
                    'titlefont': dict(family='sans-serif', size=10, color='#222'),
                    'ticksuffix': "  ",
                    'showspikes': False,
                    'spikethickness': 0.5,
                    'spikedash': 'solid',
                    'showgrid': False,
                    'gridcolor': '#aaa',
                    'gridwidth': 0.1,
                    },
            margin = {'l': 0, 'b': 15, 't': 50, 'r': 0},
            # legend = {'x': 0.9, 'y': 1, 'font': dict(family='sans-serif', size=12, color='#222')},#, 'title': dict(text=' WORD ', font=dict(size=16))},
            plot_bgcolor = background,
            paper_bgcolor = background,
            hovermode = 'closest',
            font = dict(
                family="Helvetica, sans-serif",
                size=11,
                color="#7f7f7f"
                )
            )


        # SALES HISTORY
        dfv_fltrd2 = dfv_token.sort_values(by=['sale_date'], ascending=True)
        colors = ['#ee0000','#edc951','#00a0b0','#336699','#8d2867','#949494','#4f372d','#eb6841','#2d454f']
        ci_colors = ['#ee0000','#00a0b0']
        background = '#fff'
        fig_none = go.Figure()
        fig_none.add_trace(go.Scatter(
            x=[0, 1, 2, 3, 4, 5, 6, 7, 8, 10],
            y=[0, 4, 5, 1, 2, 3, 2, 4, 2, 1],
            mode="lines+markers+text",
            text=["","","","", "    NO PAST SALES", "","","", "", ''],
            textfont_size=16,
            marker = dict(
                size = 8
                ,color = '#ffffff'
                ,line = dict(width=0, color='#fff')
                ,opacity=0
                ),
            hoverinfo='skip'
        ))
        fig_none.update_layout(
            paper_bgcolor=background,
            plot_bgcolor=background,
            margin = {'l': 70, 'b': 70, 't': 50, 'r': 50}
        )
        fig_none.update_layout(
            xaxis = dict(
                showgrid=False,
                gridcolor=background,
                zerolinecolor=background
                ),
            yaxis = dict(
                showgrid=False,
                gridcolor=background,
                zerolinecolor=background
                )
            )

        fig_none.update_traces(marker_line_color='rgba(255, 255, 255, 1)',marker_line_width=0, opacity=1)
        fig_none.update_xaxes(showticklabels=False)
        fig_none.update_yaxes(showticklabels=False)

        if dfv_fltrd2['sale_date'].isnull().values.all() == True:
            return token, traits_no, price, hist, hist2, hist3, pred, num_sales, image, fig, fig_none, fig3, [], None

        else:
            fig2 = go.Figure()
            # fig = make_subplots(specs=[[{"secondary_y": True}]])

            fig2.add_trace(go.Scatter(
                                name="Sale Price",
                                x = dfv_fltrd2['sale_date'],
                                y = dfv_fltrd2['sale_price'],
                                text=dfv_fltrd2['token_id'],
                                customdata=dfv_fltrd2['sale_date'],
                                mode = 'lines+markers',
                                marker = dict(
                                    size = 14
                                    ,color = '#00a0b0'
                                    ,line = dict(width=1, color='#fff')
                                    ,opacity=0.7
                                ),
                                hovertemplate=
                                "<b>Meebit #%{text}</b><br>" +
                                "Sale Price (ETH): %{y:.2f}<br>" +
                                "on %{x}<br>" +
                                "<extra></extra>"
                                )
                                # secondary_y=True
                            )

            # Change the bar mode
            fig2.update_layout(
                title={
                'text': "<br>Sale history <br><sup style='color:#aaa;'>Data sourced from etherscan.io</sup>",
                'y':0.975,
                'x':0,
                'xanchor': 'left',
                'yanchor': 'top',
                'font': dict(
                    family="Futura, Helvetica, sans-serif",
                    size=16,
                    color="#222"
                    ),
                },

                xaxis = {'title':'',
                        'titlefont': dict(family='sans-serif', size=10, color='#666'),
                        'tickangle': 0,
                        'showline': True,
                        'linewidth': 0.1,
                        'linecolor': '#fff',
                        # 'tickformat': ',.0',
                        'showspikes': True,
                        'spikethickness': 0.5,
                        'showgrid': True,
                        'gridcolor': '#eee',
                        'gridwidth': 0.1
                        },
                yaxis = {'title': 'Price',
                        'titlefont': dict(family='sans-serif', size=10, color='#222'),
                        'ticksuffix': "  ",
                        'showspikes': False,
                        'spikethickness': 0.5,
                        'spikedash': 'solid',
                        'showgrid': False,
                        'gridcolor': '#fff',
                        'gridwidth': 0.01,
                        },
                margin = {'l': 0, 'b': 0, 't': 50, 'r': 0},
                showlegend = False,
                plot_bgcolor = background,
                paper_bgcolor = background,
                hovermode = 'closest',
                font = dict(
                    family="Futura, Helvetica, sans-serif",
                    size=11,
                    color="#666"
                    ),
                legend=dict(
                    yanchor="top",
                    y=0.99,
                    xanchor="left",
                    x=0.05)
                )

            fig2.update_xaxes(categoryorder='total ascending',fixedrange=True, tickformat="%b %d\n%Y")

        return token, traits_no, price, hist, hist2, hist3, pred, num_sales, image, fig, fig2, fig3, [], None

    ###################################################################################################
    # TYPE-RELATED CHARTS


    # Table
    @callback(
        Output('first_{0}'.format(collection), 'style_data_conditional'),
        [Input('first_{0}'.format(collection), 'selected_columns')]
    )
    def update_styles(selected_columns):
        return [{
            'if': {'column_id': i},
            'background_color': '#D2F3FF'
        } for i in selected_columns]

    @callback(
        [
        Output(component_id='first_{0}'.format(collection), component_property='data')
        ],
        [Input(component_id='first_{0}'.format(collection), component_property="derived_virtual_data"),
        Input(component_id='first_{0}'.format(collection), component_property='derived_virtual_selected_rows'),
        Input(component_id='first_{0}'.format(collection), component_property='derived_virtual_selected_row_ids'),
        Input(component_id='first_{0}'.format(collection), component_property='selected_rows'),
        Input(component_id='first_{0}'.format(collection), component_property='derived_virtual_indices'),
        Input(component_id='first_{0}'.format(collection), component_property='derived_virtual_row_ids'),
        Input(component_id='first_{0}'.format(collection), component_property='active_cell'),
        Input(component_id='first_{0}'.format(collection), component_property='selected_cells')]
    )


    def display_value(all_rows_data, slctd_row_indices, slct_rows_names, slctd_rows,
                order_of_rows_indices, order_of_rows_names, actv_cell, slctd_cell):
        # print('***************************************************************************')
        # print('Data across all pages pre or post filtering: {}'.format(all_rows_data))
        # print('---------------------------------------------')
        # print("Indices of selected rows if part of table after filtering:{}".format(slctd_row_indices))
        # print("Names of selected rows if part of table after filtering: {}".format(slct_rows_names))
        # print("Indices of selected rows regardless of filtering results: {}".format(slctd_rows))
        # print('---------------------------------------------')
        # print("Indices of all rows pre or post filtering: {}".format(order_of_rows_indices))
        # print("Names of all rows pre or post filtering: {}".format(order_of_rows_names))
        # print("---------------------------------------------")
        # print("Complete data of active cell: {}".format(actv_cell))
        # print("Complete data of all selected cells: {}".format(slctd_cell))

        dfv_type = df

        dfv_fltrd = dfv_type[dfv_type['flag_is_latest'] == 1]
        dfv_fltrd['pred_price'] = dfv_fltrd['pred_price'].round(4)
        dfv_fltrd['opportunity_coef'] = dfv_fltrd['opportunity_coef'].round(4)
        dfv_fltrd['current_sale_price'] = dfv_fltrd['current_sale_price'].round(4)
        dfv_fltrd['url'] = "[🔗](" + dfv_fltrd['url'].map(str) + ")"

        dfv_fltrd = dfv_fltrd.sort_values(by=['opportunity_coef'], ascending=False)

        data = dfv_fltrd[['id', 'token_id','current_sale_price','pred_price','opportunity_coef', base_type,'url']].to_dict('records')
        # colors = ['#ee0000','#edc951','#00a0b0','#336699','#4f372d','#949494','#8d2867','#eb6841','#2d454f']

        return [data]

    # Scatter Plot Distribution
    @callback(
        [
        # Output(component_id='first_{0}'.format(collection), component_property='data'),
        Output(component_id='third_{0}'.format(collection), component_property='figure')
        ],
        [Input(component_id='url-dropdown_{0}'.format(collection), component_property='value')]
        # Input(component_id="load_interval", component_property="n_intervals"),
    )

    def display_value(token_id):
        dfv_type = df[df['flag_is_latest'] == 1]
        # df['sale_date'] = pd.to_datetime(df['sale_date'], format = "%Y-%m-%d")
        # mask = dfv_type['sale_date'].dt.strftime("%Y-%m") == datetime.today().strftime("%Y-%m")
        # dfv_type = dfv_type[mask]
        total = df['token_id'].nunique()
        print(total)
        print(int(total % 10 // 1))
        decimals = int(total % 10 // 1) - 1
        if total >= 100000:
            decimals = 4
        elif total >= 50000:
            decimals = 4
        elif total >= 10000:
            decimals = 3
        elif total >= 5000:
            decimals = 3
        elif total >= 1000:
            decimals = 2
        elif total >= 500:
            decimals = 2
        else:
            decimals = 1
        print(decimals)
        # 9956/10 = 995.6
        # 546/10 = 54.6
        # bucket = custom_round(total / 10)
        dfv_token = df[df[id]==token_id]
        token = dfv_token.token_id.iloc[0]
        token = '#{}'.format(token)
        rarity_ranking = dfv_token.rarity_ranking.round(decimals=-decimals).iloc[0]
        # dfv_type = dfv_type[(dfv_type['rarity_ranking'].apply(lambda x: custom_round(x, base=1000)) == rarity_ranking)]
        dfv_type = dfv_type[(dfv_type['rarity_ranking'].round(decimals=-decimals)) == rarity_ranking]

        
        dfv_fltrd = dfv_type[dfv_type['flag_is_latest'] == 1]
        dfv_fltrd = dfv_fltrd[dfv_fltrd['current_sale_price'].isnull() == False]
        dfv_fltrd['pred_price'] = dfv_fltrd['pred_price'].round(4)
        dfv_fltrd['opportunity_coef'] = dfv_fltrd['opportunity_coef'].round(4)
        dfv_fltrd['current_sale_price'] = dfv_fltrd['current_sale_price'].round(4)
        dfv_fltrd = dfv_fltrd.sort_values(by=['opportunity_coef'], ascending=False)

        dfv_type['randNumCol'] = np.random.normal(loc=0, scale=0.1, size = len(dfv_type))

        # regression
        # dfv_type['bestfit'] = dfv_type.groupby(dfv_type.sale_date)['sale_price'].mean()
        # dfv_type['bestfit'] = dfv_type.groupby(pd.PeriodIndex(df['sale_date'], freq="d"))['sale_price'].mean().reset_index().
        # dfv_type['Week'] - pd.to_timedelta(dfv_type['sale_date'].dt.dayofweek, unit='d')
        # dfv_type['week_start'] = dfv_type['sale_date'].dt.to_period('W').apply(lambda r: r.start_time)
        # dfv_type['week_start'] = dfv_type['sale_date'] - dfv_type['sale_date'].dt.weekday.astype('timedelta64[D]')
        dfv_type['bestfit'] = dfv_type.groupby(['month_index'])['sale_price'].transform('mean')

        dfv_type = dfv_type.sort_values(by=['sale_date'], ascending=True)

        # data_month = data.resample('M', on='Date').mean()

        print(dfv_type)
        colors = ['#ee0000','#edc951','#00a0b0','#336699','#4f372d','#949494','#8d2867','#eb6841','#2d454f']

        # fig2 = go.Figure()
        fig2 = make_subplots(specs=[[{"secondary_y": True}]])
        fig2.add_trace(go.Scatter(name='Monthly Average', x=dfv_type['sale_date'], y=dfv_type['bestfit'], mode='lines',marker= dict(color=colors[1], 
                                        opacity=1, 
                                        size=10,
                                        line=dict(width=2,color=colors[1])
                                        ),),secondary_y=True)

        fig2.add_trace(go.Scattergl(
                                x=dfv_type['sale_date'],
                                y=dfv_type['sale_price'].round(4),
                                name=token,
                                mode='markers',
                                showlegend=False,
                                marker= dict(color='rgba(35, 35, 35, 0.1)', 
                                        opacity=0.6, 
                                        size=10,
                                        line=dict(width=2,color=colors[2])
                                        ),
                                hovertemplate=
                                "<b>%{y} ETH</b><br>",
                                # "Impact on price: %{x:,.3f}<br>" +
                                # "<extra></extra>",
                                marker_size  = 10
                                # hoverinfo="skip"
                                )
                                ,secondary_y=False
                                )

        fig2.update_layout(
                title={
                'text': "<br>Price distribution of similar rarity rank<br><sup style='color:#aaa;'>Includes historic prices achieved by tokens with rarity ranking rounded to {0}.</sup>".format(rarity_ranking),
                'y':0.975,
                'x':0,
                'xanchor': 'left',
                'yanchor': 'top',
                'font': dict(
                    family="Futura, Helvetica, sans-serif",
                    size=16,
                    color="#222"
                    ),            
                },

                # barmode='stack', 
                xaxis = {'title': 'Value in ETH',
                        'titlefont': dict(family='Futura, sans-serif', size=10, color='#666'),
                        'tickangle': 0,
                        'showline': True,
                        'linewidth': 0.1,
                        # 'tickformat': ',.0',
                        'showspikes': True,
                        'spikethickness': 0.5,
                        # 'range': [0,1],
                        'showgrid': True,
                        'gridcolor': '#eee',
                        'gridwidth': 0.1,
                        'zeroline':True
                        },
                yaxis = {'title': '',
                        'titlefont': dict(family='sans-serif', size=12, color=colors[2]),
                        'ticksuffix': "  ",
                        'tickfont': dict(color=colors[2]),
                        'showline': True,
                        'showspikes': True,
                        'spikethickness': 0.5,
                        'spikedash': 'solid',
                        'showticklabels':True,
                        'showgrid': False,
                        'gridcolor': colors[1],
                        'gridwidth': 0.01,
                        'zeroline':True
                        },
                yaxis2 = {'title': '',
                        'titlefont': dict(family='sans-serif', size=12, color=colors[1]),
                        'ticksuffix': "  ",
                        'tickfont': dict(color=colors[1]),
                        'showline': True,
                        'showspikes': True,
                        'spikethickness': 0.5,
                        'spikedash': 'solid',
                        'showticklabels':True,
                        'showgrid': False,
                        'gridcolor': colors[2],
                        'gridwidth': 0.01,
                        'zeroline':True
                        },
                margin = {'l': 10, 'b': 0, 't': 50, 'r': 0},
                plot_bgcolor = "#fff",
                font = dict(
                    family="Futura, Helvetica, sans-serif",
                    size=11,
                    color="#666"
                ),
                paper_bgcolor = "#fff",
                hovermode = 'closest',
                legend=dict(
                    orientation="h",
                    yanchor="top",
                    y=1.02,
                    xanchor="right",
                    x=1
                )
                )

        # Customize aspect
        # fig2.update_yaxes(range=[-0.5, 0.5])

        return [go.Figure(data=fig2)]
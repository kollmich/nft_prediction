import dash
dash.register_page(__name__, title='ADD A NEW COLLECTION', name='ADD A NEW COLLECTION') 
# dash.register_page(path="https://www.buymeacoffee.com/spottingtrends", module= 'pages.add_collection',title='ADD A NEW COLLECTION', name='ADD A NEW COLLECTION', order = -1) 
from dash import dcc, html, callback, dash_table
import pandas as pd
import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots
import pandas as pd
import time
import dash_bootstrap_components as dbc
from pages.template import footer, get_callbacks

layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Iframe(src="https://docs.google.com/forms/d/e/1FAIpQLSeQYm45JiliSsmklLkU7y5MANSI3vylxho9ddnr256dfcwbLQ/viewform?embedded=true", style={"width":"768", "height":"1024", "frameborder":"0", "marginheight":"0", "marginwidth":"0"})
],id='g_form')